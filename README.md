### HacerGestoV2 is a feature rich Gesture library for the Android platform. (Supports API 14+) ###
## Features of this library are: ##

**NOTE: HacerGestoV3 has landed https://bitbucket.org/warwick/hacergestov3**

1.	The ability to use all four of the classic gestures together seamlessly.
2.	All of the gestures have behavioral flags that can be used in any combination.
3.	Intuitive callbacks that return status information of the gestures.
4.	Utilities OpenGLES 2.0 making the library extremely responsive, even when all four gestures are being used.
5.	Well abstracted intuitive class usage.
6.	Comes with an extensive demo application and source code to give developers a head start.
7.	Very open ended and functional to support almost any usage scenario that require gestures.
8.	The library is designed so that one gesture object can act upon another; opening up unlimited usage scenarios (not yet fully tested).
9.	All of the gesture classes can be used individually outside the library package.
10.	Library now supports alpha blending.

**Developer Note: HacerGestoV3 has landed. Improvements include: Rotate, Scale and Move now all work simultaneously, fling now works with move snapping and improved fling metrics calculations. The algorithms are now more optimal. Unless you need the extra processing power of OpenGL ES I recommend using HacerGestoV3 over this library.**

**You can view a a video of the demo applications here: https://youtu.be/MrIeep3Q0Ko**

**For your convenience the demo app is now published to the Google Play Store: https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HacerGestoV2&hl=en_GB**

**You can make a donation the the HG Widgets for Android here:**

**https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8**

**This library comes with four gestures: Fling, Move, Scale and Rotate.**

**Rotate Gesture Features:**

Cumulative or non cumulative rotation behavior. When set to true the rotation occurs when gesture is moved, when set to false the object will rotate directly to the angle where the finger touches and rotates from there.

Dual and single finger mode.

Angle snapping with proximity settings. The proximity settings cause the gesture to rotate freely until a proximity is reached before snapping. The angle of proximity and snapping are fully configurable with a simple method call.

Sensitivity settings. It is possible to make the object rotate at a different rate to the gesture or even in the opposite direction of the gesture.

Minimum and maximum rotation constraints.

Gesture keeps count of the number of rotations of both the image and the gesture.

It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the centre of the dial. This variable dial can be adjusted ot accelerate/decelerate on a curve.

This library comes with a 'fling-to-spin' behaviour; having configurable fling tolerance, spin start/end speed and spin animation duration.


**Scale Gesture Features:**

Scale snapping. It is possible to scale in programmable increments.

Minimum and maximum scale limits.

**Move Gesture features:**

Cumulative and non cumulative behavior. When set to true the image will move relative to the touch, disable the image will instantly move to where you put the finger down and move from there.

Move snapping. This is a very clever feature in that you can set any amount of snap positions and each snap position can have a different snap tolerance (distance in pixels before the snap occurs.)

**Fling Gesture Features:**

Very useful behavioral flags apart from the default behavior which is to fling in the direction of the gesture. It has a flag that causes the image to fling to the nearest corner or nearest edge (horizontally and vertically). It also has a flag to make it fling of the edge of the screen or to the edge of the screen, again these flags will work in any combination.

Fling sensitivity settings. It is possible to affect how sensitive the gesture is. With 3 settings fling distance, fling time and the duration of the triggered fling animation.

**Class Usage:**
At present I have written up an informal developer doc. For the time being the source code of the demo application along with this doc will show you how easy the library is to use.

**Road Map:**
I will be adding some image manipulation utilities for capturing and saving the manipulated image, plus some functions for loading the image from both local and remote sources. I may also add some more geometric methods.

To use gesture classes individually outside the library see the example Android Studio projects contained in the zip files (HGFling.zip, HGMove.zip, HGRotate.zip and HGScale.zip).

The repository also contains the Android AAR library files for easy inclusion into your project (‘HacerGestoV2-AAR-release.zip’ also contains the debug AAR file). Along with this file is an exact copy of the demo application that uses the AAR library file instead of the source code.

This to demonstrate it’s easy usage. The library is now fully documented, see files: ‘HacerGestoV2_Docs.pdf’ and ‘HacerGestoV2_Docs.rtf’.

**Contact**
If you wish to commission the services of the developer of this library or make feature requests you can contact him at: warwickwestonwright@gmail.com or warwickwestonwright@live.com

**LICENSE.**
This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.