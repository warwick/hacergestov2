/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;

public class ImageUtilities {

	public static Bitmap getProportionalBitmap(final Bitmap bitmap, int newDimensionXorY, String XorY) {

		Bitmap ImmutableBitmap;
		ImmutableBitmap = bitmap;

		if(ImmutableBitmap == null) {

			return null;

		}

		float xyRatio;
		int newWidth;
		int newHeight;

		if(XorY.toLowerCase().equals("x")) {

			xyRatio = (float) newDimensionXorY / (float) bitmap.getWidth();
			newHeight = (int) ((float) bitmap.getHeight() * xyRatio);
			ImmutableBitmap = Bitmap.createScaledBitmap(bitmap, newDimensionXorY, newHeight, true);

		}
		else if(XorY.toLowerCase().equals("y")) {

			xyRatio = (float) newDimensionXorY / (float) bitmap.getHeight();
			newWidth = (int) ((float) bitmap.getWidth() * xyRatio);
			ImmutableBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newDimensionXorY, true);

		}

		return ImmutableBitmap;

	}//End public static Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY)

	public static Bitmap getScaledBitmap(final Bitmap bitmap, int newWidth, int newHeight) {

		return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);

	}//End public static Bitmap getScaledBitmap(final Bitmap bitmap, int newWidth, int newHeight)

}