/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.opengles.GL10;

public class HGMove extends GLSurfaceView implements HGRender.IHGRender {

	private HGRender hgRender;
	private HGRender.IHGRender ihgRender;
	private int imageResourceId;
	private OnTouchListener onTouchListener;
	private final Point imageCenterPoint = new Point();
	private IHGMove ihgMove;
	private final HGResult hgResult = new HGResult();
	private static HGTransformInfo hgTransformInfo;
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private int touchPointerCount;
	private boolean cumulativeMove;
	private float cumulativeMoveDistanceX;
	private float cumulativeMoveDistanceY;
	private float moveDistanceX;
	private float moveDistanceY;
	private boolean disableGestureInternally;
	private boolean disableGestureDynamically;
	private float touchOffsetX;
	private float touchOffsetY;
	private int[][] renderOffset;
	private long quickTapTime;
	private long gestureDownTime;
	private float currentTransX;
	private float currentTransY;
	private boolean suppressRender;
	private float imageWidth;
	private float imageHeight;
	private float viewPortWidth;
	private float viewPortHeight;
	private final Point centreOffset = new Point();
	private float downTouchX;
	private float downTouchY;
	private float snapTolerances[];
	private Point[] snapPositions;//Optimisation note. Consider making an HGMove constructor with a snapPositions parameter.
	private int snapPositionsCount;
	private boolean snapFound;
	private int snapFoundAtIdx;
	private boolean surfacedChanged;
	private MultipleTextures[] multipleTextures;


	/* Top of block field declarations */
	public interface IHGMove {

		void onDown(HGResult hgResult);
		void onPointerDown(HGResult hgResult);
		void onMove(HGResult hgResult);
		void onPointerUp(HGResult hgResult);
		void onUp(HGResult hgResult);

	}

	public HGMove(Context context, AttributeSet attrs) {
		super(context, attrs);

		setFields();

		if(hgTransformInfo == null) {

			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

			if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

				imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
				a.recycle();
				hgRender = new HGRender(getResources(), imageResourceId);
				setRenderer(hgRender);
				setDimensionsAndMultipleTextures();
				setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

			}
			else {

				setDimensionsAndMultipleTextures();

			}//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

		}
		else if(hgTransformInfo != null) {

			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

		}//End if(hgTransformInfo == null)

	}//End public HGMove(Context context, AttributeSet attrs)


	private void setDimensionsAndMultipleTextures() {

		post(new Runnable() {
			@Override
			public void run() {

				setupMetrics();
				requestRender();

			}
		});

	}//End private void setDimensionsAndMultipleTextures()


	@Override
	public void onResume() {
		super.onResume();

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	}


	void setupMetrics() {

		this.imageWidth = HGRender.getImageHeightAndWidth()[0];
		this.imageHeight = HGRender.getImageHeightAndWidth()[1];
		imageCenterPoint.x = HGRender.getImageCenterPoint().x;
		imageCenterPoint.y = HGRender.getImageCenterPoint().y;
		this.multipleTextures = HGRender.getMultipleTextures();
		renderOffset = new int[multipleTextures.length][];

		if(multipleTextures[0] != null) {

			for(int i = 0; i < multipleTextures.length; i++) {

				renderOffset[i] = new int[2];
				renderOffset[i][0] = multipleTextures[i].getOffset().x;
				renderOffset[i][1] = -multipleTextures[i].getOffset().y;

			}

		}

		viewPortWidth = HGRender.getViewPortHeightAndWidth()[0];
		viewPortHeight = HGRender.getViewPortHeightAndWidth()[1];
		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		centreOffset.x = (int) ((viewPortWidth - imageWidth) / 2f);
		centreOffset.y = (int) ((viewPortHeight - imageHeight) / 2f);

	}//End void setupMetrics()


	private void setFields() {

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgMove = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.cumulativeMove = true;
		this.cumulativeMoveDistanceX = 0f;
		this.cumulativeMoveDistanceY = 0f;
		this.moveDistanceX = 0f;
		this.moveDistanceY = 0f;
		this.disableGestureInternally = false;
		this.disableGestureDynamically = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentTransX = 0f;
		this.currentTransY = 0f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.downTouchX = 0f;
		this.downTouchY = 0f;
		this.snapTolerances = null;
		this.snapPositions = null;
		this.snapPositionsCount = 0;
		this.snapFound = false;
		this.snapFoundAtIdx = 0;
		this.multipleTextures = null;

	}//End private void setFields()


	public void registerCallback(IHGMove ihgMove) {this.ihgMove = ihgMove;}


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		gestureDownTime = System.currentTimeMillis();

		try {

			try {

				touchPointerCount = event.getPointerCount();
				disableGestureInternally = false;

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					downTouchX = firstTouchX + touchOffsetX;
					downTouchY = firstTouchY + touchOffsetY;
					secondTouchX = 0;
					secondTouchY = 0;
					moveDistanceX = 0;
					moveDistanceY = 0;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else if(touchPointerCount == 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount < 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(event.getPointerCount() < 2)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		if(System.currentTimeMillis() < (gestureDownTime + quickTapTime)) {

			hgResult.setQuickTap(true);

		}
		else {

			hgResult.setQuickTap(false);

		}//End if(System.currentTimeMillis() < (gestureDownTime + quickTapTime))

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetX;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);

				}
				else if(touchPointerCount == 2) {

					disableGestureInternally = true;
					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);
					moveDistanceX = firstTouchX - event.getX(event.getActionIndex());
					moveDistanceY = firstTouchY - event.getY(event.getActionIndex());

					if((moveDistanceX == 0) && (moveDistanceY == 0)) {

						downTouchX = event.getX(event.findPointerIndex(event.getPointerId(1)));
						downTouchY = event.getY(event.findPointerIndex(event.getPointerId(1)));

					}
					else {

						downTouchX = event.getX(event.findPointerIndex(event.getPointerId(0)));
						downTouchY = event.getY(event.findPointerIndex(event.getPointerId(0)));

					}

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block move functions */
	private HGResult doDownMove() {

		if(touchPointerCount == 2) {

			disableGestureInternally = true;
			return hgResult;

		}//End if(touchPointerCount == 1)

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(hgTransformInfo != null) {

			if(hgTransformInfo.getFlingTriggered() == true) {

				if(hgTransformInfo.getBounceBack() == true) {

					hgTransformInfo.setCurrentTransX(0);
					hgTransformInfo.setCurrentTransY(0);
					hgTransformInfo.setFlingStartPoint(new Point(0, 0));
					hgResult.setXPos(0);
					hgResult.setYPos(0);
					currentTransX = 0;
					currentTransY = 0;
					cumulativeMoveDistanceX = 0;
					cumulativeMoveDistanceY = 0;

				}
				else if(hgTransformInfo.getBounceBack() == false) {

					hgResult.setXPos(hgTransformInfo.getCurrentTransX());
					hgResult.setYPos(hgTransformInfo.getCurrentTransY());
					currentTransX = hgTransformInfo.getCurrentTransX();
					currentTransY = hgTransformInfo.getCurrentTransY();

					if(hgTransformInfo.getFlingTriggered() == true) {

						cumulativeMoveDistanceX = hgTransformInfo.getCurrentTransX();
						cumulativeMoveDistanceY = -hgTransformInfo.getCurrentTransY();

					}

				}//End if(hgTransformInfo.getBounceBack() == true)

			}//End if(hgTransformInfo.getFlingTriggered() == true)

		}
		else if(hgTransformInfo == null) {

			hgResult.setXPos(currentTransX);
			hgResult.setYPos(currentTransY);

		}//End if(hgTransformInfo != null)

		if(cumulativeMove == false) {

			cumulativeMoveDistanceX = (downTouchX - imageCenterPoint.x) - centreOffset.x;
			cumulativeMoveDistanceY = (downTouchY - imageCenterPoint.y) - centreOffset.y;
		}

		if(hgTransformInfo != null) {

			hgTransformInfo.setFlingTriggered(false);

		}

		if(suppressRender == false) {

			requestRender();

		}

		return hgResult;

	}//End private HGResult doDownMove()


	private HGResult doMoveMove() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(touchPointerCount == 2) {

			disableGestureInternally = true;

			if(hgTransformInfo != null) {

				hgTransformInfo.setCurrentTransX(currentTransX);
				hgTransformInfo.setCurrentTransY(currentTransY);

			}//End if(hgTransformInfo != null)

			return hgResult;

		}//End if(touchPointerCount == 2)

		if(snapPositionsCount > 0) {

			moveSnapMotion();

			if(hgTransformInfo != null) {

				hgTransformInfo.setCurrentTransX(currentTransX);
				hgTransformInfo.setCurrentTransY(currentTransY);

			}//End if(hgTransformInfo != null)

			hgResult.setXPos(currentTransX);
			hgResult.setYPos(currentTransY);

			if(suppressRender == false) {

				requestRender();

			}

			return hgResult;

		}//End if(snapPositionsCount > 0)

		currentTransX -= (currentTransX - firstTouchX) + downTouchX - cumulativeMoveDistanceX;
		currentTransY -= (currentTransY - firstTouchY) + downTouchY - cumulativeMoveDistanceY;
		currentTransY = -(currentTransY);
		hgResult.setXPos(currentTransX);
		hgResult.setYPos(currentTransY);

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentTransX(currentTransX);
			hgTransformInfo.setCurrentTransY(currentTransY);

		}//End if(hgTransformInfo != null)

		if(suppressRender == false) {

			requestRender();

		}

		return hgResult;

	}//End private HGResult doMoveMove()


	private HGResult doUpMove() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(disableGestureInternally == true) {

			return hgResult;

		}//End if(disableGestureInternally == true)

		if(cumulativeMove == true) {

			cumulativeMoveDistanceX -= (downTouchX - firstTouchX);
			cumulativeMoveDistanceY -= (downTouchY - firstTouchY);

		}

		if(snapFound == false) {

			if(suppressRender == false) {

				requestRender();

			}

		}

		if(getSnapIn() != -1) {

			currentTransX = snapPositions[snapFoundAtIdx].x;
			currentTransY = snapPositions[snapFoundAtIdx].y;
			hgResult.setXPos(currentTransX);
			hgResult.setYPos(currentTransY);
			hgResult.setMoveSnapped(true);

		}
		else {

			hgResult.setXPos(currentTransX);
			hgResult.setYPos(currentTransY);
			hgResult.setMoveSnapped(false);

		}//End if(getSnapIn() != -1)

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentTransX(currentTransX);
			hgTransformInfo.setCurrentTransY(currentTransY);

		}

		if(snapPositionsCount > 0) {snapFound = false;}

		if(suppressRender == false) {

			requestRender();

		}

		return hgResult;

	}//End private HGResult doUpMove()
    /* Bottom of block move functions */


	private void moveSnapMotion() {

		currentTransX -= (currentTransX - firstTouchX) + downTouchX - cumulativeMoveDistanceX;
		currentTransY -= (currentTransY - firstTouchY) + downTouchY - cumulativeMoveDistanceY;
		currentTransY = -(currentTransY);

		if(getSnapIn() != -1) {

			currentTransX = snapPositions[snapFoundAtIdx].x;
			currentTransY = snapPositions[snapFoundAtIdx].y;

		}

	}//End private void moveSnapMotion()


	private int getSnapIn() {

		for(int i = 0; i < snapPositionsCount; i++) {

			if(((currentTransX < 0 && (Math.abs(snapPositions[i].x - currentTransX) < snapTolerances[i])) || (currentTransX >= 0 && (Math.abs(snapPositions[i].x - currentTransX) < snapTolerances[i]))) &&
				((currentTransY < 0 && (Math.abs(snapPositions[i].y - currentTransY) < snapTolerances[i])) || (currentTransY >= 0 && (Math.abs(snapPositions[i].y - currentTransY) < snapTolerances[i])))) {

				snapFoundAtIdx = i;
				hgResult.setMoveSnapped(true);
				return i;

			}//End if ....

		}

		return -1;

	}//End private int getSnapIn()


	public void doManualMove(final float currentTransX, final float currentTransY) {

		this.currentTransX = currentTransX;
		this.currentTransY = currentTransY;

		hgResult.setXPos(currentTransX);
		hgResult.setYPos(currentTransY);
		cumulativeMoveDistanceX += currentTransX;
		cumulativeMoveDistanceY -= currentTransY;

		if(hgTransformInfo != null) {

			this.post(new Runnable() {
				@Override
				public void run() {

					hgTransformInfo.setCurrentTransX(currentTransX);
					hgTransformInfo.setCurrentTransY(currentTransY);
					hgResult.setWhatGesture(HGResult.MOVE_GESTURE);

					if(suppressRender == false) {

						requestRender();

					}

				}
			});

		}
		else if(hgTransformInfo == null) {

			this.post(new Runnable() {
				@Override
				public void run() {

					if(suppressRender == false) {

						requestRender();

					}

				}
			});

		}//End if(hgTransformInfo != null)

	}//End public void doManualMove(final float currentTransX, final float currentTransY)


	/* Top of block Accessors */
	public Point[] getMoveSnapPositions() {return snapPositions;}
	public float[] getMoveSnapTolerance() {return snapTolerances;}
	public boolean getCumulativeMove() {return cumulativeMove;}
	public long getQuickTapTime() {return this.quickTapTime;}
	public boolean getDisableGesture() {return this.disableGestureDynamically;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public boolean getSuppressRender() {return this.suppressRender;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setMoveSnap(final Point[] snapPositions, final float snapTolerances[]) {

		this.snapTolerances = snapTolerances;
		this.snapPositions = snapPositions;

		if(snapPositions != null && snapTolerances != null) {

			snapPositionsCount = snapPositions.length;

		}
		else {

			snapPositionsCount = 0;

		}

	}//End public void setMoveSnap(final Point[] snapPositions, final float snapTolerances[])


	public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {this.touchOffsetX = touchOffsetX;this.touchOffsetY = touchOffsetY;}
	public void setCumulativeMove(final boolean cumulativeMove) {this.cumulativeMove = cumulativeMove;}
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setDisableGesture(final boolean disableGestureDynamically) {this.disableGestureDynamically = disableGestureDynamically;}
	void setMultipleTextures(final MultipleTextures[] multipleTextures) {this.multipleTextures = multipleTextures;}
	public void setSuppressRender(final boolean suppressRender) {this.suppressRender = suppressRender;}
    /* Bottom of block Mutators */

	@Override
	public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] transMatrix, float[] matrixProjectionAndView) {

		final float[] scratch = new float[16];
		Matrix.setIdentityM(transMatrix, 0);
		Matrix.translateM(transMatrix, 0, currentTransX + centreOffset.x, currentTransY + centreOffset.y, 0.0f);
		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, transMatrix, 0);
		square.draw(scratch, activeTexture);

	}//End public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] transMatrix, float[] matrixProjectionAndView)


	static HGTransformInfo getHgTransformInfo() {return HGMove.hgTransformInfo;}
	static void setHgTransformInfo(HGTransformInfo hgTransformInfo) {HGMove.hgTransformInfo = hgTransformInfo;}


	public void performQuickTap() {

		hgResult.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgMove.onUp(doUpMove());
				hgResult.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgMove = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.cumulativeMove = true;
		this.cumulativeMoveDistanceX = 0f;
		this.cumulativeMoveDistanceY = 0f;
		this.moveDistanceX = 0f;
		this.moveDistanceY = 0f;
		this.disableGestureInternally = false;
		this.disableGestureDynamically = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentTransX = 0f;
		this.currentTransY = 0f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.downTouchX = 0f;
		this.downTouchY = 0f;
		this.snapTolerances = null;
		this.snapPositions = null;
		this.snapPositionsCount = 0;
		this.snapFound = false;
		this.snapFoundAtIdx = 0;
		this.multipleTextures = null;

	}//End protected void onDetachedFromWindow()


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	private void hgTouchEvent(final View v, final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
			hgResult.setQuickTap(false);//Used for quick tap
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgResult.setQuickTap(true);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);

			}
			else {

				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				hgResult.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		switch(action) {

			case MotionEvent.ACTION_MOVE: {

				setMoveTouch(event);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				ihgMove.onMove(doMoveMove());

				break;

			}
			case MotionEvent.ACTION_DOWN: {

				hgResult.setQuickTap(false);//Used for quick tap
				gestureDownTime = System.currentTimeMillis();
				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				ihgMove.onDown(doDownMove());

				break;

			}
			case MotionEvent.ACTION_POINTER_DOWN: {

				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				ihgMove.onDown(doDownMove());

				break;

			}
			case MotionEvent.ACTION_POINTER_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				ihgMove.onUp(doUpMove());

				break;

			}
			case MotionEvent.ACTION_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.MOVE_GESTURE);
				ihgMove.onUp(doUpMove());

				break;

			}
			default:

				break;

		}//End switch(action)

	}//End private void hgTouchEvent(final View v, final MotionEvent event)

}