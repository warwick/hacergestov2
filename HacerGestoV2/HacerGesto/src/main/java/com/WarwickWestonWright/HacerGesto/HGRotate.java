/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.lang.ref.WeakReference;

import javax.microedition.khronos.opengles.GL10;

import static com.WarwickWestonWright.HacerGesto.HGGeometry.getTwoFingerDistance;

public class HGRotate extends GLSurfaceView implements
	HGRender.IHGRender,
	Runnable {

	/* Top of block field declarations */
	private final HGRotateHandler hgRotateHandler = new HGRotateHandler(this);
	private HGRender hgRender;
	private HGRender.IHGRender ihgRender;
	private int imageResourceId;
	private OnTouchListener onTouchListener;
	private static final Point viewCenterPoint = new Point();
	private IHGRotate ihgRotate;
	private final HGResult hgResult = new HGResult();
	private static HGTransformInfo hgTransformInfo;
	private final GetAngleWrapper getAngleWrapper = new GetAngleWrapper();
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private int touchPointerCount;
	private double storedGestureAngleBaseOne;
	private int storedObjectCount;
	private double currentGestureAngleBaseOne;
	private double fullObjectAngleBaseOne;
	private boolean cumulativeRotate;
	private boolean isSingleFinger;
	private double onDownAngleObjectCumulativeBaseOne;
	private double onUpAngleGestureBaseOne;
	private double precisionRotation;
	private double storedPrecisionRotation;
	private double angleSnapBaseOne;
	private double angleSnapNextBaseOne;
	private double angleSnapProximity;
	private boolean rotateSnapped;
	private double maximumRotation;
	private double minimumRotation;
	private boolean useMinMaxRotation;
	private int minMaxRotationOutOfBounds;
	private float touchOffsetX;
	private float touchOffsetY;
	private int[][] renderOffset = null;
	private float touchXLocal;
	private float touchYLocal;
	private long quickTapTime;
	private double distortDialX;
	private double distortDialY;
	private boolean suppressRender;
	private double variableDialInner;
	private double variableDialOuter;
	private double imageDiameter;
	private double viewPortMinimumDimensionOverTwo;
	private double imageWidth;
	private double imageHeight;
	private boolean useVariableDialCurve;
	private boolean positiveCurve;
	private double imageRadius;
	private double viewPortWidth;
	private double viewPortHeight;
	private final Point centreOffset = new Point();
	private boolean useVariableDial;
	private MultipleTextures[] multipleTextures;
	boolean synchWithScale;
	private double storedImageRadius;

	//Fling Spin Variables
	private float spinStartSpeed;
	private float spinEndSpeed;
	private long spinDuration;
	private long spinEndTime;
	private double spinStartAngle;
	private long gestureDownTime;//Used for quick tap and fling behaviour
	private float flingDownTouchX;
	private float flingDownTouchY;
	private int flingDistanceThreshold;
	private long flingTimeThreshold;
	volatile boolean spinTriggered;
	private int lastObjectDirection;
	boolean spinExecutesFling;
	boolean spinTriggeredFromFling;
	Thread spinAnimationThread;
    /* Bottom of block field declarations */

	/* Top of block field declarations */
	public interface IHGRotate {

		void onDown(HGResult hgResult);
		void onPointerDown(HGResult hgResult);
		void onMove(HGResult hgResult);
		void onPointerUp(HGResult hgResult);
		void onUp(HGResult hgResult);

	}//End public interface IHGRotate

	//Top of block main angle wrapper used by outer class
	final public static class GetAngleWrapper {

		public GetAngleWrapper() {

			this.rotationGestureDirection = 0;
			this.rotationObjectDirection = 0;
			this.angleGestureBaseOne = 0;
			this.angleObjectBaseOne = 0;
			this.rotationGestureCount = 0;
			this.rotationObjectCount = 0;
			this.gestureTouchAngle = 0;

		}//End public GetAngleWrapper()

		private static int rotationGestureDirection;
		private static int rotationObjectDirection;
		private static int rotationGestureCount;
		private static int rotationObjectCount;
		private static double angleGestureBaseOne;
		private static double angleObjectBaseOne;
		private static double gestureTouchAngle;

		/* Accessors */
		public int getGestureRotationDirection() {return this.rotationGestureDirection;}
		public int getObjectRotationDirection() {return this.rotationObjectDirection;}
		public int getGestureRotationCount() {return this.rotationGestureCount;}
		public int getObjectRotationCount() {return this.rotationObjectCount;}
		public double getGestureAngleBaseOne() {return this.angleGestureBaseOne;}
		public double getObjectAngleBaseOne() {return this.angleObjectBaseOne;}
		public double getGestureTouchAngle() {return this.gestureTouchAngle;}

		/* Mutators */
		private void setGestureRotationDirection(final int rotationGestureDirection) {this.rotationGestureDirection = rotationGestureDirection;}
		private void setObjectRotationDirection(final int rotationObjectDirection) {this.rotationObjectDirection = rotationObjectDirection;}
		private void setGestureRotationCount(final int gestureRotationCount) {this.rotationGestureCount = gestureRotationCount;}
		private void setObjectRotationCount(final int objectRotationCount) {this.rotationObjectCount = objectRotationCount;}
		private void setGestureAngleBaseOne(final double angleBaseOne) {this.angleGestureBaseOne = angleBaseOne;}
		private void setObjectAngleBaseOne(final double precisionAngleBaseOne) {this.angleObjectBaseOne = precisionAngleBaseOne;}
		private void setGestureTouchAngle(final double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

	}//End final public static class GetAngleWrapper
	//Bottom of block main angle wrapper used by outer class


	public HGRotate(Context context, AttributeSet attrs) {
		super(context, attrs);

		setFields();

		if(hgTransformInfo == null) {

			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			HGRender.registerRenderer(ihgRender);

			if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

				imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
				a.recycle();
				hgRender = new HGRender(getResources(), imageResourceId);
				setRenderer(hgRender);
				setDimensionsAndMultipleTextures();
				setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

			}
			else {

				setDimensionsAndMultipleTextures();

			}//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

		}
		else if(hgTransformInfo != null) {

			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			HGRender.registerRenderer(ihgRender);

		}//End if(hgTransformInfo == null)

	}//End public HGRotate(Context context, AttributeSet attrs)


	private void setDimensionsAndMultipleTextures() {

		post(new Runnable() {
			@Override
			public void run() {
				setupMetrics();
				requestRender();
			}
		});

	}


	@Override
	public void onResume() {
		super.onResume();

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	}


	void setupMetrics() {

		this.imageWidth = HGRender.getImageHeightAndWidth()[0];
		this.imageHeight = HGRender.getImageHeightAndWidth()[1];
		viewCenterPoint.x = HGRender.getViewCenterPoint().x;
		viewCenterPoint.y = HGRender.getViewCenterPoint().y;
		this.multipleTextures = HGRender.getMultipleTextures();
		renderOffset = new int[multipleTextures.length][];

		if(multipleTextures[0] != null) {

			for(int i = 0; i < multipleTextures.length; i++) {

				renderOffset[i] = new int[2];
				renderOffset[i][0] = multipleTextures[i].getOffset().x;
				renderOffset[i][1] = -multipleTextures[i].getOffset().y;

			}

		}

		viewPortWidth = HGRender.getViewPortHeightAndWidth()[0];
		viewPortHeight = HGRender.getViewPortHeightAndWidth()[1];
		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		centreOffset.x = (int) ((viewPortWidth - imageWidth) / 2f);
		centreOffset.y = (int) ((viewPortHeight - imageHeight) / 2f);

		if(imageWidth < imageHeight) {

			imageDiameter = imageWidth;

		}
		else if(imageWidth > imageHeight) {

			imageDiameter = imageHeight;

		}
		else {

			imageDiameter = imageWidth;

		}//End if(imageWidth < imageHeight)

		imageRadius = imageDiameter / 2f;
		storedImageRadius = imageRadius;

		if(viewPortWidth < viewPortHeight) {

			viewPortMinimumDimensionOverTwo = viewPortWidth / 2f;

		}
		else if(viewPortWidth > viewPortHeight) {

			viewPortMinimumDimensionOverTwo = viewPortHeight / 2f;

		}
		else {

			viewPortMinimumDimensionOverTwo = viewPortWidth / 2f;

		}//End if(viewPortWidth < viewPortHeight)

	}//End void setupMetrics()


	private void setFields() {

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.ihgRotate = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.storedGestureAngleBaseOne = 0d;
		this.storedObjectCount = 0;
		this.currentGestureAngleBaseOne = 0d;
		this.fullObjectAngleBaseOne = 0d;
		this.cumulativeRotate = true;
		this.isSingleFinger = true;
		this.onDownAngleObjectCumulativeBaseOne = 0d;
		this.onUpAngleGestureBaseOne = 0d;
		this.precisionRotation = 1d;
		this.storedPrecisionRotation = 1d;
		this.angleSnapBaseOne = 0d;
		this.angleSnapNextBaseOne = 0d;
		this.angleSnapProximity = 0d;
		this.rotateSnapped = false;
		this.maximumRotation = 0d;
		this.minimumRotation = 0d;
		this.useMinMaxRotation = false;
		this.minMaxRotationOutOfBounds = 0;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.touchXLocal = 0f;
		this.touchYLocal = 0f;
		this.quickTapTime = 75L;
		this.distortDialX = 1d;
		this.distortDialY = 1d;
		this.suppressRender = false;
		this.variableDialInner = 0f;
		this.variableDialOuter = 0f;
		this.imageDiameter = 0d;
		this.viewPortMinimumDimensionOverTwo = 0f;
		this.imageWidth = 0d;
		this.imageHeight = 0d;
		this.useVariableDialCurve = false;
		this.positiveCurve = false;
		this.imageRadius = 0d;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.useVariableDial = false;
		this.multipleTextures = null;
		this.synchWithScale = true;

		//Fling Spin Variables
		this.spinStartSpeed = 0f;
		this.spinEndSpeed = 0f;
		this.spinDuration = 0L;
		this.spinEndTime = 0L;
		this.spinStartAngle = 0d;
		this.gestureDownTime = 0L;
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0L;
		this.spinTriggered = false;
		this.lastObjectDirection = 0;
		this.spinExecutesFling = false;
		this.spinTriggeredFromFling = false;
		this.spinAnimationThread = null;

	}//End private void setFields()


	public void registerCallback(IHGRotate ihgRotate) {this.ihgRotate = ihgRotate;}


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					if(isSingleFinger == true) {

						firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
						firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
						hgResult.setFirstTouchX(firstTouchX);
						hgResult.setFirstTouchY(firstTouchY);
						secondTouchX = touchOffsetX;
						secondTouchY = touchOffsetY;

					}//End if(isSingleFinger == true)

				}
				else if(touchPointerCount == 2) {

					if(isSingleFinger == false) {

						firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
						firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
						secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
						secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
						hgResult.setFirstTouchX(firstTouchX);
						hgResult.setFirstTouchY(firstTouchY);
						hgResult.setSecondTouchX(secondTouchX);
						hgResult.setSecondTouchY(secondTouchY);

					}//End if(isSingleFinger == false)

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount < 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else /* if(touchPointerCount >= 2) */ {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount < 2)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					if(isSingleFinger == true) {

						firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
						firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
						secondTouchX = touchOffsetX;
						secondTouchY = touchOffsetX;
						hgResult.setFirstTouchX(firstTouchX);
						hgResult.setFirstTouchY(firstTouchY);

					}//End if(isSingleFinger == true)

				}
				else if(touchPointerCount == 2) {

					if(isSingleFinger == false) {

						firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
						firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
						secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
						secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
						hgResult.setFirstTouchX(firstTouchX);
						hgResult.setFirstTouchY(firstTouchY);
						hgResult.setSecondTouchX(secondTouchX);
						hgResult.setSecondTouchY(secondTouchY);

					}//End if(isSingleFinger == false)

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block rotate functions */
	private HGResult doDownRotate() {

		spinTriggered = false;
		hgResult.setSpinTriggered(false);

		//Prepare touches for single or dual touch
		if(touchPointerCount == 1 && isSingleFinger == true) {

			if(hgTransformInfo == null) {

				touchXLocal = flingDownTouchX = firstTouchX;
				touchYLocal = flingDownTouchY = firstTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = flingDownTouchX = (firstTouchX + -hgTransformInfo.getCurrentTransX());
				touchYLocal = flingDownTouchY = (firstTouchY - -hgTransformInfo.getCurrentTransY());

			}//End if(hgTransformInfo == null)

		}
		else if(touchPointerCount == 2 && isSingleFinger == false) {

			if(hgTransformInfo == null) {

				touchXLocal = secondTouchX;
				touchYLocal = secondTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = secondTouchX + -hgTransformInfo.getCurrentTransX();
				touchYLocal = secondTouchY - -hgTransformInfo.getCurrentTransY();

			}//End if(hgTransformInfo == null)

		}
		else {

			return hgResult;

		}//End if(touchPointerCount == 1 && isSingleFinger == true)

		if(precisionRotation != 0) {

			final Point touchPoint = new Point((int) touchXLocal, (int) touchYLocal);
			getAngleWrapper.setGestureTouchAngle(HGGeometry.getAngleFromPoint(viewCenterPoint, touchPoint));

			if(cumulativeRotate == true) {

				onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

				if(useVariableDial == true) {

					fullObjectAngleBaseOne = fullObjectAngleBaseOne * precisionRotation;

				}

			}
			else if(cumulativeRotate == false) {

				minMaxRotationOutOfBounds = 0;
				onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

				if(useVariableDial == true) {

					fullObjectAngleBaseOne = getAngleWrapper.getGestureTouchAngle();

				}
				else if(useVariableDial == false) {

					fullObjectAngleBaseOne = getAngleWrapper.getGestureTouchAngle() / precisionRotation;

				}//End if(useVariableDial == true)

			}//End if(cumulativeRotate == true)

			if(useVariableDial == false) {hgResult.setVariablePrecision(storedPrecisionRotation);}
			setReturnType();
			spinStartAngle = fullObjectAngleBaseOne;

		}//End if(precisionRotation != 0)

		return hgResult;

	}//End private HGResult doDownRotate()


	private HGResult doMoveRotate() {

		//Prepare touches for single or dual touch
		if(touchPointerCount == 1 && isSingleFinger == true) {

			if(hgTransformInfo == null) {

				touchXLocal = firstTouchX;
				touchYLocal = firstTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = firstTouchX + -hgTransformInfo.getCurrentTransX();
				touchYLocal = firstTouchY - -hgTransformInfo.getCurrentTransY();

			}//End if(hgTransformInfo == null)

		}
		else if(touchPointerCount == 2 && isSingleFinger == false) {

			if(hgTransformInfo == null) {

				touchXLocal = secondTouchX;
				touchYLocal = secondTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = secondTouchX + -hgTransformInfo.getCurrentTransX();
				touchYLocal = secondTouchY - -hgTransformInfo.getCurrentTransY();

			}//End if(hgTransformInfo == null)

		}
		else {

			return hgResult;

		}//End if(touchPointerCount == 1 && isSingleFinger == true)

		final Point touchPoint = new Point((int) touchXLocal, (int) touchYLocal);
		getAngleWrapper.setGestureTouchAngle(HGGeometry.getAngleFromPoint(viewCenterPoint, touchPoint));

		setReturnType();

		return hgResult;

	}//End private HGResult doMoveRotate()


	private HGResult doUpRotate() {

		//Prepare touches for single or dual touch
		if(touchPointerCount == 1 && isSingleFinger == true) {

			if(hgTransformInfo == null) {

				touchXLocal = firstTouchX;
				touchYLocal = firstTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = firstTouchX + -hgTransformInfo.getCurrentTransX();
				touchYLocal = firstTouchY - -hgTransformInfo.getCurrentTransY();

			}//End if(hgTransformInfo == null)

		}
		else if(touchPointerCount == 2 && isSingleFinger == false) {

			if(hgTransformInfo == null) {

				touchXLocal = secondTouchX;
				touchYLocal = secondTouchY;

			}
			else if(hgTransformInfo != null) {

				touchXLocal = secondTouchX + -hgTransformInfo.getCurrentTransX();
				touchYLocal = secondTouchY - -hgTransformInfo.getCurrentTransY();

			}//End if(hgTransformInfo == null)

		}
		else {

			return hgResult;

		}//End if(touchPointerCount == 1 && isSingleFinger == true);

		final Point touchPoint = new Point((int) touchXLocal, (int) touchYLocal);
		getAngleWrapper.setGestureTouchAngle(HGGeometry.getAngleFromPoint(viewCenterPoint, touchPoint));

		if(precisionRotation != 0) {

			onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle())) % 1;
			if(useVariableDial == true) {precisionRotation = 1f;}
			setReturnType();

		}//End if(precisionRotation != 0)

		setSpinStatus();

		if(spinTriggered == true) {

			storedImageRadius = imageRadius;
			hgResult.setQuickTap(false);
			spinAnimationThread = new Thread(this);
			spinAnimationThread.start();

		}
		else {

			imageRadius = storedImageRadius;

		}//End if(spinTriggered == true)

		hgResult.setGestureTouchAngle(getAngleWrapper.getGestureTouchAngle());
		hgResult.setGestureRotationDirection(0);
		hgResult.setObjectRotationDirection(0);
		if(useVariableDial == false) {hgResult.setVariablePrecision(storedPrecisionRotation);}

		return hgResult;

	}//End private HGResult doUpRotate()
	/* Bottom of block rotate functions */


	private void setReturnType() {

		if(useVariableDial == false) {

			if(useMinMaxRotation == false) {

				hgResult.setGestureRotationDirection(setRotationAngleGestureAndReturnDirection());

			}
			else if(useMinMaxRotation == true) {

				hgResult.setGestureRotationDirection(setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour());

			}//End if(useMinMaxRotation == false)

			hgResult.setGestureRotationCount(getAngleWrapper.getGestureRotationCount());
			hgResult.setGestureAngleBaseOne(getAngleWrapper.getGestureAngleBaseOne());
			hgResult.setObjectRotationDirection(lastObjectDirection);
			hgResult.setObjectRotationCount(getAngleWrapper.getObjectRotationCount());
			hgResult.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
			hgResult.setRotateSnapped(rotateSnapped);
			hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			hgResult.setVariablePrecision(storedPrecisionRotation);
			calculateObjectAngleBaseOne();

			if(angleSnapBaseOne != 0) {

				checkNextAngleSnapBaseOne();

			}

			if(suppressRender == false) {

				requestRender();

			}

		}
		else if(useVariableDial == true) {

			if(useMinMaxRotation == false) {

				getAngleWrapper.setObjectRotationDirection(setVariableDialAngleAndReturnDirection());

			}
			else if(useMinMaxRotation == true) {

				getAngleWrapper.setObjectRotationDirection(setVariableDialAngleAndReturnDirectionForMinMaxBehaviour());

			}//End if(useMinMaxRotation == false)

			hgResult.setGestureAngleBaseOne(getAngleWrapper.getGestureTouchAngle());
			hgResult.setObjectRotationCount(getAngleWrapper.getObjectRotationCount());
			hgResult.setObjectRotationDirection(lastObjectDirection);
			hgResult.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
			hgResult.setRotateSnapped(rotateSnapped);
			hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);

			if(angleSnapBaseOne != 0) {

				checkNextAngleSnapBaseOne();

			}

			if(suppressRender == false) {

				requestRender();

			}

		}//End if(Math.abs(precisionRotation) != 1 && useVariableDial == false)

		if(hgTransformInfo != null) {

			hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());

		}

		hgResult.setGestureTouchAngle(getAngleWrapper.getGestureTouchAngle());

	}//End private void setReturnType()
    /* Bottom of block rotate functions */


	private void setSpinStatus() {

		hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
		final long currentTime = System.currentTimeMillis();

		if(spinTriggeredFromFling == false) {

			if(flingTimeThreshold != 0 && isSingleFinger == true) {

				//flingDistanceThreshold IS met
				if(getTwoFingerDistance(flingDownTouchX, flingDownTouchY, firstTouchX, firstTouchY) >= flingDistanceThreshold) {

					//If flingTimeThreshold met
					if(currentTime < gestureDownTime + flingTimeThreshold) {

						spinTriggered = true;
						hgResult.setSpinTriggered(true);
						spinEndTime = currentTime + spinDuration;

					}//End if(currentTime < gestureDownTime + flingTimeThreshold)

				}
				else {

					spinTriggered = false;
					hgResult.setSpinTriggered(false);

				}//End if(getTwoFingerDistance(flingDownTouchX, flingDownTouchY, firstTouchX, firstTouchY) < flingDistanceThreshold)

			}//End if(flingTimeThreshold != 0 && isSingleFinger == true)

		}//End if(spinTriggeredFromFling == false)

	}//End private void setSpinStatus()


	/* Top of block main functions */
	private int setRotationAngleGestureAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullObjectAngleBaseOne -= (angleDifference + 1f) % 1f;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullObjectAngleBaseOne += -angleDifference % 1f;

			}

			if(precisionRotation < 0) {

				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

			}
			else if(precisionRotation > 0) {

				getAngleWrapper.setObjectRotationDirection(returnValue[0]);

			}
			else {

				getAngleWrapper.setObjectRotationDirection(0);

			}//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(precisionRotation > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else {

					getAngleWrapper.setObjectRotationDirection(0);

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
		getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
		storedObjectCount = (int) (fullObjectAngleBaseOne * precisionRotation);
		getAngleWrapper.setObjectRotationCount(storedObjectCount);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirection()


	private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullObjectAngleBaseOne -= (angleDifference + 1f) % 1f;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullObjectAngleBaseOne += -angleDifference % 1f;

			}

			if(precisionRotation < 0) {

				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

			}
			else if(precisionRotation > 0) {

				getAngleWrapper.setObjectRotationDirection(returnValue[0]);

			}
			else {

				getAngleWrapper.setObjectRotationDirection(0);

			}//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(precisionRotation > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else {

					getAngleWrapper.setObjectRotationDirection(0);

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		if(precisionRotation > 0) {

			if(fullObjectAngleBaseOne < minimumRotation / precisionRotation) {

				if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = minimumRotation / precisionRotation;

			}
			else if(fullObjectAngleBaseOne > maximumRotation / precisionRotation) {

				if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = maximumRotation / precisionRotation;

			}//End if(fullObjectAngleBaseOne < minimumRotation / precisionRotation)

		}
		else if(precisionRotation < 0) {

			if(-fullObjectAngleBaseOne < -(minimumRotation / precisionRotation)) {

				if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = (minimumRotation / precisionRotation);

			}
			else if(-fullObjectAngleBaseOne > -(maximumRotation / precisionRotation)) {

				if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = (maximumRotation / precisionRotation);

			}//End if(-fullObjectAngleBaseOne < -(minimumRotation / precisionRotation))

		}//End if(precisionRotation > 0)

		getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
		getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
		storedObjectCount = (int) (fullObjectAngleBaseOne * precisionRotation);
		getAngleWrapper.setObjectRotationCount(storedObjectCount);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour()


	private int setVariableDialAngleAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne += -(angleDifference * variablePrecision);

				}

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(variablePrecision < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(-lastObjectDirection);

				}//End if(variablePrecision > 0)

				hgResult.setObjectRotationDirection(lastObjectDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
		getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne % 1f);
		storedObjectCount = (int) (fullObjectAngleBaseOne);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setVariableDialAngleAndReturnDirection()


	private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne += -(angleDifference * variablePrecision);

				}

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(variablePrecision < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(-lastObjectDirection);

				}//End if(variablePrecision > 0)

				hgResult.setObjectRotationDirection(lastObjectDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		if(fullObjectAngleBaseOne < minimumRotation) {

			if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = -1;
			hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullObjectAngleBaseOne = minimumRotation;

		}
		else if(fullObjectAngleBaseOne > maximumRotation) {

			if(spinTriggered == true) {hgResult.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = 1;
			hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullObjectAngleBaseOne = maximumRotation;

		}//End if(fullGestureAngleBaseOne < minimumRotation)

		if(minMaxRotationOutOfBounds != 0) {

			if(variablePrecision > 0) {

				if(fullObjectAngleBaseOne == minimumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}
				else if(fullObjectAngleBaseOne == maximumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}//End if(fullGestureAngleBaseOne == minimumRotation)

			}
			else if(variablePrecision < 0) {

				if(fullObjectAngleBaseOne == minimumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}
				else if(fullObjectAngleBaseOne == maximumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}//End if(fullGestureAngleBaseOne == minimumRotation)

			}//End if(variablePrecision > 0)

		}//End if(minMaxRotationOutOfBounds != 0)

		getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
		getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne % 1f);
		storedObjectCount = (int) (fullObjectAngleBaseOne);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour()


	private double getVariablePrecision() {

		final double distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchXLocal, touchYLocal);
		hgResult.setTwoFingerDistance(distanceFromCenter);
		double[] variablePrecision = new double[1];

		if(distanceFromCenter <= imageRadius) {

			variablePrecision[0] = (variableDialInner - (((distanceFromCenter / viewPortMinimumDimensionOverTwo) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;
			hgResult.setVariablePrecision(variablePrecision[0]);

		}

		return variablePrecision[0];

	}//End private float getVariablePrecision()


	private double getVariablePrecisionCurve() {

		final double distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchXLocal, touchYLocal);
		hgResult.setTwoFingerDistance(distanceFromCenter);
		double[] variablePrecision = new double[1];

		if(distanceFromCenter <= imageRadius) {

			final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);

			if(positiveCurve == false) {

				variablePrecision[0] = -rangeDiff * (1f - ((float) Math.sin((distanceFromCenter / imageDiameter) * Math.PI))) + variableDialOuter;
				hgResult.setVariablePrecision(variablePrecision[0]);

			}
			else if(positiveCurve == true) {

				variablePrecision[0] = -rangeDiff * ((float) Math.cos((distanceFromCenter / imageDiameter) * Math.PI)) + variableDialOuter;
				hgResult.setVariablePrecision(variablePrecision[0]);

			}//End if(positiveCurve == false)

		}//End if(distanceFromCenter <= imageRadius)

		return variablePrecision[0];

	}//End private double getVariablePrecisionCurve()


	private double getVariablePrecisionInternal() {

		final double distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchXLocal, touchYLocal);
		hgResult.setTwoFingerDistance(distanceFromCenter);
		double[] variablePrecision = new double[1];

		if(distanceFromCenter <= imageRadius) {

			variablePrecision[0] = (variableDialInner - (((distanceFromCenter / viewPortMinimumDimensionOverTwo) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;
			hgResult.setVariablePrecision(variablePrecision[0]);

		}

		return variablePrecision[0];

	}//End private double getVariablePrecisionInternal()


	private double getVariablePrecisionCurveInternal() {

		final double distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchXLocal, touchYLocal);
		hgResult.setTwoFingerDistance(distanceFromCenter);
		double[] variablePrecision = new double[1];

		if(distanceFromCenter <= imageRadius) {

			final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);

			if(positiveCurve == false) {

				variablePrecision[0] = -rangeDiff * (1f - ((float) Math.sin((distanceFromCenter / imageDiameter) * Math.PI))) + variableDialOuter;
				hgResult.setVariablePrecision(variablePrecision[0]);

			}
			else if(positiveCurve == true) {

				variablePrecision[0] = -rangeDiff * ((float) Math.cos((distanceFromCenter / imageDiameter) * Math.PI)) + variableDialOuter;
				hgResult.setVariablePrecision(variablePrecision[0]);

			}//End if(positiveCurve == false)

		}//End if(distanceFromCenter <= imageRadius)

		return variablePrecision[0];

	}//End private double getVariablePrecisionCurveInternal()
	/* Bottom of block main functions */


	private void calculateObjectAngleBaseOne() {getAngleWrapper.setObjectAngleBaseOne((getGestureFullAngle() * precisionRotation) % 1);}
    /* Bottom of block Angle methods */


	/* Top of block Accessors */
	public long getQuickTapTime() {return this.quickTapTime;}
	public boolean getCumulativeRotate() {return this.cumulativeRotate;}
	public boolean getIsSingleFinger() {return this.isSingleFinger;}
	public double getPrecisionRotation() {return this.precisionRotation;}
	public double getAngleSnapBaseOne() {return this.angleSnapBaseOne;}
	public double getAngleSnapProximity() {return this.angleSnapProximity;}
	public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public double getDistortDialX() {return this.distortDialX;}
	public double getDistortDialY() {return this.distortDialY;}
	public boolean hasAngleSnapped() {return rotateSnapped;}
	public boolean getSuppressRender() {return this.suppressRender;}
	public double getVariableDialInner() {return this.variableDialInner;}
	public double getVariableDialOuter() {return this.variableDialOuter;}
	public boolean getUseVariableDial() {return this.useVariableDial;}
	public boolean getUseVariableDialCurve() {return this.useVariableDialCurve;}
	public boolean getPositiveCurve() {return this.positiveCurve;}
	public int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
	public long getFlingTimeThreshold() {return this.flingTimeThreshold;}
	public float getSpinStartSpeed() {return this.spinStartSpeed;}
	public float getSpinEndSpeed() {return this.spinEndSpeed;}
	public long getSpinDuration() {return this.spinDuration;}
	public boolean getSpinTriggersFling() {return this.spinExecutesFling;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
	public void setIsSingleFinger(final boolean isSingleFinger) {this.isSingleFinger = isSingleFinger;}
	public void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {this.useVariableDialCurve = useVariableDialCurve; this.positiveCurve = positiveCurve;}
	public void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {this.flingDistanceThreshold = flingDistanceThreshold; this.flingTimeThreshold = flingTimeThreshold;}
	public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {this.spinStartSpeed = spinStartSpeed; this.spinEndSpeed = spinEndSpeed; this.spinDuration = spinDuration;}
	void setImageDiameter(final double imageDiameter) {this.imageDiameter = imageDiameter; this.imageRadius = imageDiameter / 2;}
	void setSpinTriggersFling(final boolean spinExecutesFling) {this.spinExecutesFling = spinExecutesFling;}


	public void setPrecisionRotation(final double precisionRotation) {

		this.storedPrecisionRotation = precisionRotation;

		if(this.precisionRotation != 0 && precisionRotation != 0) {

			final double currentObjectAngle = fullObjectAngleBaseOne * this.precisionRotation;
			final double newObjectAngle = fullObjectAngleBaseOne * precisionRotation;
			fullObjectAngleBaseOne += ((currentObjectAngle - newObjectAngle) / precisionRotation);

		}

		this.precisionRotation = precisionRotation;

	}//End public void setPrecisionRotation(final double precisionRotation)


	public void setAngleSnapBaseOne(final double angleSnapBaseOne, final double angleSnapProximity) {

		this.angleSnapBaseOne = (angleSnapBaseOne) % 1;
		this.angleSnapProximity = (angleSnapProximity) % 1;
		if(angleSnapProximity > angleSnapBaseOne / 2f) {this.angleSnapProximity = angleSnapBaseOne / 2;}

		if(hgTransformInfo != null) {

			hgTransformInfo.setAngleSnapBaseOne(this.angleSnapBaseOne);

		}

	}//End public void setAngleSnapBaseOne(final double angleSnapBaseOne, final double angleSnapProximity)


	public void doRapidDial(final MultipleTextures[] multipleTextures) {

		HGRender.setUseRapidDial(true);
		this.multipleTextures = multipleTextures;
		requestRender();

	}


	public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {

		this.touchOffsetX = touchOffsetX;
		this.touchOffsetY = touchOffsetY;

	}//End public void setTouchOffset(final float touchOffsetX, final float touchOffsetY)


	void setMultipleTextures(final MultipleTextures[] multipleTextures) {this.multipleTextures = multipleTextures;}
	public void setDistortDialX(final double distortDialX) {this.distortDialX = distortDialX;}
	public void setDistortDialY(final double distortDialY) {this.distortDialY = distortDialY;}
	public void setSuppressRender(final boolean suppressRender) {this.suppressRender = suppressRender;}


	public void setVariableDial(final double variableDialInner, final double variableDialOuter, final boolean useVariableDial) {

		this.variableDialInner = variableDialInner;
		this.variableDialOuter = variableDialOuter;
		this.useVariableDial = useVariableDial;

	}//End public void setVariableDial(final double variableDialInner, final double variableDialOuter, final boolean useVariableDial)
    /* Bottom of block Mutators */


	/* Top of block behavioural methods */
	private void checkNextAngleSnapBaseOne() {

		final double tempAngleSnap = Math.round(getAngleWrapper.getObjectAngleBaseOne() / angleSnapBaseOne);
		angleSnapNextBaseOne = tempAngleSnap * angleSnapBaseOne;

		if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity) {

			rotateSnapped = true;
			hgResult.setRotateSnapped(true);

		}
		else {

			angleSnapNextBaseOne = getAngleWrapper.getObjectAngleBaseOne();
			rotateSnapped = false;
			hgResult.setRotateSnapped(false);

		}//End if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity)

		if(hgTransformInfo != null) {

			hgTransformInfo.setAngleSnapBaseOne(this.angleSnapBaseOne);
			hgTransformInfo.setAngleSnapNextBaseOne(this.angleSnapNextBaseOne);

		}

	}//End private void checkNextAngleSnapBaseOne()


	public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {

		this.minimumRotation = minimumRotation;
		this.maximumRotation = maximumRotation;
		this.useMinMaxRotation = useMinMaxRotation;

		if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation) {

			this.useMinMaxRotation = false;

		}
		else {

			this.useMinMaxRotation = true;

		}//End if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation)

		if(this.useMinMaxRotation == false) {minMaxRotationOutOfBounds = 0; hgResult.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

	}//End public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation)


	private double getVariablePrecision(final Point touchPointLocal) {

		final double distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchPointLocal.x, touchPointLocal.y);
		return (((this.imageDiameter - Math.abs(distanceFromCenter)) / (double) viewCenterPoint.x) * ((variableDialInner - variableDialOuter)) + variableDialOuter);

	}//End private double getVariablePrecision(final Point touchPointLocal)
    /* Bottom of block behavioural methods */


	/* Top of block convenience methods */
	public double getGestureFullAngle() {return this.fullObjectAngleBaseOne;}
	private Point getPointFromAngle(final double angle) {return HGGeometry.getPointFromAngle(angle, viewCenterPoint.x);}
	void doManualObjectDialInternal(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}


	public void doManualGestureDial(final double manualDial) {

		fullObjectAngleBaseOne = manualDial;
		getAngleWrapper.setGestureRotationCount((int) manualDial);
		getAngleWrapper.setGestureAngleBaseOne(Math.round((manualDial % 1) * 1000000.0f) / 1000000.0f);
		getAngleWrapper.setObjectRotationCount((int) (manualDial * precisionRotation));
		calculateObjectAngleBaseOne();
		getAngleWrapper.setObjectAngleBaseOne(Math.round((getAngleWrapper.getObjectAngleBaseOne()) * 1000000.0f) / 1000000.0f);
		onDownAngleObjectCumulativeBaseOne = 0;
		onUpAngleGestureBaseOne = 0;

		if(hgTransformInfo != null) {

			hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());

		}

		doUpRotate();

		if(suppressRender == false) {

			requestRender();

		}

	}//End public void doManualGestureDial(final double manualDial)


	public void doManualObjectDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}


	public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection) {

		this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeed = spinEndSpeed;
		this.spinDuration = spinDuration;
		this.lastObjectDirection = objectDirection;
		spinEndTime = System.currentTimeMillis() + spinDuration;
		spinTriggered = true;
		spinAnimationThread = new Thread(this);
		spinAnimationThread.start();

	}//End public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection)


	void triggerSpinFromFling() {

		spinTriggeredFromFling = true;
		double spinStartSpeedInternal = 0;

		if(useVariableDial == false) {

			spinStartSpeedInternal = (float) -precisionRotation;

		}
		else if(useVariableDial == true) {

			if(useVariableDialCurve == false) {

				spinStartSpeedInternal = -getVariablePrecisionInternal();

			}
			else if(useVariableDialCurve == true) {

				spinStartSpeedInternal = -getVariablePrecisionCurveInternal();

			}//End if(useVariableDialCurve == false)

		}//End if(useVariableDial == false)

		this.spinStartSpeed = (float) spinStartSpeedInternal;
		spinEndTime = System.currentTimeMillis() + spinDuration;
		spinTriggered = true;
		spinAnimationThread = new Thread(this);
		spinAnimationThread.start();

	}//End void triggerSpinFromFling()
    /* Bottom of block convenience methods */


	@Override
	public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView) {

		final float[] scratch = new float[16];
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.translateM(rotationMatrix, 0, centerPoint.x + renderOffset[activeTexture][0] + centreOffset.x, centerPoint.y + renderOffset[activeTexture][1] + centreOffset.y, 0f);
		Matrix.scaleM(rotationMatrix, 0, (float) distortDialX, (float) distortDialY, 1f);

		if(HGRender.getUseRapidDial() == true) {

			Matrix.rotateM(rotationMatrix, 0, -((float) this.multipleTextures[activeTexture].getRapidDialAngle() * 360f), 0.0f, 0.0f, 1.0f);
			this.multipleTextures[activeTexture].getRapidDialAngle();

		}
		else if(angleSnapBaseOne == 0) {

			if(multipleTextures.length == 1) {

				Matrix.rotateM(rotationMatrix, 0, - (float) (getAngleWrapper.getObjectAngleBaseOne() * 360), 0.0f, 0.0f, 1.0f);

			}
			else {

				Matrix.rotateM(rotationMatrix, 0, - (float) (((getAngleWrapper.getGestureRotationCount() + getAngleWrapper.getGestureAngleBaseOne()) * multipleTextures[activeTexture].getPrecision()) * 360d), 0.0f, 0.0f, 1.0f);

			}

		}
		else if(angleSnapBaseOne != 0) {

			if(multipleTextures.length == 1) {

				Matrix.rotateM(rotationMatrix, 0, - (float) (angleSnapNextBaseOne * 360), 0.0f, 0.0f, 1.0f);

			}
			else {

				Matrix.rotateM(rotationMatrix, 0, - (float) ((getAngleWrapper.getGestureRotationCount() + angleSnapNextBaseOne) * multipleTextures[activeTexture].getPrecision() * 360d), 0.0f, 0.0f, 1.0f);

			}

		}//End if(rapidDial != 0)

		Matrix.translateM(rotationMatrix, 0, - centerPoint.x, - centerPoint.y, 0f);
		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, rotationMatrix, 0);
		square.draw(scratch, activeTexture);

	}//End public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView)


	//HGRender accessors and mutators
	static HGTransformInfo getHgTransformInfo() {return HGRotate.hgTransformInfo;}
	static void setHgTransformInfo(final HGTransformInfo hgTransformInfo) {HGRotate.hgTransformInfo = hgTransformInfo;}


	public void performQuickTap() {

		hgResult.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgRotate.onUp(doUpRotate());
				hgResult.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.ihgRender = null;
		this.imageResourceId = 0;
		this.ihgRotate = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.storedGestureAngleBaseOne = 0d;
		this.storedObjectCount = 0;
		this.currentGestureAngleBaseOne = 0d;
		this.fullObjectAngleBaseOne = 0d;
		this.cumulativeRotate = true;
		this.isSingleFinger = true;
		this.onDownAngleObjectCumulativeBaseOne = 0d;
		this.onUpAngleGestureBaseOne = 0d;
		this.precisionRotation = 1d;
		this.storedPrecisionRotation = 0d;
		this.angleSnapBaseOne = 0d;
		this.angleSnapNextBaseOne = 0d;
		this.angleSnapProximity = 0d;
		this.rotateSnapped = false;
		this.maximumRotation = 0d;
		this.minimumRotation = 0d;
		this.useMinMaxRotation = false;
		this.minMaxRotationOutOfBounds = 0;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.touchXLocal = 0f;
		this.touchYLocal = 0f;
		this.quickTapTime = 75L;
		this.distortDialX = 1d;
		this.distortDialY = 1d;
		this.suppressRender = false;
		this.variableDialInner = 0f;
		this.variableDialOuter = 0f;
		this.imageDiameter = 0d;
		this.viewPortMinimumDimensionOverTwo = 0f;
		this.imageWidth = 0d;
		this.imageHeight = 0d;
		this.useVariableDialCurve = false;
		this.positiveCurve = false;
		this.imageRadius = 0d;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.useVariableDial = false;
		this.multipleTextures = null;
		this.synchWithScale = true;

		//Fling Spin Variables
		this.spinStartSpeed = 0f;
		this.spinEndSpeed = 0f;
		this.spinDuration = 0L;
		this.spinEndTime = 0L;
		this.spinStartAngle = 0d;
		this.gestureDownTime = 0L;//Used for quick tap and fling behaviour
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0L;
		this.spinTriggered = false;
		this.lastObjectDirection = 0;
		this.spinExecutesFling = false;
		this.spinTriggeredFromFling = false;
		this.spinAnimationThread = null;

	}//End protected void onDetachedFromWindow()


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	private OnTouchListener getOnTouchListerField() {

		onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				hgTouchEvent(v, event);

				return true;

			}

		};

		return onTouchListener;

	}//End private OnTouchListener getOnTouchListerField()


	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	private void hgTouchEvent(final View v, final MotionEvent event) {

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgResult.setQuickTap(false);
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap and fling time threshold tolerance
			final long currentTime = System.currentTimeMillis();

			if(currentTime < gestureDownTime + quickTapTime) {

				hgResult.setQuickTap(true);
				hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
				ihgRotate.onUp(doUpRotate());
				return;

			}//End if(currentTime < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		if(getPrecisionRotation() != 0) {

			final int action = event.getAction() & MotionEvent.ACTION_MASK;

			switch(action) {

				case MotionEvent.ACTION_MOVE: {

					setMoveTouch(event);
					hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
					ihgRotate.onMove(doMoveRotate());

					break;

				}
				case MotionEvent.ACTION_DOWN: {

					setDownTouch(event);
					hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
					ihgRotate.onDown(doDownRotate());

					break;

				}
				case MotionEvent.ACTION_POINTER_DOWN: {

					setDownTouch(event);
					hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
					ihgRotate.onDown(doDownRotate());

					break;

				}
				case MotionEvent.ACTION_POINTER_UP: {

					setUpTouch(event);
					hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
					ihgRotate.onUp(doUpRotate());

					break;

				}
				case MotionEvent.ACTION_UP: {

					setUpTouch(event);
					hgResult.setWhatGesture(HGResult.ROTATE_GESTURE);
					ihgRotate.onUp(doUpRotate());

					break;

				}
				default:

					break;

			}//End switch(action)

		}//End if(getPrecisionRotation() != 0)

	}//End private void hgTouchEvent(final View v, final MotionEvent event)


	@Override
	public void run() {

		hgResult.setSpinTriggered(true);

		if(useVariableDial == true) {

			if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0)) {

				//Uses manual start spin speed, end speed and parsed spin duration.
				doSpinAnimationVariablePrecision();

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration != 0) {

				//Uses parsed duration start speed relative to fling and an end speed of 0.
				doDefaultSpinAnimationVariablePrecision();

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration == 0) {

				//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
				post(new Runnable() {
					@Override
					public void run() {

						if(ihgRotate != null) {ihgRotate.onUp(hgResult);}

					}
				});

			}//End if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0))

		}
		else if(useVariableDial == false) {

			if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0)) {

				//Uses manual start spin speed, end speed and parsed spin duration.
				doSpinAnimationFixedPrecision();

				post(new Runnable() {
					@Override
					public void run() {

						setReturnType();
						if(ihgRotate != null) {ihgRotate.onMove(hgResult);}

					}
				});

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration != 0) {

				//Uses parsed duration start speed relative to fling and an end speed of 0.
				doDefaultSpinAnimationFixedPrecision();

				post(new Runnable() {
					@Override
					public void run() {

						setReturnType();
						if(ihgRotate != null) {ihgRotate.onMove(hgResult);}

					}
				});

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration == 0) {

				//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
				post(new Runnable() {
					@Override
					public void run() {

						if(ihgRotate != null) {ihgRotate.onUp(hgResult);}

					}
				});

			}//End if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0))

		}//End if(useVariableDial == true)

	}//End public void run()


	/* Top of block Fling Functions called on thread */
	private void doSpinAnimationVariablePrecision() {

		imageRadius = 0;

		//For when spin rate slows down.
		if(spinStartSpeed > spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

				if(spinCurrentSpeed - spinEndSpeed > 0) {

					if(currentTime + 5L < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();
						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);
						hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

						if(hgTransformInfo != null) {

							hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
							if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

						}
						else if(hgTransformInfo == null) {

							post(new Runnable() {
								@Override
								public void run() {

									if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
									requestRender();

								}
							});

						}//End if(hgTransformInfo != null)

					}//End if(currentTime + 5L < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

					spinTriggered = false;
					hgResult.setSpinTriggered(false);
					hgRotateHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed - spinEndSpeed > 0)

			}//End while(flingTriggered == true)

			hgRotateHandler.sendEmptyMessage(0);

		}
		//For when spin rate speeds up.
		else if(spinStartSpeed < spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = spinStartSpeed + (Math.abs(((float) (spinEndTime - currentTime) / spinDuration) - 1) * (spinEndSpeed - spinStartSpeed));

				if(spinCurrentSpeed < spinEndSpeed) {

					//Update view every 10 milliseconds
					if(currentTime + 5L < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();
						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);
						hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

						if(hgTransformInfo != null) {

							hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
							if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

						}
						else if(hgTransformInfo == null) {

							post(new Runnable() {
								@Override
								public void run() {

									if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
									requestRender();

								}
							});

						}//End if(hgTransformInfo != null)

					}//End if(currentTime + 5L < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed < spinEndSpeed)) */ {

					spinTriggered = false;
					hgResult.setSpinTriggered(false);
					hgRotateHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed < spinEndSpeed)

			}//End while(flingTriggered == true)

			hgRotateHandler.sendEmptyMessage(0);

		}//End if(spinStartSpeed > spinEndSpeed)

	}//End private void doSpinAnimationVariablePrecision()


	private void doDefaultSpinAnimationVariablePrecision() {

		imageRadius = 0;
		long currentTime = System.currentTimeMillis();
		final float spinStartSpeed = ((1000f / (currentTime - gestureDownTime))) * (float) Math.abs(spinStartAngle - fullObjectAngleBaseOne);

		while(spinTriggered == true) {

			final float spinCurrentSpeed = (((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed);

			if(spinCurrentSpeed - spinEndSpeed > 0) {

				if(currentTime + 5L < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();
					fullObjectAngleBaseOne += ((0.01f * spinCurrentSpeed) * lastObjectDirection);
					hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

					if(hgTransformInfo != null) {

						hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());//Necessary for manual triggerfling
						if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

					}
					else if(hgTransformInfo == null) {

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
								requestRender();

							}
						});

					}//End else if(hgTransformInfo != null)

				}//End if(currentTime + 5L < System.currentTimeMillis())

			}
			else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

				spinTriggered = false;
				hgResult.setSpinTriggered(false);
				hgRotateHandler.sendEmptyMessage(0);

			}//End if(spinCurrentSpeed - spinEndSpeed > 0)

		}//End while(flingTriggered == true)

		hgRotateHandler.sendEmptyMessage(0);

	}//End private void doDefaultSpinAnimationVariablePrecision()


	private void doSpinAnimationFixedPrecision() {

		imageRadius = 0;
		//For when spin rate slows down.
		if(spinStartSpeed > spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

				if(spinCurrentSpeed - spinEndSpeed > 0) {

					if(currentTime + 5L < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();

						if(precisionRotation > 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

						}
						else if(precisionRotation < 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

						}//End if(precisionRotation > 0)

						hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

						if(hgTransformInfo != null) {

							hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
							if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

						}
						else if(hgTransformInfo == null) {

							post(new Runnable() {
								@Override
								public void run() {

									if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
									requestRender();

								}
							});

						}//End if(hgTransformInfo != null)

					}//End if(currentTime + 5L < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

					spinTriggered = false;
					hgResult.setSpinTriggered(false);
					hgRotateHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed - spinEndSpeed > 0)

			}//End while(flingTriggered == true)

			calculateObjectAngleBaseOne();
			hgRotateHandler.sendEmptyMessage(0);

		}
		//For when spin rate speeds up.
		else if(spinStartSpeed < spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = spinStartSpeed + (Math.abs(((float) (spinEndTime - currentTime) / spinDuration) - 1) * (spinEndSpeed - spinStartSpeed));

				if(spinCurrentSpeed < spinEndSpeed) {

					//Update view every 10 milliseconds
					if(currentTime + 5L < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();

						if(precisionRotation > 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

						}
						else if(precisionRotation < 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

						}//End if(precisionRotation > 0)

						hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

						if(hgTransformInfo != null) {

							hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());//Necessary for manual triggerfling
							if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

						}
						else if(hgTransformInfo == null) {

							post(new Runnable() {
								@Override
								public void run() {

									if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
									requestRender();

								}
							});

						}//End if(hgTransformInfo != null)

					}//End if(currentTime + 5L < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed < spinEndSpeed)) */ {

					spinTriggered = false;
					hgResult.setSpinTriggered(false);
					hgRotateHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed < spinEndSpeed)

			}//End while(flingTriggered == true)

			calculateObjectAngleBaseOne();
			hgRotateHandler.sendEmptyMessage(0);

		}//End if(spinStartSpeed > spinEndSpeed)

	}//End private void doSpinAnimationFixedPrecision()


	private void doDefaultSpinAnimationFixedPrecision() {

		imageRadius = 0;
		long currentTime = System.currentTimeMillis();
		final float spinStartSpeed = (float) ((1000f / (currentTime - gestureDownTime)) * Math.abs(spinStartAngle - fullObjectAngleBaseOne));

		while(spinTriggered == true) {

			final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

			if(spinCurrentSpeed - spinEndSpeed > 0) {

				if(currentTime + 5L < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();

					if(precisionRotation > 0) {

						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

					}
					else if(precisionRotation < 0) {

						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

					}//End if(precisionRotation > 0)

					hgResult.setSpinCurrentSpeed(spinCurrentSpeed);

					if(hgTransformInfo != null) {

						hgTransformInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());//Necessary for manual trigger fling
						if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}

					}
					if(hgTransformInfo == null) {

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveRotate());}
								requestRender();

							}
						});

					}//End if(hgTransformInfo != null)

				}//End if(currentTime + 5L < System.currentTimeMillis())

			}
			else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

				spinTriggered = false;
				hgResult.setSpinTriggered(false);
				hgRotateHandler.sendEmptyMessage(0);

			}//End if(spinCurrentSpeed - spinEndSpeed > 0)

		}//End while(flingTriggered == true)

		hgRotateHandler.sendEmptyMessage(0);

	}//End private void doDefaultSpinAnimationFixedPrecision()
	/* Bottom of block Fling Functions called on thread */


	private static class HGRotateHandler extends Handler {

		private final WeakReference<HGRotate> hgglDialWeakReference;

		public HGRotateHandler(HGRotate hgRotate) {

			hgglDialWeakReference = new WeakReference<>(hgRotate);

		}

		@Override
		public void handleMessage(Message msg) {

			if(hgglDialWeakReference != null) {

				final HGRotate hgRotate = hgglDialWeakReference.get();
				hgRotate.spinAnimationThread = null;
				hgRotate.hgResult.setSpinTriggered(false);
				hgRotate.hgResult.setGestureRotationDirection(0);
				hgRotate.hgResult.setObjectRotationDirection(0);
				hgRotate.hgResult.setSpinCurrentSpeed(0f);
				hgRotate.onUpAngleGestureBaseOne = (1 - (hgRotate.onDownAngleObjectCumulativeBaseOne - hgRotate.getAngleWrapper.getGestureTouchAngle())) % 1;

				hgRotate.post(new Runnable() {
					@Override
					public void run() {
						hgRotate.invalidate();
					}
				});

			}//End if(hgDialWeakReference != null)

		}

	}//End private class HGRotateHandler extends Handler

}