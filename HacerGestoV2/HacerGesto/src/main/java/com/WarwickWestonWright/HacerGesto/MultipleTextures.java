/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Point;

public class MultipleTextures {

	private Bitmap bitmap;
	private Point offset;
	private double precision;
	private double rapidDialAngle;

	public MultipleTextures() {

		this.bitmap = null;
		this.offset = new Point();
		this.precision = 1f;
		this.rapidDialAngle = 0f;

	}

	public MultipleTextures(Bitmap bitmap, Point offset, float precision, float rapidDialAngle) {

		this.bitmap = bitmap;
		this.offset = offset;
		this.precision = precision;
		this.rapidDialAngle = rapidDialAngle;

	}

	//Accessors
	public Bitmap getBitmap() {return this.bitmap;}
	public Point getOffset() {return this.offset;}
	public double getPrecision() {return this.precision;}
	public double getRapidDialAngle() {return this.rapidDialAngle;}

	//Mutators
	public void setBitmap(Bitmap bitmap) {this.bitmap = bitmap;}
	public void setOffset(Point offset) {this.offset = offset;}
	public void setPrecision(double precision) {this.precision = precision;}
	public void setRapidDialAngle(double rapidDialAngle) {this.rapidDialAngle = rapidDialAngle;}

}