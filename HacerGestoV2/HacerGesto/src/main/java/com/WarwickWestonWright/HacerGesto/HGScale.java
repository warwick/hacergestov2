/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.opengles.GL10;

public class HGScale extends GLSurfaceView implements HGRender.IHGRender {

	private HGRender hgRender;
	private HGRender.IHGRender ihgRender;
	private int imageResourceId;
	private OnTouchListener onTouchListener;
	private final Point imageCenterPoint = new Point();
	private IHGScale ihgScale;
	private final HGResult hgResult = new HGResult();
	private static HGTransformInfo hgTransformInfo;
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private int touchPointerCount;
	private boolean cumulativeScale;
	private boolean disableGestureInternally;
	private float minimumScale;
	private float maximumScale;
	private boolean outsideMinMaxBounds;
	private float touchOffsetX;
	private float touchOffsetY;
	private int[][] renderOffset = null;
	private long quickTapTime;
	private long gestureDownTime;
	private double currentScaleX;
	private double currentScaleY;
	private boolean suppressRender;
	private float imageWidth;
	private float imageHeight;
	private float viewPortWidth;
	private float viewPortHeight;
	private final Point centreOffset = new Point();
	private double pinchStartDistance;
	private double startScaleRatio;
	private double pinchCurrentDistance;
	private double pinchPreviousDistance;
	private boolean useMinMaxScale;
	private float snapTolerance;
	private boolean disableGestureDynamically;
	private int scaleDirection;//Open End for CallBack not yet implemented
	private boolean scaleRelativeToWidth;
	private double lastScale;
	private MultipleTextures[] multipleTextures;

	/* Top of block field declarations */
	public interface IHGScale {

		void onDown(HGResult hgResult);
		void onPointerDown(HGResult hgResult);
		void onMove(HGResult hgResult);
		void onPointerUp(HGResult hgResult);
		void onUp(HGResult hgResult);

	}//End public interface IHGScale

	public HGScale(Context context, AttributeSet attrs) {
		super(context, attrs);

		setFields();

		if(hgTransformInfo == null) {

			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

			if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

				imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
				a.recycle();
				hgRender = new HGRender(getResources(), imageResourceId);
				setRenderer(hgRender);
				setDimensionsAndMultipleTextures();
				setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

			}
			else {

				setDimensionsAndMultipleTextures();

			}//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

		}
		else if(hgTransformInfo != null) {

			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

		}//End if(hgTransformInfo == null)

	}//End public HGScale(Context context, AttributeSet attrs)


	private void setDimensionsAndMultipleTextures() {

		post(new Runnable() {
			@Override
			public void run() {

				setupMetrics();
				requestRender();

			}
		});

	}//End private void setDimensionsAndMultipleTextures()


	@Override
	public void onResume() {
		super.onResume();

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	}


	void setupMetrics() {

		this.imageWidth = HGRender.getImageHeightAndWidth()[0];
		this.imageHeight = HGRender.getImageHeightAndWidth()[1];
		imageCenterPoint.x = HGRender.getImageCenterPoint().x;
		imageCenterPoint.y = HGRender.getImageCenterPoint().y;
		this.multipleTextures = HGRender.getMultipleTextures();
		renderOffset = new int[multipleTextures.length][];

		if(multipleTextures[0] != null) {

			for(int i = 0; i < multipleTextures.length; i++) {

				renderOffset[i] = new int[2];
				renderOffset[i][0] = multipleTextures[i].getOffset().x;
				renderOffset[i][1] = -multipleTextures[i].getOffset().y;

			}

		}

		viewPortWidth = HGRender.getViewPortHeightAndWidth()[0];
		viewPortHeight = HGRender.getViewPortHeightAndWidth()[1];
		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		centreOffset.x = (int) ((viewPortWidth - imageWidth) / 2f);
		centreOffset.y = (int) ((viewPortHeight - imageHeight) / 2f);

	}//End void setupMetrics()


	private void setFields() {

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgScale = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.cumulativeScale = true;
		this.disableGestureInternally = false;
		this.minimumScale = 0f;
		this.maximumScale = 0f;
		this.outsideMinMaxBounds = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentScaleX = 1f;
		this.currentScaleY = 1f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.pinchStartDistance = 0f;
		this.startScaleRatio = 1f;
		this.pinchCurrentDistance = 0f;
		this.pinchPreviousDistance = pinchCurrentDistance;
		this.useMinMaxScale = false;
		this.snapTolerance = 0f;
		this.disableGestureDynamically = false;
		this.scaleDirection = 0;
		this.scaleRelativeToWidth = true;
		this.lastScale = 0f;
		this.multipleTextures = null;

	}//End private void setFields()


	public void registerCallback(IHGScale ihgScale) {this.ihgScale = ihgScale;}


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		hgResult.setQuickTap(false);
		gestureDownTime = System.currentTimeMillis();

		try {

			try {

				touchPointerCount = event.getPointerCount();
				disableGestureInternally = false;

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else if(touchPointerCount == 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount < 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(event.getPointerCount() < 2)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setMoveTouch(MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetX;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					disableGestureInternally = true;

				}
				else if(touchPointerCount == 2) {

					disableGestureInternally = false;
					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block scale functions */
	private HGResult doDownScale() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(touchPointerCount == 1) {

			return hgResult;

		}
		else if(touchPointerCount == 2) {

			if(scaleRelativeToWidth == true) {

				lastScale = currentScaleX;

			}
			else if(scaleRelativeToWidth == false) {

				lastScale = currentScaleY;

			}//End if(scaleRelativeToWidth == true)

			pinchStartDistance = HGGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
			pinchCurrentDistance = pinchStartDistance;

			if(pinchCurrentDistance == 0) {

				startScaleRatio = (pinchCurrentDistance / pinchStartDistance);

			}

			currentScaleX = startScaleRatio;
			currentScaleY = startScaleRatio;
			hgResult.setScaleX(currentScaleX);
			hgResult.setScaleY(currentScaleY);
			hgResult.setTwoFingerDistance(pinchStartDistance);

		}//End if(touchPointerCount == 1 && isSingleFinger == true)

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentScaleX(currentScaleX);
			hgTransformInfo.setCurrentScaleY(currentScaleY);

		}//End if(hgTransformInfo != null)

		if(suppressRender == false) {

			requestRender();

		}

		return hgResult;

	}//End private HGResult doDownScale()


	private HGResult doMoveScale() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(disableGestureInternally == true) {

			return hgResult;

		}

		if(touchPointerCount > 1) {

			if(useMinMaxScale == true || snapTolerance != 0) {

				scaleMinMaxWithSnapping();

				if(hgTransformInfo != null) {

					hgTransformInfo.setCurrentScaleX(currentScaleX);
					hgTransformInfo.setCurrentScaleY(currentScaleY);

				}//End if(hgTransformInfo != null)

				if(suppressRender == false) {

					requestRender();

				}

				return hgResult;

			}//End if(useMinMaxScale == true || snapTolerance != 0)

			if(outsideMinMaxBounds == false) {

				pinchCurrentDistance = HGGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
				hgResult.setTwoFingerDistance(pinchCurrentDistance);
				currentScaleX = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;
				currentScaleY = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;
				hgResult.setScaleX(currentScaleX);
				hgResult.setScaleY(currentScaleY);

				if(suppressRender == false) {

					requestRender();

				}

			}
			else if(outsideMinMaxBounds == true) {

				currentScaleX = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;

			}//End if(outsideMinMaxBounds == false)

		}//End if(touchPointerCount > 1)

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentScaleX(currentScaleX);
			hgTransformInfo.setCurrentScaleY(currentScaleY);

		}//End if(hgTransformInfo != null)

		return hgResult;

	}//End private HGResult doMoveScale()


	private HGResult doUpScale() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		if(disableGestureInternally == true) {

			return hgResult;

		}
		else {

			if(touchPointerCount > 1) {

				disableGestureInternally = true;

				if(cumulativeScale == true) {

					startScaleRatio = currentScaleX;

				}
				else if(cumulativeScale == false) {

					startScaleRatio = 1f;

				}//End if(cumulativeScale == true)

				if(outsideMinMaxBounds == false) {

					if(suppressRender == false) {

						requestRender();

					}

				}//End if(outsideMinMaxBounds == false)

				return hgResult;

			}//End if(touchPointerCount > 1)

		}//End if(disableGestureInternally == true)

		outsideMinMaxBounds = false;

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentScaleX(currentScaleX);
			hgTransformInfo.setCurrentScaleY(currentScaleY);

		}//End if(hgTransformInfo != null)

		return hgResult;

	}//End private HGResult doUpScale()
    /* Bottom of block scale functions */


	/* Top of block behavioural methods */
	private void scaleMinMaxWithSnapping() {

		if(pinchPreviousDistance > hgResult.getTwoFingerDistance()) {

			scaleDirection = 1;

		}
		else if(pinchPreviousDistance < hgResult.getTwoFingerDistance()) {

			scaleDirection = -1;

		}//End if(pinchPreviousDistance < hgResult.getTwoFingerDistance())

		pinchPreviousDistance = HGGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
		hgResult.setTwoFingerDistance(pinchCurrentDistance);
		currentScaleX = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;
		currentScaleY = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;

		if(useMinMaxScale == true) {

			if(currentScaleX > maximumScale || currentScaleY > maximumScale) {

				currentScaleX = maximumScale;
				currentScaleY = maximumScale;
				outsideMinMaxBounds = true;

			}
			else if(currentScaleX < minimumScale || currentScaleY < minimumScale) {

				currentScaleX = minimumScale;
				currentScaleY = minimumScale;
				outsideMinMaxBounds = true;

			}
			else {

				outsideMinMaxBounds = false;

			}


		}//End if(useMinMaxScale == true)

		if(outsideMinMaxBounds == false) {

			if(outsideMinMaxBounds == false) {

				if(snapTolerance != 0) {

					if(scaleRelativeToWidth == true) {

						currentScaleX = ((imageWidth + (((int) (((imageWidth * currentScaleX) - imageWidth) / snapTolerance)) * snapTolerance)) / imageWidth);
						currentScaleY = ((imageWidth + (((int) (((imageWidth * currentScaleX) - imageWidth) / snapTolerance)) * snapTolerance)) / imageWidth);

						if(lastScale != currentScaleX) {

							hgResult.setScaleSnapped(true);

						}
						else if(lastScale == currentScaleX) {

							hgResult.setScaleSnapped(false);

						}//End if(lastScale != currentScaleX)

						lastScale = currentScaleX;

					}
					else if(scaleRelativeToWidth == false) {

						currentScaleX = ((imageHeight + (((int) (((imageHeight * currentScaleY) - imageHeight) / snapTolerance)) * snapTolerance)) / imageHeight);
						currentScaleY = ((imageHeight + (((int) (((imageHeight * currentScaleY) - imageHeight) / snapTolerance)) * snapTolerance)) / imageHeight);

						if(lastScale != currentScaleY) {

							hgResult.setScaleSnapped(true);

						}
						else if(lastScale == currentScaleY) {

							hgResult.setScaleSnapped(false);

						}//End if(lastScale != currentScaleY)

						lastScale = currentScaleY;//lastScale is an open end to dynamically return scale direction

					}//End if(scaleRelativeToWidth == true)

				}
				else if(snapTolerance == 0) {

					currentScaleX = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;
					currentScaleY = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;

				}//End if(snapTolerance != 0)

			}
			else if(outsideMinMaxBounds == true) {

				currentScaleX = (pinchCurrentDistance / pinchStartDistance) * startScaleRatio;

			}//End if(outsideMinMaxBounds == false)

		}//End if(outsideMinMaxBounds == false)

		hgResult.setScaleX(currentScaleX);
		hgResult.setScaleY(currentScaleY);
		pinchCurrentDistance = HGGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
		hgResult.setTwoFingerDistance(pinchCurrentDistance);

	}//End private void scaleMinMaxWithSnapping()
    /* Bottom of block behavioural methods */


	/* Top of block Accessors */
	public long getQuickTapTime() {return this.quickTapTime;}
	public boolean getDisableGesture() {return this.disableGestureDynamically;}
	public boolean getScaleRelativeToWidth() {return this.scaleRelativeToWidth;}
	public float getMinimumScale() {return this.minimumScale;}
	public float getMaximumScale() {return this.maximumScale;}
	public boolean getUseMinMaxScale() {return this.useMinMaxScale;}
	public float getSnapTolerance() {return this.snapTolerance;}
	public Point getCenterPoint() {return this.imageCenterPoint;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public boolean getSuppressRender() {return this.suppressRender;}
	double getCurrentScaleX() {return this.currentScaleX;}
	double getCurrentScaleY() {return this.currentScaleY;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setDisableGesture(final boolean disableGestureDynamically) {this.disableGestureDynamically = disableGestureDynamically;}
	public void setScaleRelativeToWidth(final boolean scaleRelativeToWidth) {this.scaleRelativeToWidth = scaleRelativeToWidth;}
	public void setSnapTolerance(final float snapTolerance) {this.snapTolerance = snapTolerance;}
	public void setMinimumScale(final float minimumScale) {this.minimumScale = minimumScale;}
	public void setMaximumScale(final float maximumScale) {this.maximumScale = maximumScale;}
	public void setUseMinMaxScale(final boolean useMinMaxScale) {this. useMinMaxScale = useMinMaxScale;}
	void setMultipleTextures(final MultipleTextures[] multipleTextures) {this.multipleTextures = multipleTextures;}
	public void setSuppressRender(final boolean suppressRender) {this.suppressRender = suppressRender;}


	public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {this.touchOffsetX = touchOffsetX;this.touchOffsetY = touchOffsetY;}
    /* Bottom of block Mutators */


	/* Top of block convenience methods */
	public void manualScale(final float currentScaleX, final float currentScaleY) {

		this.currentScaleX = currentScaleX;
		this.currentScaleY = currentScaleY;

		if(scaleRelativeToWidth == true) {

			startScaleRatio = currentScaleX;

		}
		else if(scaleRelativeToWidth == false) {

			startScaleRatio = currentScaleY;

		}//End if(scaleRelativeToWidth == true)

		if(hgTransformInfo != null) {

			this.post(new Runnable() {
				@Override
				public void run() {

					hgResult.setScaleX(currentScaleX);
					hgResult.setScaleY(currentScaleY);
					hgTransformInfo.setCurrentScaleX(currentScaleX);
					hgTransformInfo.setCurrentScaleY(currentScaleY);
					hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
					ihgScale.onDown(hgResult);

					if(suppressRender == false) {

						requestRender();

					}

				}
			});

		}
		else if(hgTransformInfo == null) {

			this.post(new Runnable() {
				@Override
				public void run() {

					hgResult.setScaleX(currentScaleX);
					hgResult.setScaleY(currentScaleY);

					if(suppressRender == false) {

						requestRender();

					}

				}
			});

		}//End if(hgTransformInfo != null)

	}//End public void manualScale(final float currentScaleX, final float currentScaleY)


	public void performQuickTap() {

		hgResult.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgScale.onUp(doUpScale());
				hgResult.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()
    /* Bottom of block convenience methods */


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgScale = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.cumulativeScale = true;
		this.disableGestureInternally = false;
		this.minimumScale = 0f;
		this.maximumScale = 0f;
		this.outsideMinMaxBounds = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentScaleX = 1f;
		this.currentScaleY = 1f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.pinchStartDistance = 0f;
		this.startScaleRatio = 1f;
		this.pinchCurrentDistance = 0f;
		this.pinchPreviousDistance = pinchCurrentDistance;
		this.useMinMaxScale = false;
		this.snapTolerance = 0f;
		this.disableGestureDynamically = false;
		this.scaleDirection = 0;
		this.scaleRelativeToWidth = true;
		this.lastScale = 0f;
		this.multipleTextures = null;

	}//End protected void onDetachedFromWindow()


	@Override
	public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] scaleMatrix, float[] matrixProjectionAndView) {

		final float[] scratch = new float[16];
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.translateM(scaleMatrix, 0, centerPoint.x + centreOffset.x, centerPoint.y + centreOffset.y, 0f);
		Matrix.scaleM(scaleMatrix, 0, (float) currentScaleX, (float) currentScaleY, 1.0f);
		Matrix.translateM(scaleMatrix, 0, -centerPoint.x, -centerPoint.y, 0f);
		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, scaleMatrix, 0);
		square.draw(scratch, activeTexture);

	}//End public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] scaleMatrix, float[] matrixProjectionAndView)


	static HGTransformInfo getHgTransformInfo() {return HGScale.hgTransformInfo;}
	static void setHgTransformInfo(final HGTransformInfo hgTransformInfo) {HGScale.hgTransformInfo = hgTransformInfo;}


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	private void hgTouchEvent(final View v, final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
			hgResult.setQuickTap(false);//Used for quick tap
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgResult.setQuickTap(true);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);

			}
			else {

				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				hgResult.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		switch(action) {

			case MotionEvent.ACTION_MOVE: {

				setMoveTouch(event);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				ihgScale.onMove(doMoveScale());

				break;

			}
			case MotionEvent.ACTION_DOWN: {

				hgResult.setQuickTap(false);//Used for quick tap
				gestureDownTime = System.currentTimeMillis();
				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				ihgScale.onDown(doDownScale());

				break;

			}
			case MotionEvent.ACTION_POINTER_DOWN: {

				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				ihgScale.onDown(doDownScale());

				break;

			}
			case MotionEvent.ACTION_POINTER_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				ihgScale.onUp(doUpScale());

				break;

			}
			case MotionEvent.ACTION_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.SCALE_GESTURE);
				ihgScale.onUp(doUpScale());

				break;

			}
			default:

				break;

		}//End switch(action)

	}//End private void hgTouchEvent(final View v, final MotionEvent event)

}