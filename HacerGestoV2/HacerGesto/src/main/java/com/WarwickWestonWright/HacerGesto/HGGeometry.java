/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.graphics.Point;

public class HGGeometry {

	public static float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

		float returnVal = 0;

		//+0 - 0.5
		if(touchPoint.x > centerPoint.x) {

			returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

		}
		//+0.5
		else if(touchPoint.x < centerPoint.x) {

			returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

		}//End if(touchPoint.x > centerPoint.x)

		return returnVal;

	}//End public static float getAngleFromPoint(final Point centerPoint, final Point touchPoint)


	public static Point getPointFromAngle(final double angle, final double radius) {

		final Point coords = new Point();
		coords.x = (int) (radius * Math.sin((angle) * 2 * Math.PI));
		coords.y = (int) -(radius * Math.cos((angle) * 2 * Math.PI));

		return coords;

	}//End public static Point getPointFromAngle(final double angle, final double radius)


	public static double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY) {

		float pinchDistanceX = 0;
		float pinchDistanceY = 0;

		if(firstTouchX > secondTouchX) {

			pinchDistanceX = Math.abs(secondTouchX - firstTouchX);

		}
		else if(firstTouchX < secondTouchX) {

			pinchDistanceX = Math.abs(firstTouchX - secondTouchX);

		}//End if(firstTouchX > secondTouchX)

		if(firstTouchY > secondTouchY) {

			pinchDistanceY = Math.abs(secondTouchY - firstTouchY);

		}
		else if(firstTouchY < secondTouchY) {

			pinchDistanceY = Math.abs(firstTouchY - secondTouchY);

		}//End if(firstTouchY > secondTouchY)

		if(pinchDistanceX == 0 && pinchDistanceY == 0) {

			return  0;

		}
		else {

			pinchDistanceX = (pinchDistanceX * pinchDistanceX);
			pinchDistanceY = (pinchDistanceY * pinchDistanceY);
			return (float) Math.abs(Math.sqrt(pinchDistanceX + pinchDistanceY));

		}//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

	}//End public static double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)


	public static double[] pythagoreanTheorem(final double diagonalLength, final double width, double height) {

		double[] returnVal = {0, 0};

		if(width != 0 && height != 0) {

			if(height > 0) {

				returnVal[1] = Math.sqrt((diagonalLength * diagonalLength) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
				returnVal[0] = (returnVal[1] / (height / width));

			}
			else {

				returnVal[1] = -Math.sqrt((diagonalLength * diagonalLength) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
				returnVal[0] = (returnVal[1] / (height / width));

			}//End if(height > 0)

		}
		else if(width != 0 && height == 0) {

			returnVal[0] = width;
			returnVal[1] = 0;
			return returnVal;

		}
		else if(width == 0 && height != 0) {

			returnVal[0] = 0;
			returnVal[1] = height;
			return returnVal;

		}//End if(width != 0 && height != 0)

		return returnVal;

	}//End public static float[] pythagoreanTheorem(final float diagonalLength, final float width, float height)

}