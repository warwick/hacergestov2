/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.graphics.Point;

final public class HGTransformInfo {

	//Rotate Group
	private final Point centerPoint = new Point(0 , 0);
	private float rapidDial;
	private boolean releaseAfter;
	private double angleSnapBaseOne;
	private double objectAngleBaseOne;
	private double angleSnapNextBaseOne;
	private double currentScaleX;
	private double currentScaleY;
	private float currentTransX;
	private float currentTransY;
	private long flingAnimationRelativeTime;
	private final Point flingStartPoint = new Point(0, 0);
	private final Point flingEndPoint = new Point(0, 0);
	private boolean flingTriggered;
	private boolean bounceBack;

	public HGTransformInfo() {

		//Rotate Group
		rapidDial = 0f;
		releaseAfter = true;
		angleSnapBaseOne = 0f;
		objectAngleBaseOne = 0f;
		angleSnapNextBaseOne = 0f;

		//Scale Group
		currentScaleX = 1f;
		currentScaleY = 1f;

		//Move Group
		currentTransX = 0f;
		currentTransY = 0f;

		//Fling Group
		flingAnimationRelativeTime = 0;
		flingTriggered = false;
		bounceBack = false;

	}//End public HGTransformInfo()


	/* Top of block Accessors */
	public Point getCenterPoint() {return this.centerPoint;}

	public float getRapidDial() {return this.rapidDial;}
	public boolean getReleaseAfter() {return this.releaseAfter;}
	public double getAngleSnapBaseOne() {return this.angleSnapBaseOne;}
	public double getObjectAngleBaseOne() {return this.objectAngleBaseOne;}
	public double getAngleSnapNextBaseOne() {return this.angleSnapNextBaseOne;}
	public double getCurrentScaleX() {return this.currentScaleX;}
	public double getCurrentScaleY() {return this.currentScaleY;}
	public float getCurrentTransX() {return this.currentTransX;}
	public float getCurrentTransY() {return this.currentTransY;}
	public long getFlingAnimationRelativeTime() {return this.flingAnimationRelativeTime;}
	public Point getFlingStartPoint() {return this.flingStartPoint;}
	public Point getFlingEndPoint() {return this.flingEndPoint;}
	public boolean getFlingTriggered() {return this.flingTriggered;}
	public boolean getBounceBack() {return this.bounceBack;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	void setCenterPoint(final Point centerPoint) {this.centerPoint.x = centerPoint.x; this.centerPoint.y = centerPoint.y;}
	void setRapidDial(final float rapidDial) {this.rapidDial = rapidDial;}
	void setReleaseAfter(final boolean rapidDial) {this.releaseAfter = rapidDial;}
	void setAngleSnapBaseOne(final double angleSnapBaseOne) {this.angleSnapBaseOne = angleSnapBaseOne;}
	void setObjectAngleBaseOne(final double objectAngleBaseOne) {this.objectAngleBaseOne = objectAngleBaseOne;}
	void setAngleSnapNextBaseOne(final double angleSnapNextBaseOne) {this.angleSnapNextBaseOne = angleSnapNextBaseOne;}
	void setCurrentScaleX(final double currentScaleX) {this.currentScaleX = currentScaleX;}
	void setCurrentScaleY(final double currentScaleY) {this.currentScaleY = currentScaleY;}
	void setCurrentTransX(final float currentTransX) {this.currentTransX = currentTransX;}
	void setCurrentTransY(final float currentTransY) {this.currentTransY = currentTransY;}
	void setFlingAnimationRelativeTime(final long flingAnimationRelativeTime) {this.flingAnimationRelativeTime = flingAnimationRelativeTime;}
	void setFlingStartPoint(final Point flingStartPoint) {this.flingStartPoint.x = flingStartPoint.x; this.flingStartPoint.y = flingStartPoint.y;}
	void setFlingEndPoint(final Point flingEndPoint) {this.flingEndPoint.x = flingEndPoint.x; this.flingEndPoint.y = flingEndPoint.y;}
	void setFlingTriggered(final boolean flingTriggered) {this.flingTriggered = flingTriggered;}
	void setBounceBack(final boolean bounceBack) {this.bounceBack = bounceBack;}
    /* Bottom of block Mutators */

}