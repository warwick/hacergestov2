/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.opengles.GL10;

public class HacerGestos extends GLSurfaceView implements HGRender.IHGRender {

	/* Top of block field declarations */
	public interface IHacerGestos {

		void onDown(HGResult hgResult);
		void onPointerDown(HGResult hgResult);
		void onMove(HGResult hgResult);
		void onPointerUp(HGResult hgResult);
		void onUp(HGResult hgResult);

	}//End public interface IHacerGestos

	//Variable Common to Gesture Classes
	private int imageWidth;
	private int imageHeight;
	private MultipleTextures[] multipleTextures;
	private int[][] renderOffset;
	private int viewPortWidth;
	private int viewPortHeight;
	private long gestureDownTime;
	private final static Point centreOffset = new Point(0, 0);

	private HGRender hgRender;
	private HGRender.IHGRender ihgRender;
	private HGRotate.IHGRotate ihgRotate;
	private HGScale.IHGScale ihgScale;
	private HGMove.IHGMove ihgMove;
	private HGFling.IHGFling ihgFling;
	private IHacerGestos iHacerGestos;
	private HGTransformInfo hgTransformInfo;
	private final HGResult hgResult = new HGResult();
	private int imageResourceId;
	private OnTouchListener onTouchListener;
	private final HGRotate hgRotate;
	private final HGScale hgScale;
	private final HGMove hgMove;
	private HGFling hgFling;
	private long quickTapTime;
    /* Bottom of block field declarations */


	public HacerGestos(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		setFields();

		//Top of block HGRotate
		HGRotate.setHgTransformInfo(hgTransformInfo);
		hgRotate = new HGRotate(context, attrs);

		ihgRotate = new HGRotate.IHGRotate() {
			@Override
			public void onDown(final HGResult hgResult) {

				post(new Runnable() {
					@Override
					public void run() {

						iHacerGestos.onDown(hgResult);

					}
				});

			}

			@Override
			public void onPointerDown(HGResult hgResult) {/* Open end */}

			@Override
			public void onMove(final HGResult hgResult) {

				if(hgRotate.spinTriggered == true) {

					post(new Runnable() {
						@Override
						public void run() {

							iHacerGestos.onMove(hgResult);

						}
					});

					requestRender();

				}

			}

			@Override
			public void onPointerUp(HGResult hgResult) {/* Open end */}

			@Override
			public void onUp(final HGResult hgResult) {

				if(hgRotate.spinExecutesFling == true && hgRotate.spinTriggered == true && hgFling.flingExecutesSpin == false) {

					hgFling.triggerFlingFromSpin();

				}

				iHacerGestos.onUp(hgResult);

			}
		};

		hgRotate.registerCallback(ihgRotate);
		//Bottom of block HGRotate

		//Top of block HGScale
		HGScale.setHgTransformInfo(hgTransformInfo);
		hgScale = new HGScale(context, attrs);

		ihgScale = new HGScale.IHGScale() {
			@Override
			public void onDown(final HGResult hgResult) {

				post(new Runnable() {
					@Override
					public void run() {

						iHacerGestos.onDown(hgResult);

					}
				});

			}

			@Override
			public void onPointerDown(HGResult hgResult) {/* Open end */}

			@Override
			public void onMove(final HGResult hgResult) {

				if(hgRotate.synchWithScale == true) {

					hgRotate.setImageDiameter(hgRender.getImageHeightAndWidth()[0] * hgScale.getCurrentScaleX());

				}

				iHacerGestos.onMove(hgResult);

			}

			@Override
			public void onPointerUp(HGResult hgResult) {/* Open end */}

			@Override
			public void onUp(final HGResult hgResult) {

				iHacerGestos.onUp(hgResult);

			}
		};

		hgScale.registerCallback(ihgScale);
		//Bottom of block HGScale

		//Top of block HGMove
		HGMove.setHgTransformInfo(hgTransformInfo);
		hgMove = new HGMove(context, attrs);

		ihgMove = new HGMove.IHGMove() {
			@Override
			public void onDown(final HGResult hgResult) {

				post(new Runnable() {
					@Override
					public void run() {

						iHacerGestos.onDown(hgResult);

					}
				});

			}

			@Override
			public void onPointerDown(HGResult hgResult) {/* Open end */}

			@Override
			public void onMove(final HGResult hgResult) {

				iHacerGestos.onMove(hgResult);

			}

			@Override
			public void onPointerUp(HGResult hgResult) {/* Open end */}

			@Override
			public void onUp(final HGResult hgResult) {

				iHacerGestos.onUp(hgResult);

			}
		};

		hgMove.registerCallback(ihgMove);
		//Bottom of block HGMove


		//Top of block HGFling
		HGFling.setHgTransformInfo(hgTransformInfo);
		hgFling = new HGFling(context, attrs);

		ihgFling = new HGFling.IHGFling() {
			@Override
			public void onDown(final HGResult hgResult) {

				post(new Runnable() {
					@Override
					public void run() {

						iHacerGestos.onDown(hgResult);

					}
				});

			}

			@Override
			public void onPointerDown(HGResult hgResult) {/* Open end */}

			@Override
			public void onMove(final HGResult hgResult) {

				if(hgFling.flingTriggered == true) {

					post(new Runnable() {
						@Override
						public void run() {

							iHacerGestos.onMove(hgResult);

						}
					});

					requestRender();

				}

			}

			@Override
			public void onPointerUp(HGResult hgResult) {/* Open end */}

			@Override
			public void onUp(final HGResult hgResult) {

				if(hgFling.flingExecutesSpin == true && hgFling.flingTriggered == true && hgRotate.spinExecutesFling == false) {

					hgRotate.triggerSpinFromFling();

				}

				iHacerGestos.onUp(hgResult);

			}
		};

		hgFling.registerCallback(ihgFling);
		//Bottom of block HGFling

		if(hgTransformInfo != null) {

			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

			if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

				imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
				a.recycle();
				hgRender = new HGRender(getResources(), imageResourceId);
				setRenderer(hgRender);
				setDimensionsAndMultipleTextures();
				setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

			}
			else {

				setDimensionsAndMultipleTextures();

			}//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

		}//End if(hgTransformInfo != null)

	}//End public HacerGestos(Context context, AttributeSet attrs)


	private void setDimensionsAndMultipleTextures() {

		hgRotate.setRenderer(hgRender);
		hgScale.setRenderer(hgRender);
		hgMove.setRenderer(hgRender);
		hgFling.setRenderer(hgRender);

		post(new Runnable() {
			@Override
			public void run() {

				while(HGRender.getHasSurfaceChanged() == false);
				hgRotate.setupMetrics();
				hgScale.setupMetrics();
				hgMove.setupMetrics();
				hgFling.setupMetrics();
				setupMetrics();
				requestRender();

			}
		});

	}//End private void setDimensionsAndMultipleTextures()


	@Override
	public void onResume() {
		super.onResume();

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	}


	private void setupMetrics() {

		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		multipleTextures = HGRender.getMultipleTextures();
		renderOffset = new int[multipleTextures.length][];

		if(multipleTextures[0] != null) {

			for(int i = 0; i < multipleTextures.length; i++) {

				renderOffset[i] = new int[2];
				renderOffset[i][0] = multipleTextures[i].getOffset().x;
				renderOffset[i][1] = -multipleTextures[i].getOffset().y;

			}

		}

		viewPortWidth = HGRender.getViewPortHeightAndWidth()[0];
		viewPortHeight = HGRender.getViewPortHeightAndWidth()[1];
		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		centreOffset.x = (int) ((viewPortWidth - imageWidth) / 2f);
		centreOffset.y = (int) ((viewPortHeight - imageHeight) / 2f);

	}//End private void setupMetrics()


	public void registerCallback(IHacerGestos iHacerGestos) {this.iHacerGestos = iHacerGestos;}


	private void setFields() {

		this.imageWidth = 0;
		this.imageHeight = 0;
		this.multipleTextures = null;
		this.renderOffset = null;
		this.viewPortWidth = 0;
		this.viewPortHeight = 0;
		this.imageWidth = 0;
		this.imageHeight = 0;
		this.ihgRender = this;
		this.hgTransformInfo = null;
		this.hgTransformInfo = new HGTransformInfo();
		this.hgTransformInfo.setCurrentTransX(0f);
		this.hgTransformInfo.setCurrentTransY(0f);
		this.quickTapTime = 75L;

	}//End private void setFields()


    /*
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {super.surfaceDestroyed(surfaceHolder);}
    */


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.imageWidth = 0;
		this.imageHeight = 0;
		this.multipleTextures = null;
		this.renderOffset = null;
		this.viewPortWidth = 0;
		this.viewPortHeight = 0;
		this.imageWidth = 0;
		this.imageHeight = 0;
		this.ihgRender = this;
		this.hgTransformInfo = null;
		this.hgTransformInfo = null;
		this.quickTapTime = 75L;

	}


	@Override
	public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] morphMatrix, float[] matrixProjectionAndView) {

		final float[] scratch = new float[16];
		Matrix.setIdentityM(morphMatrix, 0);

		//Move Section
		Matrix.translateM(morphMatrix, 0, hgTransformInfo.getCurrentTransX() + centreOffset.x, hgTransformInfo.getCurrentTransY() + centreOffset.y, 0.0f);

		//Rotate Section
		Matrix.translateM(morphMatrix, 0, centerPoint.x, centerPoint.y, 0f);

		if(hgTransformInfo.getRapidDial() != 0) {

			//This Condition is probably not needed
			Matrix.rotateM(morphMatrix, 0, -(hgTransformInfo.getRapidDial() * 360f), 0.0f, 0.0f, 1.0f);

			if(hgTransformInfo.getReleaseAfter() == true) {hgTransformInfo.setRapidDial(0f);}

		}
		else if(hgTransformInfo.getAngleSnapBaseOne() == 0) {

			Matrix.rotateM(morphMatrix, 0, (float) -(hgTransformInfo.getObjectAngleBaseOne() * 360), 0.0f, 0.0f, 1.0f);

		}
		else if(hgTransformInfo.getAngleSnapBaseOne() != 0) {

			Matrix.rotateM(morphMatrix, 0, -((float) hgTransformInfo.getAngleSnapNextBaseOne() * 360), 0.0f, 0.0f, 1.0f);

		}//End if(hgTransformInfo.getRapidDial() != 0)

		Matrix.translateM(morphMatrix, 0, -centerPoint.x, -centerPoint.y, 0f);

		//Scale Section
		Matrix.translateM(morphMatrix, 0, centerPoint.x, centerPoint.y, 0f);
		Matrix.scaleM(morphMatrix, 0, (float) hgTransformInfo.getCurrentScaleX(), (float) hgTransformInfo.getCurrentScaleY(), 1.0f);
		Matrix.translateM(morphMatrix, 0, -centerPoint.x, -centerPoint.y, 0f);

		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, morphMatrix, 0);
		square.draw(scratch, activeTexture);

	}//End public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] morphMatrix, float[] matrixProjectionAndView)


	/* Accessors */
	public HGRotate getHgRotate() {return this.hgRotate;}
	public HGScale getHgScale() {return this.hgScale;}
	public HGMove getHgMove() {return this.hgMove;}
	public HGFling getHgFling() {return this.hgFling;}
	public long getQuickTapTime() {return this.quickTapTime;}

	/* Mutators */
	public void setQuickTapTime(long quickTapTime) {

		this.quickTapTime = quickTapTime;
		hgRotate.setQuickTapTime(0L);
		hgScale.setQuickTapTime(0L);
		hgMove.setQuickTapTime(0L);
		hgFling.setQuickTapTime(0L);

	}//End public void setQuickTapTime(long quickTapTime)


	public void setSpinTriggersFling(final boolean spinExecutesFling) {hgRotate.spinExecutesFling = spinExecutesFling;}
	public void setFlingTriggersSpin(final boolean flingExecutesSpin) {hgFling.flingExecutesSpin = flingExecutesSpin;}


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	private OnTouchListener getOnTouchListerField() {

		onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				hgTouchEvent(v, event);

				return true;

			}

		};

		return onTouchListener;

	}//private OnTouchListener getOnTouchListerField()


	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	private void hgTouchEvent(final View v, final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		hgRotate.sendTouchEvent(v, event);
		hgScale.sendTouchEvent(v, event);
		hgMove.sendTouchEvent(v, event);
		hgFling.sendTouchEvent(v, event);
		requestRender();

		//Top of block used for quick tap
		if(action == MotionEvent.ACTION_DOWN) {

			hgResult.setQuickTap(false);
			gestureDownTime = System.currentTimeMillis();

		}
		else if(action == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgResult.setQuickTap(true);
				hgResult.setWhatGesture(HGResult.HACER_GESTO_GESTURE);

				if(hgFling.flingTriggered == false && hgRotate.spinTriggered == false) {

					iHacerGestos.onUp(hgResult);

				}

			}
			else {

				hgResult.setWhatGesture(HGResult.HACER_GESTO_GESTURE);
				hgResult.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

	}//End private void hgTouchEvent(final View v, final MotionEvent event)


	//Accessors
	public boolean isFlingAnimationInProgress() {

		if(hgFling != null) {

			return hgFling.flingTriggered;

		}
		else {

			return false;

		}

	}//End public boolean isFlingAnimationInProgress()

}