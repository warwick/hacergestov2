/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.lang.ref.WeakReference;

import javax.microedition.khronos.opengles.GL10;

public class HGFling extends GLSurfaceView implements HGRender.IHGRender, Runnable {

	private final HGFlingHandler hgFlingHandler = new HGFlingHandler(this);
	private HGRender hgRender;
	private HGRender.IHGRender ihgRender;
	private int imageResourceId;
	private OnTouchListener onTouchListener;
	private final Point imageCenterPoint = new Point();
	private IHGFling ihgFling;
	private final HGResult hgResult = new HGResult();
	private static HGTransformInfo hgTransformInfo;
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private int touchPointerCount;
	private boolean disableGestureDynamically;
	private float touchOffsetX;
	private float touchOffsetY;
	private int[][] renderOffset = null;
	private long quickTapTime;
	private long gestureDownTime;
	private float currentTransX;
	private float currentTransY;
	private boolean suppressRender;
	private float imageWidth;
	private float imageHeight;
	private float viewPortWidth;
	private float viewPortHeight;
	private final Point centreOffset = new Point();
	private float downTouchX;
	private float downTouchY;
	private boolean bounceBack;
	private final Point imageCenterOffset = new Point(0, 0);
	private final Point flingStartPoint = new Point(0, 0);
	private final Point flingEndPoint = new Point(0, 0);
	private boolean isStartPointOffScreen;
	private boolean isEndPointOffScreen;
	private boolean flingOffOfEdge;
	private boolean flingNearestCorner;
	private boolean flingNearestEdge;
	private double upperLeftRatio;
	private double lowerLeftRatio;
	private double lowerRightRatio;
	private double upperRightRatio;
	private float flingRatio;
	private float flingDistanceX;
	private float flingDistanceY;
	private float dynamicFlingDistance;
	private float flingDistanceThreshold;
	private long flingTimeThreshold;
	private long flingAnimationTime;
	private long flingAnimationRelativeTime;
	private boolean absoluteFlingTime;
	volatile boolean flingTriggered;
	private long gestureUpTime;
	boolean flingExecutesSpin;
	boolean flingTriggeredFromSpin;
	private MultipleTextures[] multipleTextures;
	private Thread animationThread;

	public interface IHGFling {

		void onDown(HGResult hgResult);
		void onPointerDown(HGResult hgResult);
		void onMove(HGResult hgResult);
		void onPointerUp(HGResult hgResult);
		void onUp(HGResult hgResult);

	}//End public interface IHGFling

	public HGFling(Context context, AttributeSet attrs) {
		super(context, attrs);

		setFields();

		if(hgTransformInfo == null) {

			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

			if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

				imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
				a.recycle();
				hgRender = new HGRender(getResources(), imageResourceId);
				setRenderer(hgRender);
				setDimensionsAndMultipleTextures();
				setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

			}
			else {

				setDimensionsAndMultipleTextures();

			}//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

		}
		else if(hgTransformInfo != null) {

			setEGLContextClientVersion(2);
			setZOrderOnTop(true);
			setEGLConfigChooser(8, 8, 8, 8, 16, 0);
			getHolder().setFormat(PixelFormat.RGBA_8888);
			hgRender.registerRenderer(ihgRender);

		}//End if(hgTransformInfo == null)

	}//End public HGFling(Context context, AttributeSet attrs)


	private void setDimensionsAndMultipleTextures() {

		post(new Runnable() {
			@Override
			public void run() {

				setupMetrics();
				requestRender();

			}
		});

	}


	@Override
	public void onResume() {
		super.onResume();

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	}


	void setupMetrics() {

		this.imageWidth = HGRender.getImageHeightAndWidth()[0];
		this.imageHeight = HGRender.getImageHeightAndWidth()[1];
		imageCenterPoint.x = HGRender.getImageCenterPoint().x;
		imageCenterPoint.y = HGRender.getImageCenterPoint().y;
		this.multipleTextures = HGRender.getMultipleTextures();
		renderOffset = new int[multipleTextures.length][];

		if(multipleTextures[0] != null) {

			for(int i = 0; i < multipleTextures.length; i++) {

				renderOffset[i] = new int[2];
				renderOffset[i][0] = multipleTextures[i].getOffset().x;
				renderOffset[i][1] = -multipleTextures[i].getOffset().y;

			}

		}

		viewPortWidth = HGRender.getViewPortHeightAndWidth()[0];
		viewPortHeight = HGRender.getViewPortHeightAndWidth()[1];
		imageWidth = HGRender.getImageHeightAndWidth()[0];
		imageHeight = HGRender.getImageHeightAndWidth()[1];
		centreOffset.x = (int) ((viewPortWidth - imageWidth) / 2f);
		centreOffset.y = (int) ((viewPortHeight - imageHeight) / 2f);

	}//End void setupMetrics()


	private void setFields() {

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgFling = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.disableGestureDynamically = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentTransX = 0f;
		this.currentTransY = 0f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.downTouchX = 0f;
		this.downTouchY = 0f;
		this.bounceBack = false;
		this.isStartPointOffScreen = false;
		this.isEndPointOffScreen = false;
		this.flingOffOfEdge = true;
		this.flingNearestCorner = false;
		this.flingNearestEdge = false;
		this.upperLeftRatio = 0f;
		this.lowerLeftRatio = 0f;
		this.lowerRightRatio = 0f;
		this.upperRightRatio = 0f;
		this.flingRatio = 0f;
		this.flingDistanceX = 0f;
		this.flingDistanceY = 0f;
		this.dynamicFlingDistance = 0f;
		this.flingDistanceThreshold = 250f;
		this.flingTimeThreshold = 350;
		this.flingAnimationTime = 500;
		this.flingAnimationRelativeTime = 0;
		this.absoluteFlingTime = false;
		this.flingTriggered = false;
		this.gestureUpTime = 0;
		this.flingExecutesSpin = false;
		this.flingTriggeredFromSpin = false;
		this.multipleTextures = null;
		this.animationThread = null;

	}//End private void setFields()


	public void registerCallback(IHGFling ihgFling) {this.ihgFling = ihgFling;}


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					downTouchX = firstTouchX;
					downTouchY = firstTouchY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else if(touchPointerCount == 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount < 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;

				}
				else {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(event.getPointerCount() < 2)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetX;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);

				}
				else if(touchPointerCount == 2) {

					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgResult.setFirstTouchX(firstTouchX);
					hgResult.setFirstTouchY(firstTouchY);
					hgResult.setSecondTouchX(secondTouchX);
					hgResult.setSecondTouchY(secondTouchY);

				}//End if(touchPointerCount == 1)

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block fling functions */
	private HGResult doDownFling() {

		flingTriggered = false;
		if(disableGestureDynamically == true) {

			return hgResult;

		}

		hgResult.setFlingTriggered(false);

		if(hgTransformInfo != null) {

			hgTransformInfo.setFlingTriggered(false);

		}

		if(hgTransformInfo == null) {

			Point pointLocal = new Point((int) hgResult.getXPos(), (int) hgResult.getYPos());
			flingStartPoint.x = pointLocal.x;
			flingStartPoint.y = pointLocal.y;

		}
		else if(hgTransformInfo != null) {

			Point pointLocal = new Point((int) hgTransformInfo.getCurrentTransX(), (int) hgTransformInfo.getCurrentTransY());
			flingStartPoint.x = pointLocal.x;
			flingStartPoint.y = pointLocal.y;

		}//End if(hgTransformInfo == null)

		isStartPointOffScreen = isOffScreen(flingStartPoint);

		if(hgTransformInfo != null) {

			hgTransformInfo.setCurrentTransX(currentTransX);
			hgTransformInfo.setCurrentTransY(currentTransY);

		}

		hgResult.setXPos(currentTransX);
		hgResult.setYPos(currentTransY);
		dynamicFlingDistance = 0f;

		return hgResult;

	}//End private HGResult doDownFling()


	private void setRatios() {

		if(flingOffOfEdge == true) {

			final Point pointLocal = new Point((int) ((imageWidth * hgResult.getScaleX()) / 2), (int) ((imageHeight * hgResult.getScaleY()) / 2));
			imageCenterOffset.x = pointLocal.x;
			imageCenterOffset.y = pointLocal.y;
			imageCenterOffset.x += centreOffset.x;
			imageCenterOffset.y += centreOffset.y;
			upperLeftRatio = -((imageCenterPoint.x - currentTransX) + ((imageWidth * hgResult.getScaleX()) / 2)) / ((imageCenterPoint.y - currentTransY) + ((imageHeight * hgResult.getScaleY()) / 2));
			lowerLeftRatio = -((imageCenterPoint.x - currentTransX) + ((imageWidth * hgResult.getScaleX()) / 2)) / -((imageCenterPoint.y - currentTransY) + ((imageHeight * hgResult.getScaleY()) / 2));
			lowerRightRatio = ((imageCenterPoint.x - currentTransX) + ((imageWidth * hgResult.getScaleX()) / 2)) / -((imageCenterPoint.y - currentTransY) + ((imageHeight * hgResult.getScaleY()) / 2));
			upperRightRatio = ((imageCenterPoint.x - currentTransX) + ((imageWidth * hgResult.getScaleX()) / 2)) / ((imageCenterPoint.y - currentTransY) + ((imageHeight * hgResult.getScaleY()) / 2));

		}
		else if(flingOffOfEdge == false) {

			final Point pointLocal = new Point(-((int) ((imageWidth * hgResult.getScaleX()) / 2)), -((int) ((imageHeight * hgResult.getScaleY()) / 2)));
			imageCenterOffset.x = pointLocal.x;
			imageCenterOffset.y = pointLocal.y;
			imageCenterOffset.x += centreOffset.x;
			imageCenterOffset.y += centreOffset.y;
			upperLeftRatio = -(((imageCenterPoint.x + centreOffset.x) + currentTransX) - ((imageWidth * hgResult.getScaleX()) / 2)) / (((imageCenterPoint.y + centreOffset.y) - currentTransY) - ((imageHeight * hgResult.getScaleY()) / 2));
			lowerLeftRatio = -(((imageCenterPoint.x - centreOffset.x) + currentTransX) - ((imageWidth * hgResult.getScaleX()) / 2)) / -(((imageCenterPoint.y - centreOffset.y) - currentTransY) - ((imageHeight * hgResult.getScaleY()) / 2));
			lowerRightRatio = (((imageCenterPoint.x + centreOffset.x) + currentTransX) - ((imageWidth * hgResult.getScaleX()) / 2)) / -(((imageCenterPoint.y + centreOffset.y) - currentTransY) - ((imageHeight * hgResult.getScaleY()) / 2));
			upperRightRatio = (((imageCenterPoint.x - centreOffset.x) + currentTransX) - ((imageWidth * hgResult.getScaleX()) / 2)) / (((imageCenterPoint.y - centreOffset.y) - currentTransY) - ((imageHeight * hgResult.getScaleY()) / 2));

		}//End if(flingOffOfEdge == true)

		flingDistanceX = (firstTouchX - downTouchX);
		flingDistanceY = (downTouchY - firstTouchY);
		flingRatio = flingDistanceX / flingDistanceY;

	}//End private void setRatios()


	private void setFlingStatus() {

		hgResult.setWhatGesture(HGResult.FLING_GESTURE);

		if(hgTransformInfo == null) {

			final Point pointLocal = new Point((int) currentTransX, (int) currentTransY);
			flingStartPoint.x = pointLocal.x;
			flingStartPoint.y = pointLocal.y;

			if(isStartPointOffScreen == false && isEndPointOffScreen == false) {

				if(flingTriggeredFromSpin == false) {

					flingTriggered = true;

				}

			}
			else {

				flingTriggered = false;

			}//End if(isStartPointOffScreen == false && isEndPointOffScreen == false)

		}
		else if(hgTransformInfo != null) {

			if(isStartPointOffScreen == false && isEndPointOffScreen == false) {

				if(flingTriggeredFromSpin == false) {

					flingTriggered = true;

				}

			}
			else {

				flingTriggered = false;

			}//End if(isStartPointOffScreen == false && isEndPointOffScreen == false)

			hgTransformInfo.setFlingTriggered(flingTriggered);
			hgTransformInfo.setFlingStartPoint(new Point((int) hgTransformInfo.getCurrentTransX(), (int) hgTransformInfo.getCurrentTransY()));
			hgTransformInfo.setFlingEndPoint(flingEndPoint);
			hgTransformInfo.setBounceBack(bounceBack);

		}//End if(hgTransformInfo == null)

		calculateRelativeTimeAndDistanceOfAnimation();
		hgResult.setFlingTriggered(true);

	}//End private void setFlingStatus()


	private HGResult doMoveFling() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		return hgResult;

	}//End private HGResult doMoveFling()


	private HGResult doUpFling() {

		if(disableGestureDynamically == true) {

			return hgResult;

		}

		final Point currentTransPoint = new Point();

		if(hgTransformInfo == null) {

			currentTransPoint.x = (int) hgResult.getXPos();
			currentTransPoint.y = (int) hgResult.getYPos();

		}
		else if(hgTransformInfo != null) {

			currentTransPoint.x = (int) hgTransformInfo.getCurrentTransX();
			currentTransPoint.y = (int) hgTransformInfo.getCurrentTransY();

		}//End if(hgTransformInfo == null)

		currentTransX = hgResult.getXPos();
		currentTransY = hgResult.getYPos();
		isEndPointOffScreen = isOffScreen(currentTransPoint);

		if(isStartPointOffScreen == false && isEndPointOffScreen == false) {

			setRatios();

		}
		else if(isStartPointOffScreen == true) {

			flingRatio = 0;

		}//End if(isStartPointOffScreen == false && isEndPointOffScreen == false)

		if(System.currentTimeMillis() < (gestureDownTime + flingTimeThreshold)) {

			gestureDownTime = 0;
			double tmpTwoFingerDistance = HGGeometry.getTwoFingerDistance(downTouchX, downTouchY, firstTouchX, firstTouchY);
			hgResult.setTwoFingerDistance(tmpTwoFingerDistance);

			if(tmpTwoFingerDistance > flingDistanceThreshold) {

				if(dynamicFlingDistance == 0) {

					if(flingNearestCorner == false && flingNearestEdge == false) {

						flingEndPoint.x = getFlingEndPoint(flingRatio).x;
						flingEndPoint.y = getFlingEndPoint(flingRatio).y;

					}
					else if(flingNearestCorner == true) {

						flingEndPoint.x = calculateFlingNearestCorner().x;
						flingEndPoint.y = calculateFlingNearestCorner().y;

					}
					else if(flingNearestEdge == true) {

						flingEndPoint.x = calculateFlingNearestEdge(flingRatio).x;
						flingEndPoint.y = calculateFlingNearestEdge(flingRatio).y;

					}//End if(flingNearestCorner == false && flingNearestEdge == false)

					setFlingStatus();

					if(flingTriggered == true && disableGestureDynamically == false) {

						hgResult.setQuickTap(false);
						animationThread = new Thread(this);
						animationThread.start();

					}

				}
				else if(dynamicFlingDistance != 0) {

					flingEndPoint.x = triggerFling(dynamicFlingDistance).x;
					flingEndPoint.y = triggerFling(dynamicFlingDistance).y;
					setFlingStatus();

					if(flingTriggered == true && disableGestureDynamically == false) {

						hgResult.setQuickTap(false);
						animationThread = new Thread(this);
						animationThread.start();

					}

				}//End if(dynamicFlingDistance == 0)

				if(hgTransformInfo != null) {

					hgTransformInfo.setCurrentTransX(currentTransX);
					hgTransformInfo.setCurrentTransY(currentTransY);

				}

				hgResult.setXPos(currentTransX);
				hgResult.setYPos(currentTransY);

				if(suppressRender == false) {

					requestRender();

				}

			}
			else if(tmpTwoFingerDistance <= flingDistanceThreshold) {

				flingEndPoint.x = flingStartPoint.x;
				flingEndPoint.y = flingStartPoint.y;

				if(hgTransformInfo != null) {

					hgTransformInfo.setFlingEndPoint(flingEndPoint);

				}//End if(hgTransformInfo != null)

			}//End if(tmpTwoFingerDistance > flingDistanceThreshold)

		}//End if(System.currentTimeMillis() < (gestureDownTime + flingTimeThreshold))

		return hgResult;

	}//End private HGResult doUpFling()
    /* Bottom of block fling functions */


	private boolean isOffScreen(final Point centerLocation) {

		double widthFromCenter = 0;
		double heightFromCenter = 0;

		if(hgTransformInfo != null) {

			widthFromCenter = (imageWidth * hgTransformInfo.getCurrentScaleX() / 2);
			heightFromCenter = (imageHeight * hgTransformInfo.getCurrentScaleY() / 2);

		}
		else if(hgTransformInfo == null) {

			widthFromCenter = (imageWidth / 2);
			heightFromCenter = (imageHeight / 2);

		}//End if(hgTransformInfo != null)

		if(flingOffOfEdge == false) {

			if(centerLocation.x - 1 < (-imageCenterPoint.x + widthFromCenter) - centreOffset.x || centerLocation.x + 1 > (imageCenterPoint.x - widthFromCenter) + centreOffset.x ||
				centerLocation.y - 1 < (-imageCenterPoint.y + heightFromCenter) - centreOffset.y || centerLocation.y + 1 > (imageCenterPoint.y - heightFromCenter) + centreOffset.y) {

				return true;

			}

		}
		else if(flingOffOfEdge == true) {

			if(centerLocation.x - 1 < (-imageCenterPoint.x - widthFromCenter) || centerLocation.x + 1 > (imageCenterPoint.x + widthFromCenter) ||
				centerLocation.y - 1 < (-imageCenterPoint.y - heightFromCenter) || centerLocation.y + 1 > (imageCenterPoint.y + heightFromCenter)) {

				return true;

			}

		}//End if(flingOffOfEdge == false)

		return false;

	}//End private boolean isOffScreen(final Point centerLocation)


	private Point getFlingEndPoint(final double flingRatio) {

		flingEndPoint.x = 0;
		flingEndPoint.y = 0;

		if(flingRatio != 0) {

			//Fling towards upper left
			if(flingDistanceX < 0 && flingDistanceY >= 0) {

				//Closer to left
				if(flingRatio <= upperLeftRatio) {

					flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
					flingEndPoint.y = (int) (((float) flingEndPoint.x) / (flingRatio));

				}
				//Closer to top
				else if(flingRatio > upperLeftRatio) {

					flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;
					flingEndPoint.x = (int) (((float) flingEndPoint.y) * (flingRatio));

				}//End if(flingRatio <= upperLeftRatio)

			}
			//Fling towards lower left
			else if(flingDistanceX <= 0 && flingDistanceY < 0) {

				//Closer to bottom
				if(flingRatio <= lowerLeftRatio) {

					flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);
					flingEndPoint.x = (int) (((float) flingEndPoint.y) * (flingRatio));

				}
				//Closer to left
				else if(flingRatio > lowerLeftRatio) {

					flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
					flingEndPoint.y = (int) (((float) (flingEndPoint.x)) / (flingRatio));

				}//End if(flingRatio <= lowerLeftRatio)

			}
			//Fling towards lower right
			else if(flingDistanceX >= 0 && flingDistanceY < 0) {

				//Closer to right
				if(flingRatio <= lowerRightRatio) {

					flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
					flingEndPoint.y = (int) (((float) (flingEndPoint.x)) / (flingRatio));

				}
				//Closer to bottom
				else if(flingRatio > lowerRightRatio) {

					flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);
					flingEndPoint.x = (int) (((float) flingEndPoint.y) * (flingRatio));

				}//End if(flingRatio <= lowerRightRatio)

			}
			//Fling towards upper right
			else if(flingDistanceX >= 0 && flingDistanceY > 0) {

				//Closer to top
				if(flingRatio <= upperRightRatio) {

					flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;
					flingEndPoint.x = (int) (((float) flingEndPoint.y) * (flingRatio));

				}
				//Closer to right
				else if(flingRatio > upperRightRatio) {

					flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
					flingEndPoint.y = (int) (((float) (flingEndPoint.x)) / (flingRatio));

				}//End if(flingRatio <= upperRightRatio)

			}//End if(flingDistanceX < 0 && flingDistanceY >= 0)

		}
		else if(flingRatio == 0) {

			if(hgTransformInfo == null) {

				final Point pointLocal = new Point((int) hgResult.getXPos(), (int) hgResult.getYPos());
				flingEndPoint.x = pointLocal.x;
				flingEndPoint.y = pointLocal.y;

			}
			else if(hgTransformInfo != null) {

				final Point pointLocal = new Point((int) hgTransformInfo.getCurrentTransX(), (int) hgTransformInfo.getCurrentTransY());
				flingEndPoint.x = pointLocal.x;
				flingEndPoint.y = pointLocal.y;

			}//End if(hgTransformInfo == null)

		}//End if(flingRatio != 0)

		return flingEndPoint;

	}//End private Point getFlingEndPoint(final double flingRatio)


	private Point calculateFlingNearestCorner() {

		flingEndPoint.x = 0;
		flingEndPoint.y = 0;

		//Fling towards upper left
		if(flingDistanceX < 0 && flingDistanceY >= 0) {

			flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
			flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;

		}
		//Fling towards lower left
		else if(flingDistanceX <= 0 && flingDistanceY < 0) {

			flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
			flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);

		}
		//Fling towards lower right
		else if(flingDistanceX >= 0 && flingDistanceY < 0) {

			flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
			flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);

		}
		//Fling towards upper right
		else if(flingDistanceX >= 0 && flingDistanceY > 0) {

			flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
			flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;

		}//End if(flingDistanceX < 0 && flingDistanceY >= 0)

		return flingEndPoint;

	}//End private Point calculateFlingNearestCorner()


	private Point calculateFlingNearestEdge(final double flingRatio) {

		flingEndPoint.x = 0;

		if(flingRatio != 0) {

			//Fling towards upper left
			if(flingDistanceX < 0 && flingDistanceY >= 0) {

				//Closer to left
				if(flingRatio <= upperLeftRatio) {

					flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
					flingEndPoint.y = 0;

				}
				//Closer to top
				else if(flingRatio > upperLeftRatio) {

					flingEndPoint.x = 0;
					flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;

				}//End if(flingRatio <= upperLeftRatio)

			}
			//Fling towards lower left
			else if(flingDistanceX <= 0 && flingDistanceY < 0) {

				//Closer to bottom
				if(flingRatio <= lowerLeftRatio) {

					flingEndPoint.x = 0;
					flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);

				}
				//Closer to left
				else if(flingRatio > lowerLeftRatio) {

					flingEndPoint.x = -(imageCenterPoint.x + imageCenterOffset.x);
					flingEndPoint.y = 0;

				}//End if(flingRatio <= lowerLeftRatio)

			}
			//Fling towards lower right
			else if(flingDistanceX >= 0 && flingDistanceY < 0) {

				//Closer to right
				if(flingRatio <= lowerRightRatio) {

					flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
					flingEndPoint.y = 0;

				}
				//Closer to bottom
				else if(flingRatio > lowerRightRatio) {

					flingEndPoint.x = 0;
					flingEndPoint.y = -(imageCenterPoint.y + imageCenterOffset.y);

				}//End if(flingRatio <= lowerRightRatio)

			}
			//Fling towards upper right
			else if(flingDistanceX >= 0 && flingDistanceY > 0) {

				//Closer to top
				if(flingRatio <= upperRightRatio) {

					flingEndPoint.x = 0;
					flingEndPoint.y = imageCenterPoint.y + imageCenterOffset.y;

				}
				//Closer to right
				else if(flingRatio > upperRightRatio) {

					flingEndPoint.x = imageCenterPoint.x + imageCenterOffset.x;
					flingEndPoint.y = 0;

				}//End if(flingRatio <= upperRightRatio)

			}//End if(flingDistanceX < 0 && flingDistanceY >= 0)

		}
		else if(flingRatio == 0) {

			if(hgTransformInfo == null) {

				Point pointLocal = new Point((int) hgResult.getXPos(), (int) hgResult.getYPos());
				flingEndPoint.x = pointLocal.x;
				flingEndPoint.y = pointLocal.y;

			}
			else if(hgTransformInfo != null) {

				Point pointLocal = new Point((int) hgTransformInfo.getCurrentTransX(), (int) hgTransformInfo.getCurrentTransY());
				flingEndPoint.x = pointLocal.x;
				flingEndPoint.y = pointLocal.y;

			}//End if(hgTransformInfo == null)

		}//End if(flingRatio != 0)

		return flingEndPoint;

	}//End private Point calculateFlingNearestEdge(final double flingRatio)


	private void calculateRelativeTimeAndDistanceOfAnimation() {

		//When absoluteFlingTime is false the fling time will be shorter when the up gesture is closer to the edge.
		if(absoluteFlingTime == false) {

			if(hgTransformInfo == null) {

				final double distanceFromCenterBasedOnRatio = HGGeometry.getTwoFingerDistance(currentTransX, currentTransY, flingEndPoint.x, flingEndPoint.y);
				final double xyDistanceFromEdge = HGGeometry.getTwoFingerDistance(hgResult.getXPos(), hgResult.getYPos(), flingEndPoint.x, flingEndPoint.y);
				flingAnimationRelativeTime = (long) ((distanceFromCenterBasedOnRatio - (distanceFromCenterBasedOnRatio - xyDistanceFromEdge)) / distanceFromCenterBasedOnRatio) * flingAnimationTime;

			}
			else if(hgTransformInfo != null) {

				final double centerToEndPointDistance =  HGGeometry.getTwoFingerDistance(hgTransformInfo.getFlingEndPoint().x, hgTransformInfo.getFlingEndPoint().y, hgTransformInfo.getCenterPoint().x, hgTransformInfo.getCenterPoint().y);
				final double distanceFromCenter = HGGeometry.getTwoFingerDistance(hgTransformInfo.getCurrentTransX(), hgTransformInfo.getCurrentTransY(), hgTransformInfo.getCenterPoint().x, hgTransformInfo.getCenterPoint().y);
				final double distanceFromEdge = HGGeometry.getTwoFingerDistance(hgTransformInfo.getCurrentTransX(), hgTransformInfo.getCurrentTransY(), hgTransformInfo.getFlingEndPoint().x, hgTransformInfo.getFlingEndPoint().y);
				flingAnimationRelativeTime = (long) (flingAnimationTime * (Math.abs(distanceFromEdge - distanceFromCenter) / centerToEndPointDistance));
				hgTransformInfo.setFlingAnimationRelativeTime(flingAnimationRelativeTime);

			}//End if(hgTransformInfo == null)

		}
		else if(absoluteFlingTime == true) {

			flingAnimationRelativeTime = flingAnimationTime;

			if(hgTransformInfo != null) {

				hgTransformInfo.setFlingAnimationRelativeTime(flingAnimationRelativeTime);

			}

		}//End if(absoluteFlingTime == false)

	}//End private void calculateRelativeTimeAndDistanceOfAnimation()


	private void animateFling(final Point startAnim, final Point finishAnim, final long flingAnimationRelativeTime) {

		if(startAnim != null && finishAnim.equals(startAnim) == false) {

			gestureUpTime = System.currentTimeMillis();
			hgResult.setXPos(startAnim.x);
			hgResult.setYPos(startAnim.y);
			final float distanceX = (float) (finishAnim.x - startAnim.x);
			final float distanceY = (float) (finishAnim.y - startAnim.y);
			long frameDelay = System.currentTimeMillis() + 5L;

			while(flingTriggered == true && System.currentTimeMillis() < gestureUpTime + flingAnimationRelativeTime) {

				if(System.currentTimeMillis() > frameDelay) {

					final long tempTime = System.currentTimeMillis();
					currentTransX = (float) startAnim.x + (((float) (tempTime - gestureUpTime)) / ((float) flingAnimationRelativeTime) * distanceX);
					currentTransY = (float) startAnim.y + (((float) (tempTime - gestureUpTime)) / ((float) flingAnimationRelativeTime) * distanceY);
					hgResult.setXPos(currentTransX);
					hgResult.setYPos(currentTransY);

					if(suppressRender == false) {

						if(hgTransformInfo != null) {

							hgTransformInfo.setCurrentTransX(currentTransX);
							hgTransformInfo.setCurrentTransY(currentTransY);

							if(ihgFling != null) {ihgFling.onMove(doMoveFling());}

						}
						else if(hgTransformInfo == null) {

							post(new Runnable() {
								@Override
								public void run() {
									requestRender();
								}
							});

						}//End if(hgTransformInfo != null)

					}//End if(suppressRender == false)

					frameDelay = System.currentTimeMillis() + 5L;

				}//End if(System.currentTimeMillis() > frameDelay)

			}//End while(flingTriggered == true && System.currentTimeMillis() < gestureUpTime + flingAnimationRelativeTime)

			if(flingTriggered == false) {

				finishAnim.x = (int) currentTransX;
				finishAnim.y = (int) currentTransY;

			}

			if(hgTransformInfo == null) {

				flingEndPoint.x = finishAnim.x;
				flingEndPoint.y = finishAnim.y;

			}
			else if(hgTransformInfo != null) {

				hgTransformInfo.setFlingEndPoint(finishAnim);
				hgTransformInfo.setCurrentTransX(finishAnim.x);
				hgTransformInfo.setCurrentTransY(finishAnim.y);

			}//End if(hgTransformInfo == null)

			hgResult.setXPos(finishAnim.x);
			hgResult.setYPos(finishAnim.y);
			hgFlingHandler.sendEmptyMessage(0);

		}//End private void animateFling(final Point startAnim, final Point finishAnim, final long flingAnimationRelativeTime)

	}//End private void animateFling(final Point startAnim, final Point finishAnim, final long flingAnimationRelativeTime)


	public Point triggerFling(final float dynamicFlingDistance, final float width, float height) {

		this.dynamicFlingDistance = dynamicFlingDistance;
		this.flingDistanceX = width;
		this.flingDistanceY = height;
		final double[] returnValue = HGGeometry.pythagoreanTheorem(dynamicFlingDistance, width, height);
		Point pointLocal = new Point((int) returnValue[0], (int) returnValue[1]);
		flingEndPoint.x = pointLocal.x;
		flingEndPoint.y = pointLocal.y;
		setFlingStatus();

		return flingEndPoint;

	}//End public Point triggerFling(final float dynamicFlingDistance, final float width, float height)


	void triggerFlingFromSpin() {

		flingTriggeredFromSpin = true;
		final Point flingEndPointInternal = triggerFling(dynamicFlingDistance);
		flingEndPoint.x = flingEndPointInternal.x;
		flingEndPoint.y = flingEndPointInternal.y;

	}//End void triggerFlingFromSpin()


	private Point triggerFling(final double dynamicFlingDistance) {

		final double[] returnValue = HGGeometry.pythagoreanTheorem(dynamicFlingDistance, flingDistanceX, flingDistanceY);
		final Point pointLocal = new Point((int) returnValue[0], (int) returnValue[1]);
		flingEndPoint.x = pointLocal.x;
		flingEndPoint.y = pointLocal.y;

		return flingEndPoint;

	}//End private Point triggerFling(final double dynamicFlingDistance)


	/* Top of block Accessors */
	public boolean getDisableGesture() {return this.disableGestureDynamically;}
	public float getFlingDistanceThreshold() {return this.dynamicFlingDistance;}
	public float getFlingTimeThreshold() {return this.flingTimeThreshold;}
	public float getFlingAnimationTime() {return this.flingAnimationTime;}
	public boolean getBounceBack() {return this.bounceBack;}
	public Point getFlingStartPoint() {return this.flingStartPoint;}
	public boolean getAbsoluteFlingTime() {return this.absoluteFlingTime;}
	public long getQuickTapTime() {return this.quickTapTime;}
	public Point getFlingEndPoint() {return this.flingEndPoint;}
	public boolean getFlingOffEdge() {return  this.flingOffOfEdge;}
	public boolean getFlingTriggered() {return this.flingTriggered;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public boolean getSuppressRender() {return this.suppressRender;}
	public boolean getFlingNearestCorner() {return this.flingNearestCorner;}
	public boolean getFlingNearestEdge() {return this.flingNearestEdge;}
	public boolean getFlingTriggersSpin() {return this.flingExecutesSpin;}
    /* Bottom of block Accessors */

	/* Top of block mutators */
	public void setDisableGesture(final boolean disableGestureDynamically) {this.disableGestureDynamically = disableGestureDynamically;}
	public void setFlingDistance(final int dynamicFlingDistance) {this.dynamicFlingDistance = dynamicFlingDistance;}
	public void setFlingBounceBack(final boolean bounceBack) {this.bounceBack = bounceBack;}
	public void setFlingStartPoint(final Point flingStartPoint) {this.flingStartPoint.x = flingStartPoint.x; this.flingStartPoint.y = flingStartPoint.y;}
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setAbsoluteFlingTime(final boolean absoluteFlingTime) {this.absoluteFlingTime = absoluteFlingTime;}
	public void setFlingEndPoint(final Point flingEndPoint) {this.flingEndPoint.x = flingEndPoint.x; this.flingEndPoint.x = flingEndPoint.x;}
	public void setFlingOffOfEdge(final boolean flingOffOfEdge) {this.flingOffOfEdge = flingOffOfEdge;}
	void setMultipleTextures(final MultipleTextures[] multipleTextures) {this.multipleTextures = multipleTextures;}
	public void setSuppressRender(final boolean suppressRender) {this.suppressRender = suppressRender;}
	void setFlingTriggersSpin(final boolean flingExecutesSpin) {this.flingExecutesSpin = flingExecutesSpin;}


	public void setupFlingValues(final float flingDistanceThreshold, final long flingTimeThreshold, final long flingAnimationTime) {

		this.flingDistanceThreshold = flingDistanceThreshold;
		this.flingTimeThreshold = flingTimeThreshold;
		this.flingAnimationTime = flingAnimationTime;

	}//End public void setupFlingValues(final float flingDistanceThreshold, final long flingTimeThreshold, final long flingAnimationTime)


	public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {

		this.touchOffsetX = touchOffsetX;
		this.touchOffsetY = touchOffsetY;

	}//End setTouchOffset(final float touchOffsetX, final float touchOffsetY)


	public void setFlingNearestCorner(final boolean flingNearestCorner) {

		this.flingNearestCorner = flingNearestCorner;

		if(flingNearestCorner == true) {

			this.flingNearestEdge = false;

		}

	}//End public void setFlingNearestCorner(boolean flingNearestCorner)


	public void setFlingNearestEdge(final boolean flingNearestEdge) {

		this.flingNearestEdge = flingNearestEdge;

		if(flingNearestEdge == true) {

			this.flingNearestCorner = false;

		}

	}//End public void setFlingNearestEdge(boolean flingNearestEdge)
    /* bottom of block mutators */


	@Override
	public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] flingMatrix, float[] matrixProjectionAndView) {

		final float[] scratch = new float[16];
		Matrix.setIdentityM(flingMatrix, 0);
		Matrix.translateM(flingMatrix, 0, currentTransX + centreOffset.x, currentTransY + centreOffset.y, 0f);
		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, flingMatrix, 0);
		square.draw(scratch, activeTexture);

	}//End public void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] flingMatrix, float[] matrixProjectionAndView)


	static HGTransformInfo getHgTransformInfo() {return HGFling.hgTransformInfo;}
	static void setHgTransformInfo(HGTransformInfo hgTransformInfo) {HGFling.hgTransformInfo = hgTransformInfo;}


	public void performQuickTap() {

		hgResult.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgFling.onUp(doUpFling());
				hgResult.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.ihgRender = this;
		this.imageResourceId = 0;
		this.onTouchListener = null;
		this.ihgFling = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchPointerCount = 0;
		this.disableGestureDynamically = false;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.quickTapTime = 75L;
		this.gestureDownTime = 0;
		this.currentTransX = 0f;
		this.currentTransY = 0f;
		this.suppressRender = false;
		this.imageWidth = 0f;
		this.imageHeight = 0f;
		this.viewPortWidth = 0f;
		this.viewPortHeight = 0f;
		this.downTouchX = 0f;
		this.downTouchY = 0f;
		this.bounceBack = false;
		this.isStartPointOffScreen = false;
		this.isEndPointOffScreen = false;
		this.flingOffOfEdge = true;
		this.flingNearestCorner = false;
		this.flingNearestEdge = false;
		this.upperLeftRatio = 0f;
		this.lowerLeftRatio = 0f;
		this.lowerRightRatio = 0f;
		this.upperRightRatio = 0f;
		this.flingRatio = 0f;
		this.flingDistanceX = 0f;
		this.flingDistanceY = 0f;
		this.dynamicFlingDistance = 0f;
		this.flingDistanceThreshold = 250f;
		this.flingTimeThreshold = 350;
		this.flingAnimationTime = 500;
		this.flingAnimationRelativeTime = 0;
		this.absoluteFlingTime = false;
		this.flingTriggered = false;
		this.gestureUpTime = 0;
		this.flingExecutesSpin = false;
		this.flingTriggeredFromSpin = false;
		this.multipleTextures = null;
		this.animationThread = null;

	}//End protected void onDetachedFromWindow()


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	private void hgTouchEvent(final View v, final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgResult.setWhatGesture(HGResult.FLING_GESTURE);
			hgResult.setQuickTap(false);//Used for quick tap
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgResult.setQuickTap(true);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onUp(doUpFling());
				return;

			}
			else {

				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				hgResult.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		switch(action) {

			case MotionEvent.ACTION_MOVE: {

				setMoveTouch(event);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onMove(doMoveFling());

				break;

			}
			case MotionEvent.ACTION_DOWN: {
				hgResult.setQuickTap(false);
				gestureDownTime = System.currentTimeMillis();
				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onDown(doDownFling());

				break;

			}
			case MotionEvent.ACTION_POINTER_DOWN: {

				setDownTouch(event);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onDown(doDownFling());

				break;

			}
			case MotionEvent.ACTION_POINTER_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onUp(doUpFling());

				break;

			}
			case MotionEvent.ACTION_UP: {

				setUpTouch(event);
				hgResult.setWhatGesture(HGResult.FLING_GESTURE);
				ihgFling.onUp(doUpFling());

				break;

			}
			default:

				break;

		}//End switch(action)

	}//End private void hgTouchEvent(final View v, final MotionEvent event)


	@Override
	public void run() {

		animateFling(new Point((int) hgResult.getXPos(), (int) hgResult.getYPos()), flingEndPoint, flingAnimationRelativeTime);
		hgResult.setWhatGesture(HGResult.FLING_GESTURE);

	}//End public void run()


	private static class HGFlingHandler extends Handler {

		private final WeakReference<HGFling> hgFlingWeakReference;

		public HGFlingHandler(HGFling hgFling) {
			hgFlingWeakReference = new WeakReference<>(hgFling);
		}

		@Override
		public void handleMessage(Message msg) {

			final HGFling hgFling = hgFlingWeakReference.get();
			hgFling.animationThread = null;

			if(hgFling != null) {

				if(hgFling.bounceBack == true) {

					hgFling.hgResult.setWhatGesture(HGResult.FLING_GESTURE);
					hgFling.currentTransX = 0f;
					hgFling.currentTransY = 0f;
					hgFling.hgResult.setXPos(hgFling.currentTransX);
					hgFling.hgResult.setYPos(hgFling.currentTransY);

					if(hgFling.hgTransformInfo != null) {

						hgFling.hgTransformInfo.setCurrentTransX(hgFling.currentTransX);
						hgFling.hgTransformInfo.setCurrentTransY(hgFling.currentTransY);

					}//End if(hgTransformInfo != null)

				}
				else if(hgFling.bounceBack == false) {

					hgFling.flingRatio = 0;

				}//End if(hgFling.bounceBack == true)

				this.post(new Runnable() {
					@Override
					public void run() {

						hgFling.flingTriggered = false;
						hgFling.hgResult.setFlingTriggered(false);
						hgFling.hgResult.setTwoFingerDistance(0);
						hgFling.hgResult.setFlingAnimationComplete(true);
						hgFling.ihgFling.onUp(hgFling.hgResult);
						hgFling.hgResult.setFlingAnimationComplete(false);

						hgFling.post(new Runnable() {
							@Override
							public void run() {
								hgFling.invalidate();
							}
						});

					}
				});

			}//End if(hgFling != null)

		}

	}//End private static class HGFlingHandler extends Handler

}