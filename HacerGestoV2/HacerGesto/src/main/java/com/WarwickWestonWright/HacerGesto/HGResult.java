/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGesto;

//ToDo: See if you can lose the statics (not priority).
final public class HGResult {

	public static final int FLING_GESTURE = 2;
	public static final int MOVE_GESTURE = 4;
	public static final int SCALE_GESTURE = 8;
	public static final int ROTATE_GESTURE = 16;
	public static final int HACER_GESTO_GESTURE = 32;
	private static int whatGesture;
	private static boolean quickTap;
	private static float xPos;
	private static float yPos;
	private static boolean moveSnapped;
	private static double scaleX;
	private static double scaleY;
	private static boolean scaleSnapped;
	private static int gestureRotationDirection;
	private static int objectRotationDirection;
	private static double gestureAngleBaseOne;
	private static double objectAngleBaseOne;
	private static float firstTouchX;
	private static float firstTouchY;
	private static float secondTouchX;
	private static float secondTouchY;
	private static int objectRotationCount;
	private static int gestureRotationCount;
	private static double gestureTouchAngle;
	private static double twoFingerDistance;
	private static boolean rotateSnapped;
	private static double variablePrecision;
	private static int minMaxRotationOutOfBounds;
	private static boolean flingTriggered;
	private static boolean flingAnimationComplete;
	private static boolean spinTriggered;
	private static float spinCurrentSpeed;

	public HGResult() {

		this.whatGesture = 0;
		this.quickTap = false;
		this.xPos = 0f;
		this.yPos = 0f;
		this.moveSnapped = false;
		this.scaleX = 1f;
		this.scaleY = 1f;
		this.scaleSnapped = false;
		this.gestureRotationDirection = 0;
		this.objectRotationDirection = 0;
		this.gestureAngleBaseOne = 0f;
		this.objectAngleBaseOne = 0f;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.objectRotationCount = 0;
		this.gestureRotationCount = 0;
		this.gestureTouchAngle = 0f;
		this.twoFingerDistance = 0f;
		this.rotateSnapped = false;
		this.variablePrecision = 0f;
		this.flingTriggered = false;
		this.flingAnimationComplete = false;
		this.spinTriggered = false;
		this.spinCurrentSpeed = 0;

	}//End public HGResult()

	/* Accessors */
	public int getWhatGesture() {return this.whatGesture;}
	public boolean getQuickTap() {return this.quickTap;}
	public float getXPos() {return this.xPos;}
	public float getYPos() {return this.yPos;}
	public boolean getMoveSnapped() {return this.moveSnapped;}
	public double getScaleX() {return this.scaleX;}
	public double getScaleY() {return this.scaleY;}
	public boolean getScaleSnapped() {return scaleSnapped;}
	public int getGestureRotationDirection() {return this.gestureRotationDirection;}
	public int getObjectRotationDirection() {return this.objectRotationDirection;}
	public double getGestureAngleBaseOne() {return this.gestureAngleBaseOne;}
	public double getObjectAngleBaseOne() {return this.objectAngleBaseOne;}
	public float getFirstTouchX() {return this.firstTouchX;}
	public float getFirstTouchY() {return this.firstTouchY;}
	public float getSecondTouchX() {return this.secondTouchX;}
	public float getSecondTouchY() {return this.secondTouchY;}
	public int getObjectRotationCount() {return this.objectRotationCount;}
	public int getGestureRotationCount() {return this.gestureRotationCount;}
	public double getGestureTouchAngle() {return this.gestureTouchAngle;}
	public double getTwoFingerDistance() {return this.twoFingerDistance;}
	public boolean getRotateSnapped() {return this.rotateSnapped;}
	public double getVariablePrecision() {return this.variablePrecision;}
	public int getMinMaxRotationOutOfBounds() {return this.minMaxRotationOutOfBounds;}
	public boolean getFlingTriggered() {return this.flingTriggered;}
	public boolean getFlingAnimationComplete() {return this.flingAnimationComplete;}
	public boolean getSpinTriggered() {return this.spinTriggered;}
	public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}

	/* Mutators */
	void setWhatGesture(final int whatGesture) {this.whatGesture = whatGesture;}
	void setQuickTap(final boolean quickTap) {this.quickTap = quickTap;}
	void setXPos(final float xPos) {this.xPos = xPos;}
	void setYPos(final float yPos) {this.yPos = yPos;}
	void setMoveSnapped(final boolean moveSnapped) {this.moveSnapped = moveSnapped;}
	void setScaleX(final double scaleX) {this.scaleX = scaleX;}
	void setScaleY(final double scaleY) {this.scaleY = scaleY;}
	void setScaleSnapped(final boolean scaleSnapped) {this.scaleSnapped = scaleSnapped;}
	void setGestureRotationDirection(final int gestureRotationDirection) {this.gestureRotationDirection = gestureRotationDirection;}
	void setObjectRotationDirection(final int objectRotationDirection) {this.objectRotationDirection = objectRotationDirection;}
	void setGestureAngleBaseOne(final double gestureAngleBaseOne) {this.gestureAngleBaseOne = gestureAngleBaseOne;}
	void setObjectAngleBaseOne(final double objectAngleBaseOne) {this.objectAngleBaseOne = objectAngleBaseOne;}
	void setFirstTouchX(final float firstTouchX) {this.firstTouchX = firstTouchX;}
	void setFirstTouchY(final float firstTouchY) {this.firstTouchY = firstTouchY;}
	void setSecondTouchX(final float secondTouchX) {this.secondTouchX = secondTouchX;}
	void setSecondTouchY(final float secondTouchY) {this.secondTouchY = secondTouchY;}
	void setObjectRotationCount(final int objectRotationCount) {this.objectRotationCount = objectRotationCount;}
	void setGestureRotationCount(final int gestureRotationCount) {this.gestureRotationCount = gestureRotationCount;}
	void setGestureTouchAngle(final double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}
	void setTwoFingerDistance(final double twoFingerDistance) {this.twoFingerDistance = twoFingerDistance;}
	void setRotateSnapped(final boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
	void setVariablePrecision(final double variablePrecision) {this.variablePrecision = variablePrecision;}
	void setMinMaxRotationOutOfBounds(final int minMaxRotationOutOfBounds) {this.minMaxRotationOutOfBounds = minMaxRotationOutOfBounds;}
	void setFlingTriggered(final boolean flingTriggered) {this.flingTriggered = flingTriggered;}
	void setFlingAnimationComplete(final boolean flingAnimationComplete) {this.flingAnimationComplete = flingAnimationComplete;}
	void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
	void setSpinCurrentSpeed(final float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}

}//End final public class HGResult