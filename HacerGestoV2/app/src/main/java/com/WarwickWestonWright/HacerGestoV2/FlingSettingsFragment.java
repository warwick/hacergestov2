/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class FlingSettingsFragment extends DialogFragment {

	public FlingSettingsFragment() {}
	private View rootView;
	private DialogInterface.OnClickListener onClickListener;
	private CheckBox chkDisableGesture;
	private CheckBox chkNearestEdge;
	private CheckBox chkNearestCorner;
	private CheckBox chkFlingOffEdge;
	private EditText txtDistanceThreshold;
	private EditText txtTimeThreshold;
	private EditText txtAnimationTimeThreshold;
	private Button btnFlingSettingClose;
	private static SharedPreferences sharedPreferences;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onClickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fling_settings_fragment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
		chkDisableGesture = rootView.findViewById(R.id.chkDisableGesture);
		chkNearestEdge = rootView.findViewById(R.id.chkNearestEdge);
		chkNearestCorner = rootView.findViewById(R.id.chkNearestCorner);
		chkFlingOffEdge = rootView.findViewById(R.id.chkFlingOffEdge);
		txtDistanceThreshold = rootView.findViewById(R.id.txtDistanceThreshold);
		txtTimeThreshold = rootView.findViewById(R.id.txtTimeThreshold);
		txtAnimationTimeThreshold = rootView.findViewById(R.id.txtAnimationTimeThreshold);
		btnFlingSettingClose = rootView.findViewById(R.id.btnFlingSettingClose);

		chkDisableGesture.setChecked(sharedPreferences.getBoolean("disableFling", false));
		chkNearestEdge.setChecked(sharedPreferences.getBoolean("nearestEdge", false));
		chkNearestCorner.setChecked(sharedPreferences.getBoolean("nearestCorner", false));
		chkFlingOffEdge.setChecked(sharedPreferences.getBoolean("flingOffEdge", true));
		txtDistanceThreshold.setText(Float.toString(sharedPreferences.getFloat("flingDistanceThreshold", 200f)));
		txtTimeThreshold.setText(Long.toString(sharedPreferences.getLong("flingTimeThreshold", 350)));
		txtAnimationTimeThreshold.setText(Long.toString(sharedPreferences.getLong("flingAnimationTime", 1000)));

		chkNearestEdge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkNearestCorner.isChecked() == true) {

					chkNearestCorner.setChecked(!chkNearestEdge.isChecked());

				}

			}
		});

		chkNearestCorner.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkNearestEdge.isChecked() == true) {

					chkNearestEdge.setChecked(!chkNearestCorner.isChecked());

				}

			}
		});

		btnFlingSettingClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				closeAction();

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void closeAction() {

		sharedPreferences.edit().putBoolean("disableFling", chkDisableGesture.isChecked()).commit();
		sharedPreferences.edit().putBoolean("nearestEdge", chkNearestEdge.isChecked()).commit();
		sharedPreferences.edit().putBoolean("nearestCorner", chkNearestCorner.isChecked()).commit();
		sharedPreferences.edit().putBoolean("flingOffEdge", chkFlingOffEdge.isChecked()).commit();
		sharedPreferences.edit().putFloat("flingDistanceThreshold", Float.parseFloat(txtDistanceThreshold.getText().toString())).commit();
		sharedPreferences.edit().putLong("flingTimeThreshold", Long.parseLong(txtTimeThreshold.getText().toString())).commit();
		sharedPreferences.edit().putLong("flingAnimationTime", Long.parseLong(txtAnimationTimeThreshold.getText().toString())).commit();
		onClickListener.onClick(getDialog(), 3);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		onClickListener = null;

	}

}
