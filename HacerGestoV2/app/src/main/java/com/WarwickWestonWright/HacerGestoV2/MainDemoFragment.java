/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class MainDemoFragment extends Fragment implements DialogInterface.OnClickListener {

	public MainDemoFragment() {}

	private View rootView;
	private TextView lblFlingMainStats;
	private TextView lblMoveMainStats;
	private TextView lblScaleMainStats;
	private TextView lblRotateMainStats;

	private MainDemoSettingsFragment mainDemoSettingsFragment;
	private SharedPreferences sp;
	private HacerGestos hacerGestoMainDemo;
	private HGRender hgRender;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;
	private final Point glCenterPoint = new Point();
	private final Point[] corners = new Point[4];
	private final float[] tolerances = new float[4];
	private boolean flingAnimationActive = false;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.main_demo_fragment, container, false);

		lblFlingMainStats = rootView.findViewById(R.id.lblFlingMainStats);
		lblMoveMainStats = rootView.findViewById(R.id.lblMoveMainStats);
		lblScaleMainStats = rootView.findViewById(R.id.lblScaleMainStats);
		lblRotateMainStats = rootView.findViewById(R.id.lblRotateMainStats);
		sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("the_real_magic_demo.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x / 2, "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1f, 0f);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		setupMainDemoFragment();
		setUpPreferenceValues();
		flingAnimationActive = false;

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setupMainDemoFragment() {

		/* Use this block if you want to use a custom loaded image */
		hgRender = new HGRender(getResources(), multipleTextures);
		hacerGestoMainDemo = rootView.findViewById(R.id.hacerGestoDemoView);
		hacerGestoMainDemo.setRenderer(hgRender);
		hacerGestoMainDemo.setQuickTapTime(65L);
		/* Use this block if you want to use a custom loaded image */

		/* Replace this block with the block above if you want to use a drawable from your XML layout */
		//hacerGestoMainDemo = rootView.findViewById(R.id.hacerGestoDemoView);
		/* Replace this block with the block above if you want to use a drawable from your XML layout */

		hacerGestoMainDemo.post(new Runnable() {
			@Override
			public void run() {

				int width = hacerGestoMainDemo.getWidth();
				int height = hacerGestoMainDemo.getHeight();
				Point centerPoint = new Point(width / 2, height / 2);
				int centerOfImage = width / 4;
				glCenterPoint.x = width / 2;
				glCenterPoint.y = height / 2;
				corners[0] = new Point(centerOfImage - centerPoint.x, centerPoint.y - centerOfImage);
				corners[1] = new Point(centerPoint.x - centerOfImage, centerPoint.y - centerOfImage);
				corners[2] = new Point(centerOfImage - centerPoint.x, centerOfImage - centerPoint.y);
				corners[3] = new Point(centerPoint.x - centerOfImage, centerOfImage - centerPoint.y);
				int tempTolerance = width / 10;
				tolerances[0] = tempTolerance;
				tolerances[1] = tempTolerance;
				tolerances[2] = tempTolerance;
				tolerances[3] = tempTolerance;
				hacerGestoMainDemo.invalidate();

			}
		});

		hacerGestoMainDemo.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {

				flingAnimationActive = false;
				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblFlingMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblMoveMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblScaleMainStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));
				lblRotateMainStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

			}

			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {

				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblFlingMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblMoveMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblScaleMainStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));
				lblRotateMainStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

			}

			@Override
			public void onPointerUp(HGResult hgResult) {}
			@Override
			public void onUp(HGResult hgResult) {

				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblFlingMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblMoveMainStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));
				lblScaleMainStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));
				lblRotateMainStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

				//Disable the default on#BackPressed behavior when a fling animation is running to prevent crash
				if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hacerGestoMainDemo.getHgFling().getFlingTriggered() == true) {

					flingAnimationActive = true;

				}
				else {

					flingAnimationActive = false;

				}

				if(hgResult.getWhatGesture() == HGResult.HACER_GESTO_GESTURE && hgResult.getQuickTap() == true && hacerGestoMainDemo.getHgFling().getFlingTriggered() == false) {

					if(mainDemoSettingsFragment == null) {

						mainDemoSettingsFragment = new MainDemoSettingsFragment();

					}
					else if(mainDemoSettingsFragment != null) {

						mainDemoSettingsFragment.dismiss();
						getActivity().getSupportFragmentManager().beginTransaction().remove(mainDemoSettingsFragment).commit();

					}

					mainDemoSettingsFragment.setCancelable(true);
					mainDemoSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("MainDemoFragment"), 2);
					mainDemoSettingsFragment.show(getActivity().getSupportFragmentManager(), "MainDemoSettingsFragment");

				}//End if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hgResult.getQuickTap() == true && hacerGestoMainDemo.getHgFling().getFlingTriggered() == false)

			}
		});

		hacerGestoMainDemo.invalidate();

	}//End private void setupMainDemoFragment()


	private void setUpPreferenceValues() {

		//Rotate Settings
		hacerGestoMainDemo.getHgRotate().setCumulativeRotate(sp.getBoolean("cumulativeRotate", true));
		hacerGestoMainDemo.getHgRotate().setIsSingleFinger(sp.getBoolean("isSingleFinger", true));
		hacerGestoMainDemo.getHgRotate().setPrecisionRotation(sp.getFloat("precisionRotation", 1.0f));

		if(sp.getBoolean("useAngleSnap", false) == true) {

			hacerGestoMainDemo.getHgRotate().setAngleSnapBaseOne(sp.getFloat("angleSnapBaseOne", 0.125f), sp.getFloat("angleSnapProximity", 0.03125f));

		}
		else if(sp.getBoolean("useAngleSnap", false) == false) {

			hacerGestoMainDemo.getHgRotate().setAngleSnapBaseOne(0, 0);

		}//End if(sharedPreferences.getBoolean("useAngleSnap", false) == true)

		if(sp.getBoolean("useMinMaxRotate", false) == true) {

			hacerGestoMainDemo.getHgRotate().setMinMaxDial(sp.getFloat("minimumRotation", -4.0f), sp.getFloat("maximumRotation", 3.0f), sp.getBoolean("useMinMaxRotate", false));

		}
		else if(sp.getBoolean("useMinMaxRotate", false) == false) {

			hacerGestoMainDemo.getHgRotate().setMinMaxDial(0, 0, sp.getBoolean("useMinMaxRotate", false));

		}//End if(sharedPreferences.getBoolean("useMinMaxRotate", false) == true)

		hacerGestoMainDemo.getHgRotate().setVariableDial(sp.getFloat("variableDialInner", 4.5f), sp.getFloat("variableDialOuter", 0.8f), sp.getBoolean("useVariableDial", false));

		if(sp.getBoolean("useVariableDialCurve", false) == true) {

			hacerGestoMainDemo.getHgRotate().setUseVariableDialCurve(true, sp.getBoolean("positiveCurve", false));

		}
		else if(sp.getBoolean("useVariableDialCurve", false) == false) {

			hacerGestoMainDemo.getHgRotate().setUseVariableDialCurve(false, sp.getBoolean("positiveCurve", false));

		}//End if(sharedPreferences.getBoolean("useVariableDialCurve", false) == true)

		if(sp.getBoolean("enableFling", false) == true) {

			hacerGestoMainDemo.getHgRotate().setFlingTolerance(sp.getInt("flingDistance", 200), sp.getLong("flingTime", 250L));
			hacerGestoMainDemo.getHgRotate().setSpinAnimation(sp.getInt("startSpeed", 10), sp.getInt("endSpeed", 0), sp.getLong("spinAnimationTime", 5000L));

		}
		else if(sp.getBoolean("enableFling", false) == false) {

			hacerGestoMainDemo.getHgRotate().setFlingTolerance(0, 0L);
			hacerGestoMainDemo.getHgRotate().setSpinAnimation(0, 0, 0L);

		}//End if(sp.getBoolean("enableFling", false) == true)

		//Developer note executing fling from spin works better than the other way around.
		hacerGestoMainDemo.setSpinTriggersFling(false);

		//Scale Settings
		hacerGestoMainDemo.getHgScale().setDisableGesture(sp.getBoolean("disableScale", false));

		if(sp.getBoolean("enableScaleSnapping", false) == true) {

			hacerGestoMainDemo.getHgScale().setSnapTolerance(sp.getFloat("scaleSnapSensitivity", 100f));

		}
		else if(sp.getBoolean("enableScaleSnapping", false) == false) {

			hacerGestoMainDemo.getHgScale().setSnapTolerance(0f);

		}//End if(sharedPreferences.getBoolean("enableScaleSnapping", false) == true)

		hacerGestoMainDemo.getHgScale().setUseMinMaxScale(sp.getBoolean("useMinMaxScale", false));
		hacerGestoMainDemo.getHgScale().setMinimumScale(sp.getFloat("minimumScale", 0.2f));
		hacerGestoMainDemo.getHgScale().setMaximumScale(sp.getFloat("maximumScale", 4.0f));

		//Move Settings
		if(sp.getBoolean("moveSnap", true) == true) {

			hacerGestoMainDemo.getHgMove().setMoveSnap(corners, tolerances);

		}
		else if(sp.getBoolean("moveSnap", true) == false) {

			hacerGestoMainDemo.getHgMove().setMoveSnap(null, null);

		}//End if(sharedPreferences.getBoolean("moveSnap", true) == true)

		hacerGestoMainDemo.getHgMove().setDisableGesture(sp.getBoolean("disableMove", true));
		hacerGestoMainDemo.getHgMove().setCumulativeMove(sp.getBoolean("cumulativeMove", true));

		//Fling Settings
		hacerGestoMainDemo.getHgFling().setDisableGesture(sp.getBoolean("disableFling", false));
		hacerGestoMainDemo.getHgFling().setFlingNearestEdge(sp.getBoolean("nearestEdge", false));
		hacerGestoMainDemo.getHgFling().setFlingNearestCorner(sp.getBoolean("nearestCorner", false));
		hacerGestoMainDemo.getHgFling().setFlingOffOfEdge(sp.getBoolean("flingOffEdge", true));
		hacerGestoMainDemo.getHgFling().setupFlingValues(sp.getFloat("flingDistanceThreshold", point.x / 5),
			sp.getLong("flingTimeThreshold", 350),
			sp.getLong("flingAnimationTime", 1000));

		//Developer note: executing spin from fling isn't as goood as the other way around.
		hacerGestoMainDemo.setFlingTriggersSpin(true);

	}//End private void setUpPreferenceValues()


	public boolean isFlingAnimationActive() {return hacerGestoMainDemo.isFlingAnimationInProgress();}//Depricated Need to Remove


	@Override
	public void onPause() {
		super.onPause();

		hacerGestoMainDemo.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		hacerGestoMainDemo.onResume();

	}


	//Used in main activity to ensure that fragment is not detached while a fling animation is in progress. This is to prevent a crash
	public boolean getFlingAnimationActive() {return this.flingAnimationActive;}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}

	@Override
	public void onDetach() {
		super.onDetach();

	}


	@Override
	public void onClick(DialogInterface dialog, int which) {

		dialog.dismiss();
		setUpPreferenceValues();

	}

}