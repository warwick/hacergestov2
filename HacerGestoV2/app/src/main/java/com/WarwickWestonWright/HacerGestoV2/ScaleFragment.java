/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class ScaleFragment extends Fragment implements DialogInterface.OnClickListener {

	public ScaleFragment() {}

	private ScaleSettingsFragment scaleSettingsFragment;
	private View rootView;
	private TextView lblScaleStats;
	private static SharedPreferences sharedPreferences;
	private HacerGestos hacerGestosScale;
	private HGRender hgRender;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.scale_fragment, container, false);

		lblScaleStats = rootView.findViewById(R.id.lblScaleStats);
		if(sharedPreferences == null) {sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());}
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("scale_demo.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x / 2, "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1f, 0f);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onPause() {
		super.onPause();

		hacerGestosScale.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		setupScaleView();
		setUpPreferenceValues();

		hacerGestosScale.onResume();

	}


	private void setupScaleView() {

		/* Use this block if you want to use a custom loaded image */
		//hgRender = new HGRender(getResources(), multipleTextures);
		//hacerGestosScale = (HacerGestos) rootView.findViewById(R.id.hacerGestoScale);
		//hacerGestosScale.setRenderer(hgRender);
		/* Use this block if you want to use a custom loaded image */

		/* Replace this block with the block above if you want to use a drawable from your XML layout */
		HGRender.setHgScale(2f, true);
		hacerGestosScale = rootView.findViewById(R.id.hacerGestoScale);
		/* Replace this block with the block above if you want to use a drawable from your XML layout */

		hacerGestosScale.getHgRotate().setPrecisionRotation(0f);
		hacerGestosScale.getHgMove().setDisableGesture(true);
		hacerGestosScale.getHgFling().setDisableGesture(true);

		hacerGestosScale.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {

				lblScaleStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));

			}

			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {

				lblScaleStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));

			}
			@Override
			public void onPointerUp(HGResult hgResult) {}

			@Override
			public void onUp(HGResult hgResult) {

				lblScaleStats.setText("W : " + String.format("%.2f", hgResult.getScaleX()) + "\n" + "H : " + String.format("%.2f", hgResult.getScaleY()));

				if(hgResult.getWhatGesture() == HGResult.SCALE_GESTURE && hgResult.getQuickTap() == true) {

					if(scaleSettingsFragment == null) {

						scaleSettingsFragment = new ScaleSettingsFragment();

					}
					else if(scaleSettingsFragment != null) {

						scaleSettingsFragment.dismiss();
						getActivity().getSupportFragmentManager().beginTransaction().remove(scaleSettingsFragment).commit();

					}

					if(scaleSettingsFragment == null) {scaleSettingsFragment = new ScaleSettingsFragment();}
					scaleSettingsFragment.setCancelable(true);
					scaleSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("ScaleFragment"), 1);
					scaleSettingsFragment.show(getActivity().getSupportFragmentManager(), "ScaleSettingsFragment");

				}//End if(hgResult.getWhatGesture() == HGResult.SCALE_GESTURE && hgResult.getQuickTap() == true)

			}
		});

	}//End private void setupScaleView()


	private void setUpPreferenceValues() {

		hacerGestosScale.getHgScale().setDisableGesture(sharedPreferences.getBoolean("disableScale", false));

		if(sharedPreferences.getBoolean("enableScaleSnapping", false) == true) {

			hacerGestosScale.getHgScale().setSnapTolerance(sharedPreferences.getFloat("scaleSnapSensitivity", 100f));

		}
		else /* if(sharedPreferences.getBoolean("enableSnapping", false) == false) */ {

			hacerGestosScale.getHgScale().setSnapTolerance(0f);

		}//End if(sharedPreferences.getBoolean("enableScaleSnapping", false) == true)

		hacerGestosScale.getHgScale().setUseMinMaxScale(sharedPreferences.getBoolean("useMinMaxScale", false));
		hacerGestosScale.getHgScale().setMinimumScale(sharedPreferences.getFloat("minimumScale", 0.2f));
		hacerGestosScale.getHgScale().setMaximumScale(sharedPreferences.getFloat("maximumScale", 4.0f));
		hacerGestosScale.requestRender();

	}//End private void setUpPreferenceValues()


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

	}


	@Override
	public void onClick(DialogInterface dialog, int which) {

		dialog.dismiss();
		setUpPreferenceValues();

	}

}