/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

public class MainDemoSettingsFragment extends DialogFragment {

	public MainDemoSettingsFragment() {}

	private DialogInterface.OnClickListener onClickListener;

	private View rootView;
	private Button btnMainDemoSettingsClose;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onClickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.main_demo_settings_fragment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		btnMainDemoSettingsClose = rootView.findViewById(R.id.btnMainDemoSettingsClose);

		btnMainDemoSettingsClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				closeAction();

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void closeAction() {

		onClickListener.onClick(getDialog(), 4);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		onClickListener = null;

	}

}