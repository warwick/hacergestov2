/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class MoveFragment extends Fragment implements DialogInterface.OnClickListener {

	public MoveFragment() {}

	private MoveSettingsFragment moveSettingsFragment;
	private View rootView;
	private TextView lblMoveStats;
	private static SharedPreferences sharedPreferences;
	private HacerGestos hacerGestosMove;
	private HGRender hgRender;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;
	private final Point glCenterPoint = new Point();
	private final Point[] corners = new Point[4];
	private static float[] tolerances;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.move_fragment, container, false);

		lblMoveStats = rootView.findViewById(R.id.lblMoveStats);
		if(sharedPreferences == null) {sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());}
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("move_demo.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x / 2, "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1f, 0f);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onPause() {
		super.onPause();

		hacerGestosMove.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		setupMoveView();
		setUpPreferenceValues();

		hacerGestosMove.onResume();

	}


	private void setupMoveView() {

		/* Use this block if you want to use a custom loaded image */
		//hgRender = new HGRender(getResources(), multipleTextures);
		//hacerGestosMove = rootView.findViewById(R.id.hacerGestoMove);
		//hacerGestosMove.setRenderer(hgRender);
		/* Use this block if you want to use a custom loaded image */

		/* Replace this block with the block above if you want to use a drawable from your XML layout */
		HGRender.setHgScale(HGRender.SCALE_TYPE_FIT_CENTER);
		hacerGestosMove = rootView.findViewById(R.id.hacerGestoMove);
		/* Replace this block with the block above if you want to use a drawable from your XML layout */

		hacerGestosMove.getHgRotate().setPrecisionRotation(0f);
		hacerGestosMove.getHgScale().setDisableGesture(true);
		hacerGestosMove.getHgFling().setDisableGesture(true);
		hacerGestosMove.getHgFling().setupFlingValues(point.y * 2, 0, 1);
		setMoveSnap();

		hacerGestosMove.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {

				lblMoveStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));

			}

			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {

				lblMoveStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));

			}

			@Override
			public void onPointerUp(HGResult hgResult) {}
			@Override
			public void onUp(HGResult hgResult) {

				lblMoveStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));

				if(hgResult.getWhatGesture() == HGResult.MOVE_GESTURE && hgResult.getQuickTap() == true) {

					if(moveSettingsFragment == null) {

						moveSettingsFragment = new MoveSettingsFragment();

					}
					else if (moveSettingsFragment != null) {

						moveSettingsFragment.dismiss();
						getActivity().getSupportFragmentManager().beginTransaction().remove(moveSettingsFragment).commit();

					}

					moveSettingsFragment.setCancelable(true);
					moveSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("MoveFragment"), 2);
					moveSettingsFragment.show(getActivity().getSupportFragmentManager(), "MoveSettingsFragment");

				}//End if(hgResult.getWhatGesture() == HGResult.MOVE_GESTURE && hgResult.getQuickTap() == true)

			}
		});

	}//End private void setupMoveView()


	private void setUpPreferenceValues() {

		hacerGestosMove.getHgMove().setDisableGesture(sharedPreferences.getBoolean("disableMove", true));
		hacerGestosMove.getHgMove().setCumulativeMove(sharedPreferences.getBoolean("cumulativeMove", true));
		setMoveSnap();
		hacerGestosMove.requestRender();

	}//End private void setUpPreferenceValues()


	private void setMoveSnap() {

		if(sharedPreferences.getBoolean("moveSnap", true)) {

			hacerGestosMove.post(new Runnable() {
				@Override
				public void run() {

					int width = hacerGestosMove.getWidth();
					int height = hacerGestosMove.getHeight();
					Point centerPoint = new Point(width / 2, height / 2);
					int centerOfImage = width / 4;
					glCenterPoint.x = width / 2;
					glCenterPoint.y = height / 2;
					corners[0] = new Point(centerOfImage - centerPoint.x, centerPoint.y - centerOfImage);
					corners[1] = new Point(centerPoint.x - centerOfImage, centerPoint.y - centerOfImage);
					corners[2] = new Point(centerOfImage - centerPoint.x, centerOfImage - centerPoint.y);
					corners[3] = new Point(centerPoint.x - centerOfImage, centerOfImage - centerPoint.y);
					int tempTolerance = width / 12;
					float[] tolerances = {tempTolerance, tempTolerance, tempTolerance, tempTolerance};
					MoveFragment.tolerances = new float[4];
					MoveFragment.tolerances = tolerances;
					hacerGestosMove.getHgMove().setMoveSnap(corners, tolerances);

				}
			});

		} else {

			hacerGestosMove.getHgMove().setMoveSnap(null, null);

		}//End if(sharedPreferences.getBoolean("cumulativeMove", true))

	}//End private void setMoveSnap()


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

	}


	@Override
	public void onClick(DialogInterface dialog, int which) {

		dialog.dismiss();
		setUpPreferenceValues();

	}

}