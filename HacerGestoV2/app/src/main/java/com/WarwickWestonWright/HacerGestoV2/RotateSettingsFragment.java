/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class RotateSettingsFragment extends DialogFragment {

	private DialogInterface.OnClickListener onClickListener;
	public RotateSettingsFragment() {}

	private View rootView;
	private CheckBox chkCumulativeRotate;
	private CheckBox chkIsSingleFinger;
	private EditText txtPrecisionRotation;
	private EditText txtAngleSnapBaseOne;
	private EditText txtAngleSnapProximity;
	private EditText txtMinimumRotation;
	private EditText txtMaximumRotation;
	private CheckBox chkUseAngleSnap;
	private CheckBox chkUseMinMax;
	private CheckBox chkUseVariableDial;
	private EditText txtVariableDialInner;
	private EditText txtVariableDialOuter;
	private CheckBox chkUseVariableDialCurve;
	private CheckBox chkPositiveCurve;
	private Button btnRotateSettingClose;
	private CheckBox chkEnableFling;
	private EditText txtFlingDistance;
	private EditText txtFlingTime;
	private EditText txtStartSpeed;
	private EditText txtEndSpeed;
	private EditText txtSpinAnimationTime;

	private SharedPreferences sp;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onClickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.rotate_settings_fragment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		chkCumulativeRotate = rootView.findViewById(R.id.chkCumulativeRotate);
		chkIsSingleFinger = rootView.findViewById(R.id.chkIsSingleFinger);
		txtPrecisionRotation = rootView.findViewById(R.id.txtPrecisionRotation);
		txtAngleSnapBaseOne = rootView.findViewById(R.id.txtAngleSnapBaseOne);
		txtAngleSnapProximity = rootView.findViewById(R.id.txtAngleSnapProximity);
		txtMinimumRotation = rootView.findViewById(R.id.txtMinimumRotation);
		txtMaximumRotation = rootView.findViewById(R.id.txtMaximumRotation);
		chkUseAngleSnap = rootView.findViewById(R.id.chkUseAngleSnap);
		chkUseMinMax = rootView.findViewById(R.id.chkUseMinMax);
		btnRotateSettingClose = rootView.findViewById(R.id.btnRotateSettingClose);
		chkUseVariableDial = rootView.findViewById(R.id.chkUseVariableDial);
		txtVariableDialInner = rootView.findViewById(R.id.txtVariableDialInner);
		txtVariableDialOuter = rootView.findViewById(R.id.txtVariableDialOuter);
		chkUseVariableDialCurve = rootView.findViewById(R.id.chkUseVariableDialCurve);
		chkPositiveCurve = rootView.findViewById(R.id.chkPositiveCurve);
		chkEnableFling = rootView.findViewById(R.id.chkEnableFling);
		txtFlingDistance = rootView.findViewById(R.id.txtFlingDistance);
		txtFlingTime = rootView.findViewById(R.id.txtFlingTime);
		txtStartSpeed = rootView.findViewById(R.id.txtStartSpeed);
		txtEndSpeed = rootView.findViewById(R.id.txtEndSpeed);
		txtSpinAnimationTime = rootView.findViewById(R.id.txtSpinAnimationTime);

		chkCumulativeRotate.setChecked(sp.getBoolean("cumulativeRotate", true));
		chkIsSingleFinger.setChecked(sp.getBoolean("isSingleFinger", true));
		txtPrecisionRotation.setText(Float.toString(sp.getFloat("precisionRotation", 1.0f)));
		txtAngleSnapBaseOne.setText(Float.toString(sp.getFloat("angleSnapBaseOne", 0.125f)));
		txtAngleSnapProximity.setText(Float.toString(sp.getFloat("angleSnapProximity", 0.03125f)));
		txtMinimumRotation.setText(Float.toString(sp.getFloat("minimumRotation", -3.6f)));
		txtMaximumRotation.setText(Float.toString(sp.getFloat("maximumRotation", 4.4f)));
		chkUseAngleSnap.setChecked(sp.getBoolean("useAngleSnap", false));
		chkUseMinMax.setChecked(sp.getBoolean("useMinMaxRotate", false));
		chkUseVariableDial.setChecked(sp.getBoolean("useVariableDial", false));
		txtVariableDialInner.setText(Float.toString(sp.getFloat("variableDialInner", 4.5f)));
		txtVariableDialOuter.setText(Float.toString(sp.getFloat("variableDialOuter", 0.8f)));
		chkUseVariableDialCurve.setChecked(sp.getBoolean("useVariableDialCurve", false));
		chkPositiveCurve.setChecked(sp.getBoolean("positiveCurve", false));
		chkEnableFling.setChecked(sp.getBoolean("enableFling", false));
		txtFlingDistance.setText(Integer.toString(sp.getInt("flingDistance", 200)));
		txtFlingTime.setText(Long.toString(sp.getLong("txtFlingTime", 250L)));
		txtStartSpeed.setText(Integer.toString(sp.getInt("startSpeed", 10)));
		txtEndSpeed.setText(Integer.toString(sp.getInt("endSpeed", 0)));
		txtSpinAnimationTime.setText(Long.toString(sp.getLong("spinAnimationTime", 5000L)));

		btnRotateSettingClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				closeAction();
				onClickListener.onClick(getDialog(), 0);

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void closeAction() {

		//Set Default Values for empty text controls.
		if(txtPrecisionRotation.getText().toString().isEmpty()) {txtPrecisionRotation.setText(Float.toString(sp.getFloat("precisionRotation", 1.0f)));}
		if(txtAngleSnapBaseOne.getText().toString().isEmpty()) {txtAngleSnapBaseOne.setText(Float.toString(sp.getFloat("angleSnapBaseOne", 0.125f)));}
		if(txtAngleSnapProximity.getText().toString().isEmpty()) {txtAngleSnapProximity.setText(Float.toString(sp.getFloat("angleSnapProximity", 0.03125f)));}
		if(txtMinimumRotation.getText().toString().isEmpty()) {txtMinimumRotation.setText(Float.toString(sp.getFloat("minimumRotation", -3.6f)));}
		if(txtMaximumRotation.getText().toString().isEmpty()) {txtMaximumRotation.setText(Float.toString(sp.getFloat("maximumRotation", 4.4f)));}
		if(txtVariableDialInner.getText().toString().isEmpty()) {txtVariableDialInner.setText(Float.toString(sp.getFloat("variableDialInner", 4.5f)));}
		if(txtVariableDialOuter.getText().toString().isEmpty()) {txtVariableDialOuter.setText(Float.toString(sp.getFloat("variableDialOuter", 0.8f)));}
		if(txtFlingDistance.getText().toString().isEmpty()) {txtFlingDistance.setText(Integer.toString(sp.getInt("flingDistance", 200)));}
		if(txtFlingTime.getText().toString().isEmpty()) {txtFlingTime.setText(Long.toString(sp.getLong("txtFlingTime", 250L)));}
		if(txtStartSpeed.getText().toString().isEmpty()) {txtStartSpeed.setText(Integer.toString(sp.getInt("startSpeed", 10)));}
		if(txtEndSpeed.getText().toString().isEmpty()) {txtEndSpeed.setText(Integer.toString(sp.getInt("endSpeed", 0)));}
		if(txtSpinAnimationTime.getText().toString().isEmpty()) {txtSpinAnimationTime.setText(Long.toString(sp.getLong("spinAnimationTime", 5000L)));}

		//Persist values on fragment exit.
		sp.edit().putBoolean("cumulativeRotate", chkCumulativeRotate.isChecked()).commit();
		sp.edit().putBoolean("isSingleFinger", chkIsSingleFinger.isChecked()).commit();
		sp.edit().putFloat("precisionRotation", Float.parseFloat(txtPrecisionRotation.getText().toString())).commit();
		sp.edit().putFloat("angleSnapBaseOne", Float.parseFloat(txtAngleSnapBaseOne.getText().toString())).commit();
		sp.edit().putFloat("angleSnapProximity", Float.parseFloat(txtAngleSnapProximity.getText().toString())).commit();
		sp.edit().putFloat("minimumRotation", Float.parseFloat(txtMinimumRotation.getText().toString())).commit();
		sp.edit().putFloat("maximumRotation", Float.parseFloat(txtMaximumRotation.getText().toString())).commit();
		sp.edit().putBoolean("useAngleSnap", chkUseAngleSnap.isChecked()).commit();
		sp.edit().putBoolean("useMinMaxRotate", chkUseMinMax.isChecked()).commit();
		sp.edit().putBoolean("useVariableDial", chkUseVariableDial.isChecked()).commit();
		sp.edit().putFloat("variableDialInner", Float.parseFloat(txtVariableDialInner.getText().toString())).commit();
		sp.edit().putFloat("variableDialOuter", Float.parseFloat(txtVariableDialOuter.getText().toString())).commit();
		sp.edit().putBoolean("useVariableDialCurve", chkUseVariableDialCurve.isChecked()).commit();
		sp.edit().putBoolean("positiveCurve", chkPositiveCurve.isChecked()).commit();
		sp.edit().putBoolean("enableFling", chkEnableFling.isChecked()).commit();
		sp.edit().putInt("flingDistance", Integer.parseInt(txtFlingDistance.getText().toString())).commit();
		sp.edit().putLong("txtFlingTime", Long.parseLong(txtFlingTime.getText().toString())).commit();
		sp.edit().putInt("startSpeed", Integer.parseInt(txtStartSpeed.getText().toString())).commit();
		sp.edit().putInt("endSpeed", Integer.parseInt(txtEndSpeed.getText().toString())).commit();
		sp.edit().putLong("spinAnimationTime", Long.parseLong(txtSpinAnimationTime.getText().toString())).commit();

	}//End private void closeAction()


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		onClickListener = null;

	}

}