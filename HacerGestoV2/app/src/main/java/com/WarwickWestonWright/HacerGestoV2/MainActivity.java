/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity implements MainFragment.IMainFragment {

	private MainFragment mainFragment;
	private RotateFragment rotateFragment;
	private ScaleFragment scaleFragment;
	private MoveFragment moveFragment;
	private FlingFragment flingFragment;
	private MainDemoFragment mainDemoFragment;
	private SharedPreferences sp;
	private Point point;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View decorView = getWindow().getDecorView();
		//Hide the status bar.
		int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);

		setContentView(R.layout.main_activity);

		sp = PreferenceManager.getDefaultSharedPreferences(this);
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

		if(sp.getBoolean("spIsSetup", false) == false) {

			//Initialise Rotate settings.
			//Rotate Values
			sp.edit().putBoolean("cumulativeRotate", true).commit();
			sp.edit().putBoolean("isSingleFinger", true).commit();
			sp.edit().putFloat("precisionRotation", 1).commit();
			sp.edit().putFloat("angleSnapBaseOne", 0.125f).commit();
			sp.edit().putFloat("angleSnapProximity", 0.03125f).commit();
			sp.edit().putFloat("minimumRotation", -3.6f).commit();
			sp.edit().putFloat("maximumRotation", 4.4f).commit();
			sp.edit().putBoolean("useAngleSnap", false).commit();
			sp.edit().putBoolean("useMinMaxRotate", false).commit();
			sp.edit().putBoolean("useVariableDial", false).commit();
			sp.edit().putFloat("variableDialInner", 4.5f).commit();
			sp.edit().putFloat("variableDialOuter", 0.8f).commit();
			sp.edit().putBoolean("useVariableDialCurve", false).commit();
			sp.edit().putBoolean("positiveCurve", false).commit();
			sp.edit().putBoolean("enableFling", false).commit();
			sp.edit().putInt("flingDistance", 200).commit();
			sp.edit().putLong("flingTime", 250L).commit();
			sp.edit().putInt("startSpeed", 10).commit();
			sp.edit().putInt("endSpeed", 0).commit();
			sp.edit().putLong("spinAnimationTime", 5000L).commit();

			//Scale Values
			sp.edit().putBoolean("disableScale", false).commit();
			sp.edit().putBoolean("enableScaleSnapping", false).commit();
			sp.edit().putFloat("scaleSnapSensitivity", 100f).commit();
			sp.edit().putBoolean("useMinMaxScale", false).commit();
			sp.edit().putFloat("minimumScale", 0.2f).commit();
			sp.edit().putFloat("maximumScale", 4.0f).commit();

			//Move Values
			sp.edit().putBoolean("disableMove", false).commit();
			sp.edit().putBoolean("cumulativeMove", true).commit();
			sp.edit().putBoolean("moveSnap", false).commit();

			//Fling Values
			sp.edit().putBoolean("disableFling", false).commit();
			sp.edit().putBoolean("nearestEdge", false).commit();
			sp.edit().putBoolean("nearestCorner", false).commit();
			sp.edit().putBoolean("flingOffEdge", true).commit();
			sp.edit().putFloat("flingDistanceThreshold", point.x / 5).commit();
			sp.edit().putLong("flingTimeThreshold", 350).commit();
			sp.edit().putLong("flingAnimationTime", 1000).commit();

			sp.edit().putBoolean("spIsSetup", true).commit();

		}//End if(sharedPreferences.getBoolean("spIsSetup", false) == false)

		if(mainFragment == null) {

			mainFragment = new MainFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

		}

	}//End protected void onCreate(Bundle savedInstanceState)


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);

		return true;

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void mainFragmentCallback(String fragmentName) {

		if(fragmentName == RotateFragment.class.getName()) {

			if(rotateFragment == null) {rotateFragment = new RotateFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, rotateFragment, "RotateFragment").commit();

		}
		else if(fragmentName == ScaleFragment.class.getName()) {

			if(scaleFragment == null) {scaleFragment = new ScaleFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, scaleFragment, "ScaleFragment").commit();

		}
		else if(fragmentName == MoveFragment.class.getName()) {

			if(moveFragment == null) {moveFragment = new MoveFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, moveFragment, "MoveFragment").commit();

		}
		else if(fragmentName == FlingFragment.class.getName()) {

			if(flingFragment == null) {flingFragment = new FlingFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, flingFragment, "FlingFragment").commit();

		}
		else if(fragmentName == MainDemoFragment.class.getName()) {

			if(mainDemoFragment == null) {mainDemoFragment = new MainDemoFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainDemoFragment, "MainDemoFragment").commit();

		}//End if(fragmentName == RotateFragment.class.getName())

	}//End public void mainFragmentCallback(String fragmentName)


	@Override
	public void onBackPressed() {

		mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag("MainFragment");

		if(flingFragment != null && flingFragment.isAdded() == true) {

			if(flingFragment.isFlingAnimationActive() == false) {

				if(mainFragment == null) {mainFragment = new MainFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

			}

		}
		else if(moveFragment != null && moveFragment.isAdded() == true) {

			if(mainFragment == null) {mainFragment = new MainFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

		}
		else if(scaleFragment != null && scaleFragment.isAdded() == true) {

			if(mainFragment == null) {mainFragment = new MainFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

		}
		else if(rotateFragment != null && rotateFragment.isAdded() == true) {

			if(mainFragment == null) {mainFragment = new MainFragment();}
			getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

		}
		else if(mainDemoFragment != null && mainDemoFragment.isAdded() == true) {

			if(mainDemoFragment.isFlingAnimationActive() == false) {

				if(mainFragment == null) {mainFragment = new MainFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

			}

		}
		else if(mainFragment != null && mainFragment.isAdded() == true) {

			if(mainFragment.isFlingAnimationActive() == false) {

				super.onBackPressed();

			}

		}
		else {

			super.onBackPressed();

		}//End if(flingFragment != null && flingFragment.isAdded() == true && flingFragment.isFlingAnimationActive() == false)

	}//End public void onBackPressed()

}