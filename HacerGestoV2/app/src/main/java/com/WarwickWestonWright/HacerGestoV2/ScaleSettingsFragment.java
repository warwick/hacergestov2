/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class ScaleSettingsFragment extends DialogFragment {

	private DialogInterface.OnClickListener onClickListener;
	public ScaleSettingsFragment() {}

	private View rootView;
	private CheckBox chkDisableGesture;
	private CheckBox chkEnableSnapping;
	private EditText txtSnappingSensitivity;
	private CheckBox chkUseMinMax;
	private EditText txtMinimumScale;
	private EditText txtMaximumScale;
	private Button btnScaleSettingClose;
	private static SharedPreferences sharedPreferences;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onClickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.scale_settings_fragment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
		chkDisableGesture = rootView.findViewById(R.id.chkDisableGesture);
		chkEnableSnapping = rootView.findViewById(R.id.chkEnableSnapping);
		txtSnappingSensitivity = rootView.findViewById(R.id.txtSnappingSensitivity);
		chkUseMinMax = rootView.findViewById(R.id.chkUseMinMax);
		txtMinimumScale = rootView.findViewById(R.id.txtMinimumScale);
		txtMaximumScale = rootView.findViewById(R.id.txtMaximumScale);
		btnScaleSettingClose = rootView.findViewById(R.id.btnScaleSettingClose);

		chkDisableGesture.setChecked(sharedPreferences.getBoolean("disableScale", false));
		chkEnableSnapping.setChecked(sharedPreferences.getBoolean("enableScaleSnapping", false));
		txtSnappingSensitivity.setText(Float.toString(sharedPreferences.getFloat("scaleSnapSensitivity", 100f)));
		chkUseMinMax.setChecked(sharedPreferences.getBoolean("useMinMaxScale", false));
		txtMinimumScale.setText(Float.toString(sharedPreferences.getFloat("minimumScale", 0.2f)));
		txtMaximumScale.setText(Float.toString(sharedPreferences.getFloat("maximumScale", 4.0f)));

		btnScaleSettingClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				closeAction();

			}
		});

		return rootView;

	}


	private void closeAction() {

		sharedPreferences.edit().putBoolean("disableScale", chkDisableGesture.isChecked()).commit();
		sharedPreferences.edit().putBoolean("enableScaleSnapping", chkEnableSnapping.isChecked()).commit();
		sharedPreferences.edit().putFloat("scaleSnapSensitivity", Float.parseFloat(txtSnappingSensitivity.getText().toString())).commit();
		sharedPreferences.edit().putBoolean("useMinMaxScale", chkUseMinMax.isChecked()).commit();
		sharedPreferences.edit().putFloat("minimumScale", Float.parseFloat(txtMinimumScale.getText().toString())).commit();
		sharedPreferences.edit().putFloat("maximumScale", Float.parseFloat(txtMaximumScale.getText().toString())).commit();
		onClickListener.onClick(getDialog(), 1);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		onClickListener = null;

	}

}