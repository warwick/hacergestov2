/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class FlingFragment extends Fragment implements DialogInterface.OnClickListener {

	public FlingFragment() {}

	private FlingSettingsFragment flingSettingsFragment;
	private View rootView;
	private TextView lblFlingStats;
	private static SharedPreferences sharedPreferences;
	private HacerGestos hacerGestosFling;
	private HGRender hgRender;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;
	private static boolean flingAnimationActive = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fling_fragment, container, false);

		lblFlingStats = rootView.findViewById(R.id.lblFlingStats);
		if(sharedPreferences == null) {sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());}
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("fling_demo.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x / 2, "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1f, 0f);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		setupFlingView();
		setUpPreferenceValues();
		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onPause() {
		super.onPause();

		hacerGestosFling.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		hacerGestosFling.onResume();

	}


	private void setupFlingView() {

		/* Use this block if you want to use a custom loaded image */
		hgRender = new HGRender(getResources(), multipleTextures);
		hacerGestosFling = rootView.findViewById(R.id.hacerGestoFling);
		hacerGestosFling.setRenderer(hgRender);
		/* Use this block if you want to use a custom loaded image */

		/* Replace this block with the block above if you want to use a drawable from your XML layout */
		//hacerGestosFling = rootView.findViewById(R.id.hacerGestoFling);
		/* Replace this block with the block above if you want to use a drawable from your XML layout */


		hacerGestosFling.getHgFling().setFlingBounceBack(true);
		hacerGestosFling.getHgRotate().setPrecisionRotation(0f);
		hacerGestosFling.getHgScale().setDisableGesture(true);
		hacerGestosFling.getHgMove().setDisableGesture(true);

		hacerGestosFling.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {flingAnimationActive = false;}
			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {

				lblFlingStats.setText("X : " + String.format("%04d", (int) hgResult.getXPos()) + "\n" + "Y : " + String.format("%04d", (int) hgResult.getYPos()));

			}

			@Override
			public void onPointerUp(HGResult hgResult) {}

			@Override
			public void onUp(HGResult hgResult) {

				//Disable the default on#BackPressed behavior when a fling animation is running to prevent crash
				//isFlingAnimationInProgress() Depricated Need to Remove
				if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hacerGestosFling.isFlingAnimationInProgress() == true) {

					flingAnimationActive = true;

				}
				else {

					flingAnimationActive = false;

				}

				if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hgResult.getQuickTap() == true && hacerGestosFling.getHgFling().getFlingTriggered() == false) {

					if(flingSettingsFragment == null) {

						flingSettingsFragment = new FlingSettingsFragment();

					}
					else if(flingSettingsFragment != null) {

						flingSettingsFragment.dismiss();
						getActivity().getSupportFragmentManager().beginTransaction().remove(flingSettingsFragment).commit();

					}

					flingSettingsFragment = new FlingSettingsFragment();
					flingSettingsFragment.setCancelable(true);
					flingSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("FlingFragment"), 3);
					flingSettingsFragment.show(getActivity().getSupportFragmentManager(), "FlingSettingsFragment");

				}//End if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hgResult.getQuickTap() == true && hacerGestosFling.getHgFling().getFlingTriggered() == false)

			}
		});

	}//End private void setupFlingView()


	private void setUpPreferenceValues() {

		hacerGestosFling.getHgFling().setDisableGesture(sharedPreferences.getBoolean("disableFling", false));
		hacerGestosFling.getHgFling().setFlingNearestEdge(sharedPreferences.getBoolean("nearestEdge", false));
		hacerGestosFling.getHgFling().setFlingNearestCorner(sharedPreferences.getBoolean("nearestCorner", false));
		hacerGestosFling.getHgFling().setFlingOffOfEdge(sharedPreferences.getBoolean("flingOffEdge", true));
		hacerGestosFling.getHgFling().setupFlingValues(sharedPreferences.getFloat("flingDistanceThreshold", point.x / 5),
			sharedPreferences.getLong("flingTimeThreshold", 350),
			sharedPreferences.getLong("flingAnimationTime", 1000));
		hacerGestosFling.requestRender();

	}//End private void setUpPreferenceValues()


	public boolean isFlingAnimationActive() {return hacerGestosFling.isFlingAnimationInProgress();}//Depricated Need to Remove


	//Used in main activity to ensure that fragment is not detached while a fling animation is in progress to prevent a crash
	public static boolean getFlingAnimationActive() {return FlingFragment.flingAnimationActive;}
	public static void setFlingAnimationActive(boolean flingAnimationActive) {FlingFragment.flingAnimationActive = flingAnimationActive;}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

	}


	@Override
	public void onClick(DialogInterface dialog, int which) {

		dialog.dismiss();
		setUpPreferenceValues();

	}

}