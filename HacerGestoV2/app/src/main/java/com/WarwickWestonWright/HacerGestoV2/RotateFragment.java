/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class RotateFragment extends Fragment implements DialogInterface.OnClickListener {

	public RotateFragment() {}

	private View rootView;
	private TextView lblRotateStats;
	private RotateSettingsFragment rotateSettingsFragment;
	private SharedPreferences sp;
	private HacerGestos hacerGestoRotate;
	private HGRender hgRender;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.rotate_fragment, container, false);

		lblRotateStats = rootView.findViewById(R.id.lblRotateStats);
		if(sp == null) {sp = PreferenceManager.getDefaultSharedPreferences(getContext());}
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("rotate_demo.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, (int) (point.x / 1.2), "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1f, 0f);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		setupRotateView();
		setUpPreferenceValues();
		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setupRotateView() {

		/* Use this block if you want to use a custom loaded image */
		//hgRender = new HGRender(getResources(), R.drawable.rotate_demo);
		hgRender = new HGRender(getResources(), multipleTextures);
		hacerGestoRotate = rootView.findViewById(R.id.hacerGestoRotate);
		hacerGestoRotate.setRenderer(hgRender);
		//hacerGestoRotate.getHgRotate().setFlingTolerance(150, 225);
		//hacerGestoRotate.getHgRotate().setSpinAnimation(10f, 0f, 5000l);
		/* Use this block if you want to use a custom loaded image */

		/* Replace this block with the block above if you want to use a drawable from your XML layout */
		//hacerGestosRotate = rootView.findViewById(R.id.hacerGestoRotate);
		/* Replace this block with the block above if you want to use a drawable from your XML layout */

		hacerGestoRotate.getHgScale().setDisableGesture(true);
		hacerGestoRotate.getHgMove().setDisableGesture(true);
		hacerGestoRotate.getHgFling().setDisableGesture(true);
		hacerGestoRotate.getHgFling().setupFlingValues(point.y * 2, 0, 1);

		hacerGestoRotate.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {

				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblRotateStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

			}

			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {

				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblRotateStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

			}

			@Override
			public void onPointerUp(HGResult hgResult) {}

			@Override
			public void onUp(HGResult hgResult) {

				final double fullObjectAngle = hgResult.getObjectRotationCount() + hgResult.getObjectAngleBaseOne();
				final double fullGestureAngle = hgResult.getGestureRotationCount() + hgResult.getGestureAngleBaseOne();
				lblRotateStats.setText("O : " + String.format("%.2f", fullObjectAngle) + "\n" + "G : " + String.format("%.2f", fullGestureAngle));

				if(hgResult.getWhatGesture() == HGResult.ROTATE_GESTURE && hgResult.getQuickTap() == true) {

					if(rotateSettingsFragment == null) {

						rotateSettingsFragment = new RotateSettingsFragment();

					}
					else if(rotateSettingsFragment != null) {

						rotateSettingsFragment.dismiss();
						getActivity().getSupportFragmentManager().beginTransaction().remove(rotateSettingsFragment).commit();

					}

					if(rotateSettingsFragment == null) {rotateSettingsFragment = new RotateSettingsFragment();}
					rotateSettingsFragment.setCancelable(true);
					rotateSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("RotateFragment"), 0);
					rotateSettingsFragment.show(getActivity().getSupportFragmentManager(), "RotateSettingsFragment");

				}//End if(hgResult.getWhatGesture() == HGResult.ROTATE_GESTURE && hgResult.getQuickTap() == true)

			}
		});

	}//End private void setupRotateView()


	private void setUpPreferenceValues() {

		hacerGestoRotate.getHgRotate().setCumulativeRotate(sp.getBoolean("cumulativeRotate", true));
		hacerGestoRotate.getHgRotate().setIsSingleFinger(sp.getBoolean("isSingleFinger", true));
		hacerGestoRotate.getHgRotate().setPrecisionRotation(sp.getFloat("precisionRotation", 1.0f));

		if(sp.getBoolean("useAngleSnap", false) == true) {

			hacerGestoRotate.getHgRotate().setAngleSnapBaseOne(sp.getFloat("angleSnapBaseOne", 0.125f), sp.getFloat("angleSnapProximity", 0.03125f));

		}
		else if(sp.getBoolean("useAngleSnap", false) == false) {

			hacerGestoRotate.getHgRotate().setAngleSnapBaseOne(0, 0);

		}//End if(sharedPreferences.getBoolean("useAngleSnap", false) == true)

		if(sp.getBoolean("useMinMaxRotate", false) == true) {

			hacerGestoRotate.getHgRotate().setMinMaxDial(sp.getFloat("minimumRotation", -3.6f), sp.getFloat("maximumRotation", 4.4f), sp.getBoolean("useMinMaxRotate", false));

		}
		else if(sp.getBoolean("useMinMaxRotate", false) == false) {

			hacerGestoRotate.getHgRotate().setMinMaxDial(0, 0, sp.getBoolean("useMinMaxRotate", false));

		}//End if(sharedPreferences.getBoolean("useMinMaxRotate", false) == true)

		hacerGestoRotate.getHgRotate().setVariableDial(sp.getFloat("variableDialInner", 4.5f), sp.getFloat("variableDialOuter", 0.8f), sp.getBoolean("useVariableDial", false));

		if(sp.getBoolean("useVariableDialCurve", false) == true) {

			hacerGestoRotate.getHgRotate().setUseVariableDialCurve(true, sp.getBoolean("positiveCurve", false));

		}
		else if(sp.getBoolean("useVariableDialCurve", false) == false) {

			hacerGestoRotate.getHgRotate().setUseVariableDialCurve(false, sp.getBoolean("positiveCurve", false));

		}//End if(sharedPreferences.getBoolean("useVariableDialCurve", false) == true)

		if(sp.getBoolean("enableFling", false) == true) {

			hacerGestoRotate.getHgRotate().setFlingTolerance(sp.getInt("flingDistance", 200), sp.getLong("flingTime", 250L));
			hacerGestoRotate.getHgRotate().setSpinAnimation(sp.getInt("startSpeed", 10), sp.getInt("endSpeed", 0), sp.getLong("spinAnimationTime", 5000L));

		}
		else if(sp.getBoolean("enableFling", false) == false) {

			hacerGestoRotate.getHgRotate().setFlingTolerance(0, 0l);
			hacerGestoRotate.getHgRotate().setSpinAnimation(0, 0, 0l);

		}//End if(sp.getBoolean("enableFling", false) == true)

	}//End private void setUpPreferenceValues()


	@Override
	public void onPause() {
		super.onPause();

		hacerGestoRotate.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		hacerGestoRotate.onResume();

	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

	}


	@Override
	public void onClick(DialogInterface dialog, int which) {

		dialog.dismiss();
		setUpPreferenceValues();

	}

}