/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;

public class MoveSettingsFragment extends DialogFragment {

	public MoveSettingsFragment() {}

	private DialogInterface.OnClickListener onClickListener;

	private View rootView;
	private CheckBox chkDisableGesture;
	private CheckBox chkCumulativeMove;
	private CheckBox chkMoveSnap;
	private Button btnMoveSettingClose;
	private static SharedPreferences sharedPreferences;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onClickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.move_settings_fragment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
		chkDisableGesture = rootView.findViewById(R.id.chkDisableGesture);
		chkCumulativeMove = rootView.findViewById(R.id.chkCumulativeMove);
		chkMoveSnap = rootView.findViewById(R.id.chkMoveSnap);
		btnMoveSettingClose = rootView.findViewById(R.id.btnMoveSettingClose);

		chkDisableGesture.setChecked(sharedPreferences.getBoolean("disableMove", false));
		chkCumulativeMove.setChecked(sharedPreferences.getBoolean("cumulativeMove", true));
		chkMoveSnap.setChecked(sharedPreferences.getBoolean("moveSnap", false));

		btnMoveSettingClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				closeAction();

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void closeAction() {

		sharedPreferences.edit().putBoolean("disableMove", chkDisableGesture.isChecked()).commit();
		sharedPreferences.edit().putBoolean("cumulativeMove", chkCumulativeMove.isChecked()).commit();
		sharedPreferences.edit().putBoolean("moveSnap", chkMoveSnap.isChecked()).commit();
		onClickListener.onClick(getDialog(), 2);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		onClickListener = null;

	}

}