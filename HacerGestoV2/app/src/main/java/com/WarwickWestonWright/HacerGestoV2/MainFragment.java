/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HacerGestoV2;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.WarwickWestonWright.HacerGesto.HGRender;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HacerGestos;
import com.WarwickWestonWright.HacerGesto.ImageUtilities;
import com.WarwickWestonWright.HacerGesto.MultipleTextures;

import java.io.IOException;
import java.io.InputStream;

public class MainFragment extends Fragment {

	public interface IMainFragment {

		void mainFragmentCallback(String fragmentName);

	}

	private IMainFragment iMainFragment;

	private HacerGestos hacerGestos;
	private HGRender hgRender;
	private ImageView imgUpperLeft;
	private ImageView imgUpperRight;
	private ImageView imgLowerLeft;
	private ImageView imgLowerRight;
	private AssetManager assetManager;
	private InputStream assetStream;
	private Bitmap bitmap;
	private MultipleTextures[] multipleTextures;
	private Point point;
	private final Point glCenterPoint = new Point();
	private final Point[] corners = new Point[5];
	private View rootView;
	private boolean moveHasSnapped;
	private boolean flingHasSnapped;


	@Override
	public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.main_fragment, container, false);

		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		imgUpperLeft = rootView.findViewById(R.id.imgUpperLeft);
		imgUpperRight = rootView.findViewById(R.id.imgUpperRight);
		imgLowerLeft = rootView.findViewById(R.id.imgLowerLeft);
		imgLowerRight = rootView.findViewById(R.id.imgLowerRight);

		assetManager = getActivity().getAssets();

		try {

			assetStream = assetManager.open("the_real_magic_center.png");
			bitmap = BitmapFactory.decodeStream(assetStream);
			bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x / 3, "X");
			multipleTextures = new MultipleTextures[1];
			multipleTextures[0] = new MultipleTextures(bitmap, new Point(), 1, 0);

		}
		catch(IOException e) {

			e.printStackTrace();

		}

		imgUpperLeft.post(new Runnable() {
			@Override
			public void run() {

				imgUpperLeft.setScaleType(ImageView.ScaleType.MATRIX);
				float width = imgUpperLeft.getWidth();
				float scale = (float) (point.x / 3) / width;
				imgUpperLeft.getImageMatrix().postScale(scale, scale);
				imgUpperLeft.postInvalidate();

			}
		});

		imgUpperRight.post(new Runnable() {
			@Override
			public void run() {

				imgUpperRight.setScaleType(ImageView.ScaleType.MATRIX);
				float width = imgUpperRight.getWidth();
				float scale = (float) (point.x / 3) / width;
				imgUpperRight.getImageMatrix().postScale(scale, scale);
				float offset = imgUpperRight.getWidth() - (imgUpperRight.getWidth() * scale);
				imgUpperRight.getImageMatrix().postTranslate(offset, 0);
				imgUpperRight.postInvalidate();

			}
		});

		imgLowerLeft.post(new Runnable() {
			@Override
			public void run() {

				imgLowerLeft.setScaleType(ImageView.ScaleType.MATRIX);
				float width = imgLowerLeft.getWidth();
				float scale = (float) (point.x / 3) / width;
				imgLowerLeft.getImageMatrix().postScale(scale, scale);
				float offset = imgLowerLeft.getWidth() - (imgLowerLeft.getWidth() * scale);
				imgLowerLeft.getImageMatrix().postTranslate(0, offset);
				imgLowerLeft.postInvalidate();

			}
		});

		imgLowerRight.post(new Runnable() {
			@Override
			public void run() {

				imgLowerRight.setScaleType(ImageView.ScaleType.MATRIX);
				float width = imgLowerRight.getWidth();
				float scale = (float) (point.x / 3) / width;
				imgLowerRight.getImageMatrix().postScale(scale, scale);
				float offset = imgLowerRight.getWidth() - (imgLowerRight.getWidth() * scale);
				imgLowerRight.getImageMatrix().postTranslate(offset, offset);
				imgLowerRight.postInvalidate();

			}
		});

		setupMainFragment();

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void launchSelectedFragment(final Point position) {

		//Upper Left
		if(position.x < 0 && position.y > 0) {

			iMainFragment.mainFragmentCallback(FlingFragment.class.getName());

		}
		//Upper Right
		else if(position.x > 0 && position.y > 0) {

			iMainFragment.mainFragmentCallback(MoveFragment.class.getName());

		}
		//Lower Left
		else if(position.x < 0 && position.y < 0) {

			iMainFragment.mainFragmentCallback(ScaleFragment.class.getName());

		}
		//Lower Right
		else if(position.x > 0 && position.y < 0) {

			iMainFragment.mainFragmentCallback(RotateFragment.class.getName());

		}
		else if(position.x == 0 && position.y == 0) {

			iMainFragment.mainFragmentCallback(MainDemoFragment.class.getName());

		}//End if(position.x < 0 && position.y > 0)

	}//End private void launchSelectedFragment(final Point position)


	private void setupMainFragment() {

        /* Use this block if you want to use a custom loaded image */
		//hgRender = new HGRender(getResources(), multipleTextures);
		//hacerGestos = rootView.findViewById(R.id.hacerGestoView);
		//hacerGestos.setRenderer(hgRender);
        /* Use this block if you want to use a custom loaded image */

        /* Replace this block with the block above if you want to use a drawable from your XML layout */
		//HGRender.setHgScale(HGRender.SCALE_TYPE_FIT_CENTER);//Library Default
		HGRender.setHgScale(3f, true);
		hacerGestos = rootView.findViewById(R.id.hacerGestoView);
        /* Replace this block with the block above if you want to use a drawable from your XML layout */

		hacerGestos.post(new Runnable() {
			@Override
			public void run() {

				int width = hacerGestos.getWidth();
				int height = hacerGestos.getHeight();
				Point centerPoint = new Point(width / 2, height / 2);
				int centerOfImage = width / 6;
				glCenterPoint.x = width / 2;
				glCenterPoint.y = height / 2;
				corners[0] = new Point(centerOfImage - centerPoint.x, centerPoint.y - centerOfImage);
				corners[1] = new Point(centerPoint.x - centerOfImage, centerPoint.y - centerOfImage);
				corners[2] = new Point(centerOfImage - centerPoint.x, centerOfImage - centerPoint.y);
				corners[3] = new Point(centerPoint.x - centerOfImage, centerOfImage - centerPoint.y);
				corners[4] = new Point(0, 0);
				int tempTolerance = width / 10;
				float[] tolerances = {tempTolerance, tempTolerance, tempTolerance, tempTolerance, tempTolerance};
				hacerGestos.getHgMove().setMoveSnap(corners, tolerances);
				hacerGestos.invalidate();

			}
		});

		hacerGestos.getHgRotate().setPrecisionRotation(0f);
		hacerGestos.getHgScale().setDisableGesture(true);
		hacerGestos.getHgMove().setDisableGesture(false);
		hacerGestos.getHgFling().setDisableGesture(false);
		hacerGestos.getHgFling().setFlingOffOfEdge(false);
		hacerGestos.getHgFling().setFlingNearestCorner(true);
		hacerGestos.getHgFling().setFlingBounceBack(false);
		hacerGestos.getHgFling().setupFlingValues(point.x / 8, 350L, 2500L);

		hacerGestos.registerCallback(new HacerGestos.IHacerGestos() {
			@Override
			public void onDown(HGResult hgResult) {}
			@Override
			public void onPointerDown(HGResult hgResult) {}
			@Override
			public void onMove(HGResult hgResult) {}
			@Override
			public void onPointerUp(HGResult hgResult) {}

			@Override
			public void onUp(HGResult hgResult) {

				final float centerSnapTolerance = hacerGestos.getHgMove().getMoveSnapTolerance()[4];

				if(flingHasSnapped == false) {

					if(hgResult.getWhatGesture() == HGResult.MOVE_GESTURE && hgResult.getMoveSnapped() == true) {

						launchSelectedFragment(new Point((int) hgResult.getXPos(), (int) hgResult.getYPos()));
						moveHasSnapped = true;
						flingHasSnapped = false;

					}

				}

				if(moveHasSnapped == false) {

					if(hgResult.getWhatGesture() == HGResult.FLING_GESTURE && hgResult.getFlingTriggered() == false && hgResult.getFlingAnimationComplete() == true) {

						launchSelectedFragment(new Point((int) hgResult.getXPos(), (int) hgResult.getYPos()));
						flingHasSnapped = true;
						moveHasSnapped = false;

					}//End if((hgResult.getWhatGesture() == HGResult.MOVE_GESTURE && hgResult.getMovedMoveSnapped() == true) && hgResult.getQuickTap() == true)

				}//End if(moveHasSnapped == false)

			}
		});

	}//End private void setupMainView()


	public boolean isFlingAnimationActive() {return hacerGestos.isFlingAnimationInProgress();}//Depricated Need to Remove

	@Override
	public void onPause() {
		super.onPause();

		hacerGestos.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		hacerGestos.onResume();

	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IMainFragment) {

			iMainFragment = (IMainFragment) context;

		}
		else {

			throw new RuntimeException(context.toString() + " must implement IMainFragment");

		}

	}


	@Override
	public void onDetach() {
		super.onDetach();

		iMainFragment = null;

	}

}